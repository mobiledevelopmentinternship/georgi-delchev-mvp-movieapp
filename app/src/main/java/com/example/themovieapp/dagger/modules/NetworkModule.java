package com.example.themovieapp.dagger.modules;

import android.content.Context;

import com.example.themovieapp.BuildConfig;
import com.example.themovieapp.networking.retrofit.base.MovieApi;
import com.readystatesoftware.chuck.ChuckInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.themovieapp.utils.Constants.CONNECTION_TIMEOUT;

@Module
public abstract class NetworkModule {

    @Singleton
    @Provides
    static OkHttpClient provideOkHttpClient(ChuckInterceptor chuckInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(chuckInterceptor)
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .build();
    }

    @Singleton
    @Provides
    static ChuckInterceptor provideChuckInterceptor(Context context) {
        return new ChuckInterceptor(context);
    }

    @Singleton
    @Provides
    static Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    @Singleton
    @Provides
    static MovieApi providesMovieApi(Retrofit retrofit) {
        return retrofit.create(MovieApi.class);
    }
}