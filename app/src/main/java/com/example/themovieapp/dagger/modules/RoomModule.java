package com.example.themovieapp.dagger.modules;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.themovieapp.persistance.daos.CrewMemberDao;
import com.example.themovieapp.persistance.daos.EpisodeDao;
import com.example.themovieapp.persistance.daos.GuestStarDao;
import com.example.themovieapp.persistance.daos.MovieDao;
import com.example.themovieapp.persistance.daos.ScreenCastDao;
import com.example.themovieapp.persistance.daos.SeasonDao;
import com.example.themovieapp.persistance.daos.TvSeriesDao;
import com.example.themovieapp.persistance.daos.WatchlistsDao;
import com.example.themovieapp.persistance.db.MovieDatabase;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private final MovieDatabase movieDatabase;

    public RoomModule(Application application) {
        movieDatabase = Room
                .databaseBuilder(application, MovieDatabase.class, "MovieDB")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    MovieDatabase provideMovieDatabase() {
        return movieDatabase;
    }

    @Provides
    MovieDao providesMovieDao() {
        return movieDatabase.movieDao();
    }

    @Provides
    TvSeriesDao provideTvSeriesDao() {
        return movieDatabase.tvSeriesDao();
    }

    @Provides
    WatchlistsDao provideWatchlistsDao() {
        return movieDatabase.watchlistsDao();
    }

    @Provides
    SeasonDao providesSeasonDao() {
        return movieDatabase.seasonDao();
    }

    @Provides
    EpisodeDao providesEpisodeDao() {
        return movieDatabase.episodeDao();
    }

    @Provides
    CrewMemberDao providesCrewMemberDao() {
        return movieDatabase.crewMemberDao();
    }

    @Provides
    GuestStarDao providesGuestStarDao() {
        return movieDatabase.guestStarDao();
    }

    @Provides
    ScreenCastDao providesScreenCastDao() {
        return movieDatabase.screenCastDao();
    }
}