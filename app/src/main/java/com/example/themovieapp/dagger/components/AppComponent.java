package com.example.themovieapp.dagger.components;

import com.example.themovieapp.dagger.modules.AppModule;
import com.example.themovieapp.dagger.modules.NetworkModule;
import com.example.themovieapp.dagger.modules.RoomModule;
import com.example.themovieapp.presenters.details.moviedetails.MovieDetailsPresenter;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.TvSeasonEpisodeListPresenter;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.TvSeasonsPresenter;
import com.example.themovieapp.presenters.details.tv.tvseriesdetails.TvSeriesDetailsPresenter;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.TvSeriesEpisodeDetailsPresenter;
import com.example.themovieapp.presenters.discover.DiscoverPresenter;
import com.example.themovieapp.presenters.home.moviehome.MovieHomePresenter;
import com.example.themovieapp.presenters.home.tvserieshome.TVSeriesHomePresenter;
import com.example.themovieapp.presenters.watchlist.WatchlistsPresenter;
import com.example.themovieapp.ui.dialogs.addtowatchlist.presenter.AddToWatchlistPresenter;
import com.example.themovieapp.ui.dialogs.createnewwatachlist.presenter.CreateNewWatchlistPresenter;
import com.example.themovieapp.ui.dialogs.rating.presenter.RatingDialogPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        RoomModule.class,
        AppModule.class,
        NetworkModule.class
})
public interface AppComponent {

    void inject(MovieHomePresenter movieHomePresenter);

    void inject(TVSeriesHomePresenter tvSeriesHomePresenter);

    void inject(MovieDetailsPresenter movieDetailsPresenter);

    void inject(TvSeriesDetailsPresenter tvSeriesDetailsPresenter);

    void inject(TvSeriesEpisodeDetailsPresenter tvSeriesEpisodeDetailsPresenter);

    void inject(WatchlistsPresenter watchlistsPresenter);

    void inject(TvSeasonsPresenter tvSeasonsPresenter);

    void inject(TvSeasonEpisodeListPresenter tvSeasonEpisodeListPresenter);

    void inject(AddToWatchlistPresenter addToWatchlistPresenter);

    void inject(CreateNewWatchlistPresenter createNewWatchlistPresenter);

    void inject(RatingDialogPresenter ratingDialogPresenter);

    void inject(DiscoverPresenter discoverPresenter);
}