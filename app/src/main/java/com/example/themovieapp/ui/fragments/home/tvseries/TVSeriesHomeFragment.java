package com.example.themovieapp.ui.fragments.home.tvseries;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.poster.PosterAdapter;
import com.example.themovieapp.presenters.home.base.BaseHomeContracts;
import com.example.themovieapp.presenters.home.tvserieshome.contracts.TVSeriesHomeContracts;
import com.example.themovieapp.ui.activities.tvseriesdetails.TvSeriesDetailsActivity;
import com.example.themovieapp.ui.fragments.home.BaseHomeFragment;
import com.example.themovieapp.utils.Category;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.themovieapp.utils.Constants.MEDIA_INTENT_TAG;
import static com.example.themovieapp.utils.Constants.MEDIA_TYPE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;

public class TVSeriesHomeFragment extends BaseHomeFragment implements TVSeriesHomeContracts.View {

    private TVSeriesHomeContracts.Presenter tvSeriesPresenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpCategoriesTittles();
        tvSeriesPresenter.start();
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.item_home;
    }

    @Override
    public void setPresenter(BaseHomeContracts.Presenter presenter) {
        this.tvSeriesPresenter = (TVSeriesHomeContracts.Presenter) presenter;
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        if (isAdded()) {
            progressLayoutHome.setLoading(isVisible);
            nestedScrollViewHome.setVisibility(isVisible ? GONE : VISIBLE);
        }
    }

    public static TVSeriesHomeFragment newInstance() {
        return new TVSeriesHomeFragment();
    }

    private void setUpCategoriesTittles() {
        txtUpcomingCategoryTitle.setText(R.string.home_screen_tv_show_airing_today_title);
        txtMostPopularCategoryTitle.setText(R.string.home_screen_tv_show_popular_shows_title);
        txtTopRatedCategoryTitle.setText(R.string.home_screen_tv_show_top_rated_shows_title);
    }

    @Override
    public void setupTopRatedAdapter(Category category) {
        if (isAdded()) {
            if (tvSeriesPresenter.getTopRated().isEmpty()) {
                cardViewTopRatedCategory.setVisibility(GONE);
            } else {
                PosterAdapter topRatedTvAdapter = new PosterAdapter(tvSeriesPresenter, category);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerViewTopRatedCategory.setAdapter(topRatedTvAdapter);
                recyclerViewTopRatedCategory.setLayoutManager(layoutManager);
                topRatedTvAdapter.notifyDataSetChanged();
            }
            tvSeriesPresenter.setTopRatedAdapterFlag();
        }
    }

    @Override
    public void setupMostPopularAdapter(Category category) {
        if (isAdded()) {
            if (tvSeriesPresenter.getMostPopular().isEmpty()) {
                cardViewMostPopularCategory.setVisibility(GONE);
            } else {
                PosterAdapter mostPopularTvAdapter = new PosterAdapter(tvSeriesPresenter, category);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerViewMostPopularCategory.setAdapter(mostPopularTvAdapter);
                recyclerViewMostPopularCategory.setLayoutManager(layoutManager);
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mostPopularTvAdapter.notifyDataSetChanged();
            }
            tvSeriesPresenter.setMostPopularAdapterFlag();
        }
    }

    @Override
    public void setupUpcomingAdapter(Category category) {
        if (isAdded()) {
            if (tvSeriesPresenter.getUpcoming().isEmpty()) {
                cardViewUpcomingCategory.setVisibility(GONE);
            } else {
                PosterAdapter upcomingTvAdapter = new PosterAdapter(tvSeriesPresenter, category);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerViewUpcomingCategory.setAdapter(upcomingTvAdapter);
                recyclerViewUpcomingCategory.setLayoutManager(layoutManager);
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                upcomingTvAdapter.notifyDataSetChanged();
            }
            tvSeriesPresenter.setUpcomingAdapterFlag();
        }
    }

    @Override
    public void loadSelectedTvSeries(Integer tvSeriesId, Integer mediaTypeId) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), TvSeriesDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, tvSeriesId);
            bundle.putString(MEDIA_TYPE_BUNDLE_KEY, getString(mediaTypeId));
            intent.putExtra(MEDIA_INTENT_TAG, bundle);

            startActivity(intent);
        }
    }

    @Override
    public void showError(Integer messageId) {
        if (isAdded()) {
            loadErrorDialog(getString(messageId));
        }
    }

    @Override
    public void onDestroy() {
        tvSeriesPresenter.cancelRequests();
        super.onDestroy();
    }
}