package com.example.themovieapp.ui.activities.tvseriesdetails;

import android.os.Bundle;

import com.example.themovieapp.R;
import com.example.themovieapp.presenters.details.tv.tvseriesdetails.TvSeriesDetailsPresenter;
import com.example.themovieapp.ui.activities.base.BaseActivity;
import com.example.themovieapp.ui.fragments.details.tvseriesdetails.TvSeriesDetailsFragment;

import static com.example.themovieapp.utils.Constants.DISCOVERED_ID_DEFAULT_VALUE;
import static com.example.themovieapp.utils.Constants.DISCOVERED_TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MEDIA_INTENT_TAG;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;

public class TvSeriesDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBottomNavigationVisibility(false);
        Bundle bundle = getIntent().getBundleExtra(MEDIA_INTENT_TAG);
        if (bundle != null) {
            loadTvSeriesDetailsFragment(bundle.getInt(TV_SERIES_ID_BUNDLE_KEY),
                    getString(R.string.tv_series_media_type));
        } else {
            if (getIntent().getExtras() != null
                    && getIntent().getExtras().containsKey(DISCOVERED_TV_SERIES_ID_BUNDLE_KEY)) {
                loadTvSeriesDetailsFragment(getIntent().getIntExtra(DISCOVERED_TV_SERIES_ID_BUNDLE_KEY, DISCOVERED_ID_DEFAULT_VALUE),
                        getString(R.string.tv_series_media_type));
            }
        }
    }

    private void loadTvSeriesDetailsFragment(Integer id, String mediaType) {
        TvSeriesDetailsFragment tvSeriesDetailsFragment = TvSeriesDetailsFragment.newInstance(id, mediaType);
        commitFragmentTransaction(tvSeriesDetailsFragment);
    }
}
