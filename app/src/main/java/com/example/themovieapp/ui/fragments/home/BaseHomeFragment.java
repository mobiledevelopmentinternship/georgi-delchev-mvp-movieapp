package com.example.themovieapp.ui.fragments.home;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.ui.fragments.base.BaseFragment;
import com.example.themovieapp.view.ProgressLayout;

import butterknife.BindView;

public abstract class BaseHomeFragment extends BaseFragment {

    @BindView(R.id.txt_upcoming)
    protected TextView txtUpcomingCategoryTitle;
    @BindView(R.id.card_view_upcoming)
    protected CardView cardViewUpcomingCategory;
    @BindView(R.id.recycler_view_upcoming)
    protected RecyclerView recyclerViewUpcomingCategory;
    @BindView(R.id.txt_top_trending)
    protected TextView txtMostPopularCategoryTitle;
    @BindView(R.id.card_view_top_trending)
    protected CardView cardViewMostPopularCategory;
    @BindView(R.id.recycler_view_top_trending)
    protected RecyclerView recyclerViewMostPopularCategory;
    @BindView(R.id.txt_top_rated)
    protected TextView txtTopRatedCategoryTitle;
    @BindView(R.id.card_view_top_rated)
    protected CardView cardViewTopRatedCategory;
    @BindView(R.id.recycler_view_top_rated)
    protected RecyclerView recyclerViewTopRatedCategory;
    @BindView(R.id.layout_home_progress)
    protected ProgressLayout progressLayoutHome;
    @BindView(R.id.nested_scroll_view_home)
    protected NestedScrollView nestedScrollViewHome;

    @Override
    protected Integer getLayoutResId() {
        return R.layout.item_home;
    }
}
