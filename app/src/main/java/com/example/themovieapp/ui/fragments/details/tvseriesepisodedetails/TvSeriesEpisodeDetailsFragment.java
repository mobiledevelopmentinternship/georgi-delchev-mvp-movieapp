package com.example.themovieapp.ui.fragments.details.tvseriesepisodedetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.additionalinformation.EpisodeAdditionalInformationAdapter;
import com.example.themovieapp.adapters.pageradapters.ImagesPagerAdapter;
import com.example.themovieapp.adapters.screencast.ScreenCastAdapter;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.TvSeriesEpisodeDetailsPresenter;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.contracts.TvSeriesEpisodeDetailsContracts;
import com.example.themovieapp.ui.activities.tvseasondetails.TvSeasonDetailsActivity;
import com.example.themovieapp.ui.activities.tvseriesepisodedetails.TvSeriesEpisodeDetailsActivity;
import com.example.themovieapp.ui.fragments.base.BaseFragment;
import com.example.themovieapp.utils.PicassoImageLoader;
import com.example.themovieapp.view.ProgressLayout;

import butterknife.BindView;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.themovieapp.utils.Constants.EPISODE_DETAILS_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.EPISODE_NUMBER_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.NUMBER_OF_SEASON_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.SEASON_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.SEASON_NUMBER_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SEASONS_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;

public class TvSeriesEpisodeDetailsFragment extends BaseFragment implements TvSeriesEpisodeDetailsContracts.View {

    @BindView(R.id.progress_layout_details)
    ProgressLayout progressLayout;
    @BindView(R.id.view_pager_details_episode_images)
    ViewPager viewPager;
    @BindView(R.id.txt_details_name)
    TextView txtEpisodeName;
    @BindView(R.id.txt_details_release_date)
    TextView txtEpisodeReleaseDate;
    @BindView(R.id.txt_details_media_type)
    TextView txtMediaType;
    @BindView(R.id.img_details_overview_poster)
    ImageView imgOverviewCardPoster;
    @BindView(R.id.txt_details_overview)
    TextView txtEpisodeOverview;
    @BindView(R.id.btn_details_add_to_collection)
    Button btnAddToCollection;
    @BindView(R.id.btn_details_rate)
    Button btnRate;
    @BindView(R.id.btn_details_all_episodes)
    Button btnAllEpisodes;
    @BindView(R.id.recycler_view_details_episode_cast)
    RecyclerView recyclerViewEpisodeCast;
    @BindView(R.id.txt_details_other_information_title)
    TextView txtOtherInfoTitle;
    @BindView(R.id.recycler_view_details_additional_information)
    RecyclerView recyclerViewAdditionalInformation;
    @BindView(R.id.view_group_details)
    Group viewGroup;
    @BindView(R.id.card_view_details_members)
    CardView cardViewMembers;

    private TvSeriesEpisodeDetailsContracts.Presenter presenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpView();

        presenter.start();
    }

    private void setUpView() {
        btnAddToCollection.setVisibility(GONE);
        btnRate.setVisibility(GONE);
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.fragment_details;
    }

    public static TvSeriesEpisodeDetailsFragment newInstance(Integer seasonId, Integer seasonNumber, Integer episodeNumber, Integer tvSeriesId, Integer numberOfSeasons, String title) {
        TvSeriesEpisodeDetailsFragment fragment = new TvSeriesEpisodeDetailsFragment();
        new TvSeriesEpisodeDetailsPresenter(fragment, seasonId, seasonNumber, episodeNumber, tvSeriesId, numberOfSeasons, title);
        return fragment;
    }

    @Override
    public void setPresenter(TvSeriesEpisodeDetailsContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        if (isAdded()) {
            progressLayout.setLoading(isVisible);
            viewGroup.setVisibility(isVisible ? GONE : VISIBLE);
            btnAllEpisodes.setVisibility(isVisible ? GONE : VISIBLE);
        }
    }

    @OnClick(R.id.btn_details_all_episodes)
    public void allEpisodesButtonClicked() {
        if (isAdded()) {
            presenter.loadAllEpisodes();
        }
    }

    @Override
    public void showError(Integer messageId) {
        if (isAdded()) {
            loadErrorDialog(getString(messageId));
        }
    }

    @Override
    public void setupEpisodeImagesAdapter() {
        if (isAdded()) {
            ImagesPagerAdapter imagesPagerAdapter = new ImagesPagerAdapter(presenter);
            viewPager.setAdapter(imagesPagerAdapter);
            imagesPagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void hideViewPager() {
        if (isAdded()) {
            viewPager.setVisibility(GONE);
        }
    }

    @Override
    public void setEpisodeName(String name) {
        if (isAdded()) {
            txtEpisodeName.setText(name);
        }
    }

    @Override
    public void setEpisodeReleaseDate(String date) {
        if (isAdded()) {
            txtEpisodeReleaseDate.setText(date);
        }
    }

    @Override
    public void setMediaType(Integer mediaType) {
        if (isAdded()) {
            txtMediaType.setText(getString(mediaType));
        }
    }

    @Override
    public void setPosterImageInOverviewCard(String url) {
        if (isAdded()) {
            PicassoImageLoader.setImage(imgOverviewCardPoster, url);
        }
    }

    @Override
    public void setEpisodeOverview(String overview) {
        if (isAdded()) {
            txtEpisodeOverview.setText(overview);
        }
    }

    @Override
    public void setAdditionalInformationTitle(Integer title) {
        if(isAdded()) {
            txtOtherInfoTitle.setText(getString(title));
        }
    }

    @Override
    public void setupScreenCastAdapter() {
        if (isAdded()) {
            ScreenCastAdapter screenCastAdapter = new ScreenCastAdapter(presenter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerViewEpisodeCast.setAdapter(screenCastAdapter);
            recyclerViewEpisodeCast.setLayoutManager(linearLayoutManager);
            screenCastAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setupEpisodesAdapter() {
        if (isAdded()) {
            EpisodeAdditionalInformationAdapter episodeAdditionalInformationAdapter = new EpisodeAdditionalInformationAdapter(presenter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerViewAdditionalInformation.setAdapter(episodeAdditionalInformationAdapter);
            recyclerViewAdditionalInformation.setLayoutManager(linearLayoutManager);
            episodeAdditionalInformationAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showAnotherEpisodeFromTheSeason(Integer tvSeriesId,
                                                Integer seasonId,
                                                Integer seasonNumber,
                                                Integer episodeNumber,
                                                Integer numberOfSeasons) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), TvSeriesEpisodeDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(SEASON_ID_BUNDLE_KEY, seasonId);
            bundle.putInt(SEASON_NUMBER_BUNDLE_KEY, seasonNumber);
            bundle.putInt(EPISODE_NUMBER_BUNDLE_KEY, episodeNumber);
            bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, tvSeriesId);
            bundle.putInt(NUMBER_OF_SEASON_BUNDLE_KEY, numberOfSeasons);
            intent.putExtra(EPISODE_DETAILS_BUNDLE_KEY, bundle);

            startActivity(intent);
        }
    }

    @Override
    public void showAllEpisodes(Integer tvSeriesId, Integer numberOfSeasons) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), TvSeasonDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, tvSeriesId);
            bundle.putInt(NUMBER_OF_SEASON_BUNDLE_KEY, numberOfSeasons);
            intent.putExtra(TV_SEASONS_BUNDLE_KEY, bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onDestroy() {
        presenter.cancelTasks();
        super.onDestroy();
    }
}
