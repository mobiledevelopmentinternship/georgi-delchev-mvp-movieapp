package com.example.themovieapp.ui.fragments.watchlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.Toast;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.watchlist.ExpandableWatchlistAdapter;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.presenters.watchlist.contracts.WatchlistsContracts;
import com.example.themovieapp.ui.activities.moviedetails.MovieDetailsActivity;
import com.example.themovieapp.ui.activities.tvseriesdetails.TvSeriesDetailsActivity;
import com.example.themovieapp.ui.dialogs.createnewwatachlist.CreateNewWatchlistDialogFragment;
import com.example.themovieapp.ui.fragments.base.BaseFragment;
import com.example.themovieapp.view.ProgressLayout;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import butterknife.BindView;
import butterknife.OnClick;

import static android.widget.LinearLayout.VERTICAL;
import static com.example.themovieapp.utils.Constants.MEDIA_INTENT_TAG;
import static com.example.themovieapp.utils.Constants.MEDIA_TYPE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MOVIE_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.WATCHLISTS_FRAGMENT_REQUEST_CODE;
import static com.example.themovieapp.utils.Constants.WATCHLIST_FRAGMENT_TAG;
import static com.example.themovieapp.utils.Constants.WATCHLIST_INTENT_KEY;

public class WatchlistsFragment extends BaseFragment implements WatchlistsContracts.View {

    @BindView(R.id.recycler_view_watchlist)
    RecyclerView expandableWatchlists;
    @BindView(R.id.progress_layout_watchlists)
    ProgressLayout progressLayout;

    private WatchlistsContracts.Presenter presenter;
    private ExpandableWatchlistAdapter expandableWatchlistAdapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.start();
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.fragment_watchlists;
    }

    public static WatchlistsFragment newInstance() {
        return new WatchlistsFragment();
    }

    @Override
    public void setPresenter(WatchlistsContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        if (isAdded()) {
            progressLayout.setLoading(isVisible);
        }
    }

    @Override
    public void setWatchlistAdapter() {
        if (isAdded()) {
            RecyclerViewExpandableItemManager expandableItemManager = new RecyclerViewExpandableItemManager(null);
            expandableWatchlistAdapter = new ExpandableWatchlistAdapter(presenter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(VERTICAL);
            expandableWatchlists.setLayoutManager(linearLayoutManager);
            expandableWatchlists.setAdapter(expandableItemManager.createWrappedAdapter(
                    expandableWatchlistAdapter
            ));
            expandableItemManager.attachRecyclerView(expandableWatchlists);
            if (expandableWatchlists.getItemAnimator() != null) {
                ((SimpleItemAnimator) expandableWatchlists
                        .getItemAnimator()).setSupportsChangeAnimations(false);
            }

            presenter.showWatchlists();
        }
    }

    @Override
    public void showWatchlistEntryDetails(Integer id, String mediaType) {
        Bundle bundle = new Bundle();
        Intent intent;
        bundle.putString(MEDIA_TYPE_BUNDLE_KEY, mediaType);
        if (mediaType.equals(getString(R.string.movie_media_type))) {
            bundle.putInt(MOVIE_ID_BUNDLE_KEY, id);
            intent = new Intent(getActivity(), MovieDetailsActivity.class);
        } else {
            bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, id);
            intent = new Intent(getActivity(), TvSeriesDetailsActivity.class);
        }

        intent.putExtra(MEDIA_INTENT_TAG, bundle);
        startActivity(intent);
    }

    @Override
    public void addNewlyCreatedWatchlist(int position) {
        expandableWatchlistAdapter.notifyItemInserted(position);
    }

    @OnClick(R.id.fab_create_new_watchlist)
    public void createNewWatchlist() {
        if (getFragmentManager() == null) {
            return;
        }

        CreateNewWatchlistDialogFragment createNewWatchlistDialogFragment = CreateNewWatchlistDialogFragment.newInstance(WATCHLISTS_FRAGMENT_REQUEST_CODE);
        createNewWatchlistDialogFragment.setTargetFragment(this, WATCHLISTS_FRAGMENT_REQUEST_CODE);
        createNewWatchlistDialogFragment.show(getFragmentManager(), WATCHLIST_FRAGMENT_TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == WATCHLISTS_FRAGMENT_REQUEST_CODE && data.getExtras() != null) {
            WatchlistPersist created = data.getExtras().getParcelable(WATCHLIST_INTENT_KEY);
            presenter.loadNewlyCreatedWatchlist(created);
        }
    }

    @Override
    public void showEmptyWatchlistMessage(String watchlistName) {
        Toast.makeText(getContext(), watchlistName + " " + getString(R.string.watchlists_is_empty), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getWatchlistsFromDb();
    }

    @Override
    public void onDestroy() {
        presenter.cancelTasks();
        super.onDestroy();
    }
}