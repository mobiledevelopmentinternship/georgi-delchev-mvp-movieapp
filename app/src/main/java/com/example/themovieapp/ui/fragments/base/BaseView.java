package com.example.themovieapp.ui.fragments.base;

public interface BaseView<T> {

    void setPresenter(T presenter);

    void setProgressBarVisibility(boolean isVisible);
}
