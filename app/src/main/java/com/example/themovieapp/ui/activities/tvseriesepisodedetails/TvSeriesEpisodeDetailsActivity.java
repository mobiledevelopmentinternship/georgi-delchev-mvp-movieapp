package com.example.themovieapp.ui.activities.tvseriesepisodedetails;

import android.os.Bundle;

import com.example.themovieapp.ui.activities.base.BaseActivity;
import com.example.themovieapp.ui.fragments.details.tvseriesepisodedetails.TvSeriesEpisodeDetailsFragment;

import static com.example.themovieapp.utils.Constants.EPISODE_DETAILS_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.EPISODE_NUMBER_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.NUMBER_OF_SEASON_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.SEASON_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.SEASON_NUMBER_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_TITLE;

public class TvSeriesEpisodeDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBottomNavigationVisibility(false);
        Bundle bundle = getIntent().getBundleExtra(EPISODE_DETAILS_BUNDLE_KEY);
        if (bundle != null) {
            loadTvSeriesEpisodeDetails(bundle.getInt(SEASON_ID_BUNDLE_KEY),
                    bundle.getInt(SEASON_NUMBER_BUNDLE_KEY),
                    bundle.getInt(EPISODE_NUMBER_BUNDLE_KEY),
                    bundle.getInt(TV_SERIES_ID_BUNDLE_KEY),
                    bundle.getInt(NUMBER_OF_SEASON_BUNDLE_KEY),
                    bundle.getString(TV_SERIES_TITLE));
        }
    }

    private void loadTvSeriesEpisodeDetails(Integer seasonId,
                                            Integer seasonNumber,
                                            Integer episodeNumber,
                                            Integer tvSeriesId,
                                            Integer numberOfSeasons,
                                            String title) {
        TvSeriesEpisodeDetailsFragment tvSeriesEpisodeDetailsFragment =
                TvSeriesEpisodeDetailsFragment.newInstance(seasonId, seasonNumber, episodeNumber,
                        tvSeriesId, numberOfSeasons, title);
        commitFragmentTransaction(tvSeriesEpisodeDetailsFragment);
    }
}