package com.example.themovieapp.ui.dialogs.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.themovieapp.ui.dialogs.error.ErrorDialogFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.example.themovieapp.utils.Constants.ERROR_DIALOG_TAG;

public abstract class BaseDialogFragment extends DialogFragment {

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResId(), container, false);

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @LayoutRes
    protected abstract int getLayoutResId();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    protected void loadErrorDialog(String message) {
        if (getFragmentManager() == null) {
            return;
        }

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment previousFragment = getFragmentManager().findFragmentByTag(ERROR_DIALOG_TAG);
        if (previousFragment != null) {
            fragmentTransaction.remove(previousFragment);
        }

        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(message);
        errorDialogFragment.show(fragmentTransaction, ERROR_DIALOG_TAG);
    }
}