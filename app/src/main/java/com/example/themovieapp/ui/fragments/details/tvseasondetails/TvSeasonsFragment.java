package com.example.themovieapp.ui.fragments.details.tvseasondetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.pageradapters.BasicTabLayoutAdapter;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.TvSeasonsPresenter;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts.TvSeasonDetailsContracts;
import com.example.themovieapp.ui.fragments.base.BaseFragment;
import com.example.themovieapp.view.ProgressLayout;

import butterknife.BindView;

public class TvSeasonsFragment extends BaseFragment implements TvSeasonDetailsContracts.View {

    @BindView(R.id.view_pager_seasons_screen)
    ViewPager viewPager;
    @BindView(R.id.tab_layout_seasons)
    TabLayout tabLayoutSeason;
    @BindView(R.id.progress_layout_seasons)
    ProgressLayout viewGroupSeasons;

    private TvSeasonDetailsContracts.Presenter presenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.start();
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.fragment_seasons;
    }

    public static TvSeasonsFragment newInstance(Integer id, Integer numberOfSeasons, String title) {
        TvSeasonsFragment fragment = new TvSeasonsFragment();
        new TvSeasonsPresenter(fragment, id, numberOfSeasons, title);
        return fragment;
    }

    @Override
    public void setPresenter(TvSeasonDetailsContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        if (isAdded()) {
            viewGroupSeasons.setLoading(isVisible);
        }
    }

    @Override
    public void onDestroy() {
        presenter.cancelRequests();
        super.onDestroy();
    }

    @Override
    public void setSeasonAdapter() {
        if (isAdded()) {
            BasicTabLayoutAdapter tabAdapter = new BasicTabLayoutAdapter(getChildFragmentManager(), presenter);
            viewPager.setAdapter(tabAdapter);
            tabLayoutSeason.setupWithViewPager(viewPager);
            tabAdapter.notifyDataSetChanged();
            presenter.loadingOfFragmentsIsFinished(true);
        }
    }

    @Override
    public void showError(Integer messageId) {
        if (isAdded()) {
            loadErrorDialog(getString(messageId));
        }
    }
}