package com.example.themovieapp.ui.dialogs.error;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.ui.dialogs.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.ERROR_DIALOG_MESSAGE_KEY;

public class ErrorDialogFragment extends BaseDialogFragment {

    @BindView(R.id.txt_error)
    TextView txtErrorMessage;

    public static ErrorDialogFragment newInstance(String message) {
        ErrorDialogFragment dialog = new ErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ERROR_DIALOG_MESSAGE_KEY, message);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ERROR_DIALOG_MESSAGE_KEY)) {
            txtErrorMessage.setText(getArguments().getString(ERROR_DIALOG_MESSAGE_KEY));
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            }
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_fragment_error;
    }

    @OnClick(R.id.btn_ok)
    void btnOkClicked() {
        dismiss();
    }
}
