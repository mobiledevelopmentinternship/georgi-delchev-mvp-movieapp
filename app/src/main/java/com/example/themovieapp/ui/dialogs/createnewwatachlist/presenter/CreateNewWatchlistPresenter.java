package com.example.themovieapp.ui.dialogs.createnewwatachlist.presenter;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.services.WatchlistPersistService;
import com.example.themovieapp.ui.dialogs.base.BaseDialogPresenter;
import com.example.themovieapp.ui.dialogs.createnewwatachlist.presenter.contracts.CreateNewWatchlistContracts;

import javax.inject.Inject;

public class CreateNewWatchlistPresenter extends BaseDialogPresenter implements CreateNewWatchlistContracts.Presenter {

    private final CreateNewWatchlistContracts.View view;
    @Inject
    WatchlistPersistService watchlistPersistService;

    public CreateNewWatchlistPresenter(CreateNewWatchlistContracts.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void createNewWatchlist(String watchlistName) {
        if (watchlistName.length() == 0) {
            view.loadError(R.string.dialog_create_new_watchlist_empty_watchlist_name_error);
            return;
        }

        WatchlistPersist watchlistPersist = new WatchlistPersist(watchlistName);
        insertWatchlistInDb(watchlistPersist);
    }

    private void insertWatchlistInDb(WatchlistPersist watchlistPersist) {
        subscribeCompletable(
                createCompletable(() -> watchlistPersistService.insert(watchlistPersist)),
                () -> view.loadCreatedWatchlist(watchlistPersist),
                throwable -> view.loadError(R.string.create_new_watchlist_error));
    }

    @Override
    public void cancelTasks() {
        dispose();
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}