package com.example.themovieapp.ui.dialogs.rating.presenter;

import com.example.themovieapp.networking.services.common.RatingService;
import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.rating.GuestSessionResponse;
import com.example.themovieapp.ui.dialogs.base.BaseDialogPresenter;
import com.example.themovieapp.ui.dialogs.rating.presenter.contracts.RatingDialogContracts;
import com.example.themovieapp.utils.SharedPrefsUtil;

import javax.inject.Inject;

import static com.example.themovieapp.utils.Utils.compareGuestSessionExpireTimestamp;
import static com.example.themovieapp.utils.Utils.constructCongratulationsMessage;

public class RatingDialogPresenter extends BaseDialogPresenter implements RatingDialogContracts.Presenter {

    private final RatingDialogContracts.View view;
    @Inject
    RatingService ratingService;
    @Inject
    SharedPrefsUtil sharedPrefs;

    private String mediaType;
    private Integer id;
    private String title;
    private Integer rating;

    public RatingDialogPresenter(RatingDialogContracts.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public void setMediaId(Integer id) {
        this.id = id;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Override
    public void cancelTasks() {
        dispose();
    }

    @Override
    public void checkGuestSession() {
        if (compareGuestSessionExpireTimestamp(sharedPrefs.getSessionExpireDate())) {
            getNewGuestSessionId();
        } else {
            sendRating();
        }
    }

    private void sendRating() {
        if (!isConnected()) {
            view.showError(R.string.error_network_connection_unavailable);
        } else {
            subscribeCompletable(
                    createCompletable(() ->
                            ratingService.postRating(rating,
                                    mediaType,
                                    id,
                                    sharedPrefs.getGuestSessionId())),
                    this::showCongratulationsMessage,
                    this::onPostRequestError
            );
        }
    }

    private void showCongratulationsMessage() {
        String message = constructCongratulationsMessage(rating, title);
        view.showCongratulationsMessage(message);
    }

    private void getNewGuestSessionId() {
        subscribeSingle(
                ratingService.getGuestSessionId(),
                this::onNewGuestSessionRequestSuccess,
                this::onNewGuestSessionRequestError
        );
    }

    private void onNewGuestSessionRequestError(Throwable throwable) {
        view.showError(R.string.dialog_rating_error_message);
    }

    private void onNewGuestSessionRequestSuccess(GuestSessionResponse guestSessionResponse) {
        sharedPrefs.saveGuestSessionId(guestSessionResponse.getGuestSessionId());
        sharedPrefs.saveSessionExpireTimestamp(guestSessionResponse.getGuestSessionExpireDate());

        sendRating();
    }

    private void onPostRequestError(Throwable throwable) {
        view.showError(R.string.dialog_rating_error_message);
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}
