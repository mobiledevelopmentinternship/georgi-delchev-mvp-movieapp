package com.example.themovieapp.ui.fragments.details.tvseasondetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.season.SeasonAdapter;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts.SeasonEpisodeListContracts;
import com.example.themovieapp.ui.activities.tvseriesepisodedetails.TvSeriesEpisodeDetailsActivity;
import com.example.themovieapp.ui.fragments.base.BaseFragment;

import java.util.List;

import butterknife.BindView;

import static android.widget.LinearLayout.VERTICAL;
import static com.example.themovieapp.utils.Constants.EPISODE_DETAILS_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.EPISODE_NUMBER_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.INDEX_OFFSET;
import static com.example.themovieapp.utils.Constants.NUMBER_OF_SEASON_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.SEASON_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.SEASON_NUMBER_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_TITLE;

public class TvSeasonEpisodesListFragment extends BaseFragment implements SeasonEpisodeListContracts.View {

    @BindView(R.id.recycler_view_tv_season)
    RecyclerView recyclerViewSeasonEpisodes;

    private SeasonEpisodeListContracts.Presenter presenter;

    public static TvSeasonEpisodesListFragment newInstance() {
        return new TvSeasonEpisodesListFragment();
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.layout_tv_season;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.start();
    }

    @Override
    public void setPresenter(SeasonEpisodeListContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void loadEpisodeList(List<EpisodePersist> episodes) {
        SeasonAdapter seasonAdapter = new SeasonAdapter(presenter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(VERTICAL);
        recyclerViewSeasonEpisodes.setAdapter(seasonAdapter);
        recyclerViewSeasonEpisodes.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void showEpisodeDetails(Integer seasonId,
                                   Integer seasonNumber,
                                   Integer episodeNumber,
                                   Integer tvSeriesId,
                                   Integer allSeasonsCount,
                                   String tvSeriesTitle) {
        Intent intent = new Intent(getActivity(), TvSeriesEpisodeDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(SEASON_ID_BUNDLE_KEY, seasonId);
        bundle.putInt(SEASON_NUMBER_BUNDLE_KEY, seasonNumber);
        bundle.putInt(EPISODE_NUMBER_BUNDLE_KEY, episodeNumber + INDEX_OFFSET);
        bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, tvSeriesId);
        bundle.putInt(NUMBER_OF_SEASON_BUNDLE_KEY, allSeasonsCount);
        bundle.putString(TV_SERIES_TITLE, tvSeriesTitle);
        intent.putExtra(EPISODE_DETAILS_BUNDLE_KEY, bundle);

        startActivity(intent);
    }
}