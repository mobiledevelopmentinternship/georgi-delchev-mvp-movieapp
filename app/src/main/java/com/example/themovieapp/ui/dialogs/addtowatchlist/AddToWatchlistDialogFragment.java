package com.example.themovieapp.ui.dialogs.addtowatchlist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.ui.dialogs.addtowatchlist.presenter.AddToWatchlistPresenter;
import com.example.themovieapp.ui.dialogs.addtowatchlist.presenter.contracts.AddToWatchlistContracts;
import com.example.themovieapp.ui.dialogs.base.BaseDialogFragment;
import com.example.themovieapp.ui.dialogs.createnewwatachlist.CreateNewWatchlistDialogFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.ADD_TO_WATCHLIST_REQUEST_CODE;
import static com.example.themovieapp.utils.Constants.CREATE_NEW_WATCHLIST_DIALOG_TAG;
import static com.example.themovieapp.utils.Constants.FRAGMENT_REQUEST_CODE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.IS_ADDED;
import static com.example.themovieapp.utils.Constants.WATCHABLE_KEY;
import static com.example.themovieapp.utils.Constants.WATCHABLE_NAME_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.WATCHLIST_NAME_BUNDLE_KEY;

public class AddToWatchlistDialogFragment extends BaseDialogFragment
        implements AddToWatchlistContracts.View, RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.radio_grp_watchlists)
    RadioGroup radioGroupWatchlists;

    private AddToWatchlistContracts.Presenter presenter;
    private Integer requestCode;

    public static AddToWatchlistDialogFragment newInstance(Integer requestCode, Watchable watchable) {
        AddToWatchlistDialogFragment addToWatchlistDialogFragment = new AddToWatchlistDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_REQUEST_CODE_BUNDLE_KEY, requestCode);
        bundle.putParcelable(WATCHABLE_KEY, watchable);
        addToWatchlistDialogFragment.setArguments(bundle);
        new AddToWatchlistPresenter(addToWatchlistDialogFragment);

        return addToWatchlistDialogFragment;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(WATCHABLE_KEY)
                && getArguments().containsKey(FRAGMENT_REQUEST_CODE_BUNDLE_KEY)) {
            presenter.setContent((Watchable) getArguments().get(WATCHABLE_KEY));
            this.requestCode = getArguments().getInt(FRAGMENT_REQUEST_CODE_BUNDLE_KEY);
            getDialog().setTitle(getString(R.string.dialog_add_to_watchlist_title));

            presenter.getWatchlists();
            radioGroupWatchlists.setOnCheckedChangeListener(this);
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_fragment_add_to_collection;
    }

    @OnClick(R.id.btn_dialog_add_to_collection)
    public void onSaveClicked() {
        presenter.addToWatchlist();
        dismiss();
    }

    @OnClick(R.id.btn_dialog_add_to_collection_create)
    public void createNewWatchlist() {
        if (getFragmentManager() == null) {
            return;
        }

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment previousDialogFragment = getFragmentManager().findFragmentByTag(CREATE_NEW_WATCHLIST_DIALOG_TAG);
        if (previousDialogFragment != null) {
            fragmentTransaction.remove(previousDialogFragment);
        }

        CreateNewWatchlistDialogFragment createNewWatchlistDialogFragment = CreateNewWatchlistDialogFragment.newInstance(ADD_TO_WATCHLIST_REQUEST_CODE);
        createNewWatchlistDialogFragment.setTargetFragment(this, ADD_TO_WATCHLIST_REQUEST_CODE);
        createNewWatchlistDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
        createNewWatchlistDialogFragment.show(fragmentTransaction, CREATE_NEW_WATCHLIST_DIALOG_TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_TO_WATCHLIST_REQUEST_CODE) {
            radioGroupWatchlists.removeAllViews();
            presenter.getWatchlists();
        }
    }

    @Override
    public void setPresenter(AddToWatchlistContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setRadioGroup(ArrayList<WatchlistPersist> watchlists) {
        for (WatchlistPersist watchlist : watchlists) {
            RadioButton button = createRadioBtn(watchlist);
            radioGroupWatchlists.addView(button);
        }
        radioGroupWatchlists.check(radioGroupWatchlists.getChildAt(0).getId());
    }

    private RadioButton createRadioBtn(WatchlistPersist watchlist) {
        RadioButton button = new RadioButton(getContext());
        button.setId(View.generateViewId());
        button.setText(watchlist.getName());
        return button;
    }

    @Override
    public void showError(Integer messageId) {
        dismiss();
        loadErrorDialog(getString(messageId));
    }

    @Override
    public void loadSuccessMessage(String watchlistName, String title, boolean isAlreadyAdded) {
        if (getTargetFragment() != null) {
            Intent intent = new Intent();
            intent.putExtra(WATCHLIST_NAME_BUNDLE_KEY, presenter.getSelectedWatchlistName());
            intent.putExtra(WATCHABLE_NAME_BUNDLE_KEY, presenter.getWatchableName());
            intent.putExtra(IS_ADDED, isAlreadyAdded);
            getTargetFragment().onActivityResult(getTargetRequestCode(), requestCode, intent);
        }
    }

    @Override
    public void onDestroy() {
        presenter.cancelTasks();
        super.onDestroy();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int radioBtnId) {
        View selectedChild = radioGroup.findViewById(radioBtnId);
        int childIndex = radioGroup.indexOfChild(selectedChild);
        presenter.onWatchlistClicked(childIndex);
    }
}