package com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.creator;

import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.TvSeasonEpisodesListFragment;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.base.BaseFragmentFactory;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.product.SeasonDetailsProductCreator;

import java.util.ArrayList;

public class TvSeasonFragmentFactory extends BaseFragmentFactory {

    @Override
    public TvSeasonEpisodesListFragment createSeasonDetailsFragment(ArrayList<? extends Watchable> content,
                                                                    Integer seasonId,
                                                                    Integer seasonNumber,
                                                                    Integer tvSeriesId,
                                                                    String tvSeriesTitle,
                                                                    Integer numberOfSeasons) {
        return new SeasonDetailsProductCreator(content, seasonId, seasonNumber, tvSeriesId, numberOfSeasons, tvSeriesTitle).createSeasonDetailsFragment();
    }

}
