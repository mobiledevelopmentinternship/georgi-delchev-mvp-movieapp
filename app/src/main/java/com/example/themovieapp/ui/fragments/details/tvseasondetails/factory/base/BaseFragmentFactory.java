package com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.base;

import android.support.v4.app.Fragment;

import com.example.themovieapp.persistance.models.markers.Watchable;

import java.util.ArrayList;

public abstract class BaseFragmentFactory {

    public abstract Fragment createSeasonDetailsFragment(ArrayList<? extends Watchable> content,
                                                         Integer seasonId,
                                                         Integer seasonNumber,
                                                         Integer tvSeriesId, String tvSeriesTitle, Integer numberOfSeasons);
}
