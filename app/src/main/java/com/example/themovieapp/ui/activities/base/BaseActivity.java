package com.example.themovieapp.ui.activities.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.themovieapp.R;
import com.example.themovieapp.presenters.discover.DiscoverPresenter;
import com.example.themovieapp.presenters.watchlist.WatchlistsPresenter;
import com.example.themovieapp.ui.fragments.discover.DiscoverFragment;
import com.example.themovieapp.ui.fragments.home.HomeFragment;
import com.example.themovieapp.ui.fragments.watchlist.WatchlistsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public abstract class BaseActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.bottom_navigation)
    protected BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @LayoutRes
    protected int getLayoutResId() {
        return R.layout.activity_base;
    }

    protected void commitFragmentTransaction(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_content, fragment)
                .commit();
    }

    protected void setBottomNavigationVisibility(boolean isVisible) {
        bottomNavigationView.setVisibility(isVisible ? VISIBLE : GONE);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_item_home:
                commitFragmentTransaction(HomeFragment.newInstance());
                break;
            case R.id.menu_item_discover:
                DiscoverFragment discoverFragment = DiscoverFragment.newInstance();
                new DiscoverPresenter(discoverFragment);
                commitFragmentTransaction(discoverFragment);
                break;
            case R.id.menu_item_watchlists:
                WatchlistsFragment watchlistsFragment = WatchlistsFragment.newInstance();
                new WatchlistsPresenter(watchlistsFragment);
                commitFragmentTransaction(watchlistsFragment);
                break;
        }
        return true;
    }
}