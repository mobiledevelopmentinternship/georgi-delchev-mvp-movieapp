package com.example.themovieapp.ui.dialogs.addtowatchlist.presenter;

import android.text.TextUtils;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.MoviePersistService;
import com.example.themovieapp.persistance.services.TvSeriesPersistService;
import com.example.themovieapp.persistance.services.WatchlistPersistService;
import com.example.themovieapp.ui.dialogs.addtowatchlist.presenter.contracts.AddToWatchlistContracts;
import com.example.themovieapp.ui.dialogs.base.BaseDialogPresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.operators.completable.CompletableFromAction;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

import static com.example.themovieapp.utils.Utils.extractIds;

public class AddToWatchlistPresenter extends BaseDialogPresenter implements AddToWatchlistContracts.Presenter {

    private final AddToWatchlistContracts.View view;
    @Inject
    MoviePersistService moviePersistService;
    @Inject
    TvSeriesPersistService tvSeriesPersistService;
    @Inject
    WatchlistPersistService watchlistPersistService;

    private MoviePersist movie;
    private TvSeriesPersist tvSeries;
    private ArrayList<WatchlistPersist> watchlists;
    private Integer watchlistPosition;
    private String selectedWatchlistName;
    private final WatchlistPersist defaultWatchlist;
    private Disposable insertDisposable;

    @Inject
    public AddToWatchlistPresenter(AddToWatchlistContracts.View view) {
        this.view = view;
        view.setPresenter(this);
        watchlists = new ArrayList<>();
        defaultWatchlist = new WatchlistPersist(App.getInstance()
                .getString(R.string.watchlists_default_watchlist_name));
    }

    @Override
    public void setContent(Watchable content) {
        if (content instanceof MoviePersist) {
            movie = (MoviePersist) content;
        } else {
            tvSeries = (TvSeriesPersist) content;
        }
    }

    private void setWatchlists(ArrayList<WatchlistPersist> watchlists) {
        this.watchlists = watchlists;
        Collections.sort(watchlists);
        view.setRadioGroup(this.watchlists);
    }

    @Override
    public void addToWatchlist() {
        if (watchlistPosition == null) {
            view.showError(R.string.error_add_to_watchlist_no_watchlist_selected);
        } else {
            if (movie != null) {
                isMovieInWatchlist();
            } else {
                isTvSeriesInWatchlist();
            }
        }
    }

    private void isTvSeriesInWatchlist() {
        subscribeSingle(
                watchlistPersistService.getTvSeriesByWatchlistId(watchlists.get(watchlistPosition).getId()),
                this::onTvSeriesByWatchlistIdQuerySuccess,
                this::onTvSeriesByWatchlistIdQueryError
        );
    }

    private void onTvSeriesByWatchlistIdQueryError(Throwable throwable) {
        view.showError(R.string.add_to_watchlist_cant_be_added);
    }

    private void onTvSeriesByWatchlistIdQuerySuccess(List<TvSeriesPersist> allTvSeriesInWatchlist) {
        List<Integer> tvSeriesIds = new ArrayList<>();
        if (!allTvSeriesInWatchlist.isEmpty()) {
            tvSeriesIds = extractIds(allTvSeriesInWatchlist);
        }

        if (!tvSeriesIds.isEmpty() && tvSeriesIds.contains(tvSeries.getId())) {
            view.loadSuccessMessage(watchlists.get(watchlistPosition).getName(),
                    tvSeries.getTitle(), true);
        } else {
            tvSeries.setWatchlistId(watchlists.get(watchlistPosition).getId());
            insertTvSeries();
        }
    }

    private void isMovieInWatchlist() {
        subscribeSingle(
                watchlistPersistService
                        .getMoviesByWatchlistId(watchlists.get(watchlistPosition).getId()),
                this::onMoviesByWatchlistIdQuerySuccess,
                this::onMoviesByWatchlistIdQueryError
        );
    }

    private void onMoviesByWatchlistIdQueryError(Throwable throwable) {
        view.showError(R.string.add_to_watchlist_cant_be_added);
    }

    private void onMoviesByWatchlistIdQuerySuccess(List<MoviePersist> movies) {
        List<Integer> movieIds = new ArrayList<>();
        if (!movies.isEmpty()) {
            movieIds = extractIds(movies);
        }

        if (!movieIds.isEmpty() && movieIds.contains(movie.getId())) {
            view.loadSuccessMessage(watchlists.get(watchlistPosition).getName(),
                    movie.getTitle(), true);
        } else {
            movie.setWatchlistId(watchlists.get(watchlistPosition).getId());
            insertMovie();
        }
    }

    private void insertTvSeries() {
        CompletableFromAction comp = new CompletableFromAction(() -> tvSeriesPersistService.update(tvSeries));

        insertDisposable = comp
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> insertDisposable.dispose())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        view.loadSuccessMessage(
                                watchlists.get(watchlistPosition).getName(),
                                tvSeries.getTitle(),
                                false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(R.string.add_to_watchlist_cant_be_added);
                    }
                });
    }

    private void insertMovie() {
        CompletableFromAction comp = new CompletableFromAction(() -> moviePersistService.update(movie));

        insertDisposable = comp
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> insertDisposable.dispose())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        view.loadSuccessMessage(
                                watchlists.get(watchlistPosition).getName(),
                                movie.getTitle(),
                                false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(R.string.add_to_watchlist_cant_be_added);
                    }
                });
    }

    @Override
    public void getWatchlists() {
        subscribeSingle(
                watchlistPersistService.getAllWatchlists(),
                this::onQuerySuccess,
                this::onQueryError
        );
    }

    private void onQueryError(Throwable throwable) {
        createDefaultWatchlist();
    }

    private void onQuerySuccess(List<WatchlistPersist> watchlists) {
        if (watchlists.isEmpty()) {
            createDefaultWatchlist();
        } else {
            this.watchlists.clear();
            setWatchlists((ArrayList<WatchlistPersist>) watchlists);
        }

        checkForWatchlistsWithoutName(watchlists);
    }

    private void checkForWatchlistsWithoutName(List<WatchlistPersist> watchlists) {
        for (WatchlistPersist watchlist : watchlists) {
            if (TextUtils.equals(watchlist.getName(), "")) {
                watchlists.remove(watchlist);
            }
        }
    }

    private void createDefaultWatchlist() {
        subscribeCompletable(
                createCompletable(() -> watchlistPersistService.insert(defaultWatchlist)),
                this::getWatchlists);
    }

    @Override
    public void onWatchlistClicked(Integer position) {
        this.watchlistPosition = position;
        selectedWatchlistName = watchlists.get(watchlistPosition).getName();
    }

    @Override
    public void cancelTasks() {
        dispose();
    }

    @Override
    public String getSelectedWatchlistName() {
        return selectedWatchlistName;
    }

    @Override
    public String getWatchableName() {
        if (movie != null) {
            return movie.getTitle();
        } else {
            return tvSeries.getTitle();
        }
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}