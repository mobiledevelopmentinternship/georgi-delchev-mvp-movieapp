package com.example.themovieapp.ui.fragments.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.pageradapters.BasicTabLayoutAdapter;
import com.example.themovieapp.presenters.home.moviehome.MovieHomePresenter;
import com.example.themovieapp.presenters.home.tvserieshome.TVSeriesHomePresenter;
import com.example.themovieapp.ui.fragments.base.BaseFragment;
import com.example.themovieapp.ui.fragments.home.movie.MovieHomeFragment;
import com.example.themovieapp.ui.fragments.home.tvseries.TVSeriesHomeFragment;
import com.example.themovieapp.utils.Constants;

import butterknife.BindView;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.view_pager_home_screen)
    ViewPager viewPager;
    @BindView(R.id.tab_layout_home)
    TabLayout tabLayoutHome;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BasicTabLayoutAdapter tabAdapter = new BasicTabLayoutAdapter(getChildFragmentManager());
        tabAdapter.addFragment(loadMovieHomeFragment(), Constants.MOVIES_TAB_TITLE);
        tabAdapter.addFragment(loadTvSeriesHomeFragment(), Constants.TV_SERIES_TAB_TITLE);

        viewPager.setAdapter(tabAdapter);
        tabLayoutHome.setupWithViewPager(viewPager);
    }

    private Fragment loadTvSeriesHomeFragment() {
        TVSeriesHomeFragment tvSeriesHomeFragment = TVSeriesHomeFragment.newInstance();
        new TVSeriesHomePresenter(tvSeriesHomeFragment);
        return tvSeriesHomeFragment;
    }

    private Fragment loadMovieHomeFragment() {
        MovieHomeFragment movieHomeFragment = MovieHomeFragment.newInstance();
        new MovieHomePresenter(movieHomeFragment);
        return movieHomeFragment;
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.fragment_home;
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }
}