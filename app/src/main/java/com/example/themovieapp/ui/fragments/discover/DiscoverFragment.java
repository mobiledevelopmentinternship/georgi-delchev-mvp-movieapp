package com.example.themovieapp.ui.fragments.discover;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.search.DiscoverResultAdapter;
import com.example.themovieapp.networking.retrofit.response.search.SearchResponse;
import com.example.themovieapp.presenters.discover.contracts.DiscoverContracts;
import com.example.themovieapp.ui.activities.moviedetails.MovieDetailsActivity;
import com.example.themovieapp.ui.activities.tvseriesdetails.TvSeriesDetailsActivity;
import com.example.themovieapp.ui.fragments.base.BaseFragment;
import com.example.themovieapp.view.ProgressLayout;
import com.paginate.Paginate;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.themovieapp.utils.Constants.CURRENT_PAGE_INITIAL_VALUE;
import static com.example.themovieapp.utils.Constants.DISCOVERED_MOVIE_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.DISCOVERED_TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MAX_PAGES_INITIAL_VALUE;
import static com.example.themovieapp.utils.Constants.PAGE_THRESHOLD;

public class DiscoverFragment extends BaseFragment implements DiscoverContracts.View, Paginate.Callbacks {

    private DiscoverContracts.Presenter presenter;

    @BindView(R.id.progress_layout_discover)
    ProgressLayout progressLayout;
    @BindView(R.id.toolbar_discover)
    Toolbar toolbarDiscover;
    @BindView(R.id.recycler_view_discovery_result)
    RecyclerView recyclerViewDiscoveryResult;
    @BindView(R.id.discover_search_view)
    SearchView searchViewDiscover;

    private DiscoverResultAdapter discoverAdapter;
    private Paginate paginate;
    private Integer currentPage = CURRENT_PAGE_INITIAL_VALUE;
    private Integer maxPages = MAX_PAGES_INITIAL_VALUE;
    private boolean isLoading;

    @Override
    protected Integer getLayoutResId() {
        return R.layout.fragment_discover;
    }

    public static DiscoverFragment newInstance() {
        return new DiscoverFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.start();

        setupSearchView();
    }

    private void setupSearchView() {
        EditText searchViewEditText = searchViewDiscover.findViewById(R.id.search_src_text);
        View searchViewUnderline = searchViewDiscover.findViewById(R.id.search_plate);
        ImageView searchViewCloseButton = searchViewDiscover.findViewById(R.id.search_close_btn);

        searchViewUnderline.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.MULTIPLY);
        searchViewEditText.setTextColor(getResources().getColor(R.color.colorText));
        searchViewEditText.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        searchViewEditText.setHint(getString(R.string.fragment_discover_search_view_hint));
        searchViewEditText.setHintTextColor(getResources().getColor(R.color.colorText));

        searchViewCloseButton.setOnClickListener(view -> {
            if (searchViewEditText.getText() != null) {
                presenter.onQueryCleared();
                searchViewEditText.requestFocus();
            } else {
                presenter.onStopButtonClick(true);
                setProgressBarVisibility(false);
            }

            searchViewEditText.setText("");
        });

        setQueryListener(searchViewUnderline);
    }

    private void setQueryListener(View searchViewUnderline) {
        searchViewDiscover.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.discover(query.trim());
                searchViewDiscover.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (!TextUtils.isEmpty(query.trim())) {
                    searchViewUnderline.getBackground().setColorFilter(getResources()
                                    .getColor(R.color.colorDiscoverSearchViewUnderline),
                            PorterDuff.Mode.MULTIPLY);
                } else {
                    searchViewUnderline.getBackground().setColorFilter(getResources()
                                    .getColor(R.color.colorAccent),
                            PorterDuff.Mode.MULTIPLY);
                }
                return false;
            }
        });
    }

    @Override
    public void setPresenter(DiscoverContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        if (isAdded()) {
            progressLayout.setLoading(isVisible);
        }
    }

    @Override
    public void setResultListVisibility(boolean isVisible) {
        if (isAdded()) {
            recyclerViewDiscoveryResult.setVisibility(isVisible ? VISIBLE : GONE);
        }
    }

    @Override
    public void setResultsAdapter(List<SearchResponse> result) {
        if (paginate != null && discoverAdapter != null) {
            paginate.unbind();
            discoverAdapter = null;
        }

        discoverAdapter = new DiscoverResultAdapter(result, presenter);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerViewDiscoveryResult.setAdapter(discoverAdapter);
        recyclerViewDiscoveryResult.setLayoutManager(manager);

        if (result.size() == PAGE_THRESHOLD) {
            paginate = Paginate.with(recyclerViewDiscoveryResult, this)
                    .setLoadingTriggerThreshold(PAGE_THRESHOLD)
                    .addLoadingListItem(true)
                    .build();
        }

        presenter.showResults();
    }

    @Override
    public void loadMovieDetails(Integer movieId) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
            intent.putExtra(DISCOVERED_MOVIE_ID_BUNDLE_KEY, movieId);

            startActivity(intent);
        }
    }

    @Override
    public void loadTvSeriesDetails(Integer tvSeriesId) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), TvSeriesDetailsActivity.class);
            intent.putExtra(DISCOVERED_TV_SERIES_ID_BUNDLE_KEY, tvSeriesId);

            startActivity(intent);
        }
    }

    @Override
    public void showError(Integer messageId) {
        if (isAdded()) {
            loadErrorDialog(getString(messageId));
        }
    }

    @Override
    public void addResults(List<SearchResponse> result) {
        if (isAdded()) {
            discoverAdapter.updateResults(result);
            presenter.showResults();
        }
    }

    @Override
    public void setMaxPagesForQuery(Integer maxPages) {
        this.maxPages = maxPages;
    }

    @Override
    public void incrementCurrentPageNumber() {
        if (isAdapterAvailable()) {
            currentPage++;
        }
    }

    @Override
    public void resetPagesForNewQuery() {
        if (isAdapterAvailable()) {
            maxPages = MAX_PAGES_INITIAL_VALUE;
            currentPage = CURRENT_PAGE_INITIAL_VALUE;
        }
    }

    private boolean isAdapterAvailable() {
        return discoverAdapter != null;
    }

    @Override
    public void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    @Override
    public void onLoadMore() {
        if (discoverAdapter.hasReachedTheEndOfCurrentList()) {
            presenter.loadMoreResults();
            setLoading(true);
        } else {
            setLoading(false);
        }
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return !discoverAdapter.hasReachedTheEndOfCurrentList() && currentPage <= maxPages;
    }
}