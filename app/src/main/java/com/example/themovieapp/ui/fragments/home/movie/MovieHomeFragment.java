package com.example.themovieapp.ui.fragments.home.movie;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.example.themovieapp.adapters.poster.PosterAdapter;
import com.example.themovieapp.presenters.home.base.BaseHomeContracts;
import com.example.themovieapp.presenters.home.moviehome.contracts.MovieHomeContracts;
import com.example.themovieapp.ui.activities.moviedetails.MovieDetailsActivity;
import com.example.themovieapp.ui.fragments.home.BaseHomeFragment;
import com.example.themovieapp.utils.Category;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.themovieapp.utils.Constants.MEDIA_INTENT_TAG;
import static com.example.themovieapp.utils.Constants.MEDIA_TYPE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MOVIE_ID_BUNDLE_KEY;

public class MovieHomeFragment extends BaseHomeFragment implements MovieHomeContracts.View {

    private MovieHomeContracts.Presenter presenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.start();
    }

    @Override
    public void setupTopRatedAdapter(Category category) {
        if (isAdded()) {
            if (presenter.getTopRated().isEmpty()) {
                cardViewTopRatedCategory.setVisibility(GONE);
            } else {
                PosterAdapter topRatedAdapter = new PosterAdapter(presenter, category);
                LinearLayoutManager topRatedLayoutManager = new LinearLayoutManager(getContext());
                topRatedLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerViewTopRatedCategory.setAdapter(topRatedAdapter);
                recyclerViewTopRatedCategory.setLayoutManager(topRatedLayoutManager);
                topRatedAdapter.notifyDataSetChanged();
            }
            presenter.setTopRatedAdapterFlag();
        }
    }

    @Override
    public void setupMostPopularAdapter(Category category) {
        if (isAdded()) {
            if (presenter.getMostPopular().isEmpty()) {
                cardViewMostPopularCategory.setVisibility(GONE);
            } else {
                PosterAdapter mostPopularAdapter = new PosterAdapter(presenter, category);
                LinearLayoutManager mostPopularLayoutManager = new LinearLayoutManager(getContext());
                mostPopularLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerViewMostPopularCategory.setAdapter(mostPopularAdapter);
                recyclerViewMostPopularCategory.setLayoutManager(mostPopularLayoutManager);
                mostPopularAdapter.notifyDataSetChanged();
            }
            presenter.setMostPopularAdapterFlag();
        }
    }

    @Override
    public void setupUpcomingAdapter(Category category) {
        if (isAdded()) {
            if (presenter.getUpcoming().isEmpty()) {
                cardViewUpcomingCategory.setVisibility(GONE);
            } else {
                PosterAdapter upcomingAdapter = new PosterAdapter(presenter, category);
                LinearLayoutManager upcomingLayoutManager = new LinearLayoutManager(getContext());
                upcomingLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerViewUpcomingCategory.setLayoutManager(upcomingLayoutManager);
                recyclerViewUpcomingCategory.setAdapter(upcomingAdapter);
                upcomingAdapter.notifyDataSetChanged();
            }
            presenter.setUpcomingAdapterFlag();
        }
    }

    @Override
    public void loadSelectedMovie(Integer movieId, Integer mediaTypeId) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(MOVIE_ID_BUNDLE_KEY, movieId);
            bundle.putString(MEDIA_TYPE_BUNDLE_KEY, getString(mediaTypeId));
            intent.putExtra(MEDIA_INTENT_TAG, bundle);

            startActivity(intent);
        }
    }

    @Override
    public void showError(Integer messageId) {
        if (isAdded()) {
            loadErrorDialog(getString(messageId));
        }
    }

    public static MovieHomeFragment newInstance() {
        return new MovieHomeFragment();
    }

    @Override
    public void setPresenter(BaseHomeContracts.Presenter presenter) {
        this.presenter = (MovieHomeContracts.Presenter) presenter;
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        if (isAdded()) {
            progressLayoutHome.setLoading(isVisible);
            nestedScrollViewHome.setVisibility(isVisible ? GONE : VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        presenter.cancelRequests();
        super.onDestroy();
    }
}