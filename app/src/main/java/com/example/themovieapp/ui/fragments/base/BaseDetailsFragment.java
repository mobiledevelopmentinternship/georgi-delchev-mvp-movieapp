package com.example.themovieapp.ui.fragments.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.screencast.ScreenCastAdapter;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.ui.activities.moviedetails.MovieDetailsActivity;
import com.example.themovieapp.ui.activities.tvseriesdetails.TvSeriesDetailsActivity;
import com.example.themovieapp.utils.PicassoImageLoader;
import com.example.themovieapp.view.ProgressLayout;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.themovieapp.utils.Constants.MEDIA_INTENT_TAG;
import static com.example.themovieapp.utils.Constants.MEDIA_TYPE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MOVIE_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;

public abstract class BaseDetailsFragment extends BaseFragment implements BaseDetailsContracts.View {

    @BindView(R.id.progress_layout_details)
    protected ProgressLayout progressLayout;
    @BindView(R.id.view_pager_details_episode_images)
    protected ViewPager viewPager;
    @BindView(R.id.txt_details_name)
    TextView txtName;
    @BindView(R.id.txt_details_release_date)
    TextView txtReleaseDate;
    @BindView(R.id.txt_details_media_type)
    TextView txtMediaType;
    @BindView(R.id.img_details_overview_poster)
    ImageView imgOverviewCardPoster;
    @BindView(R.id.txt_details_overview)
    TextView txtOverview;
    @BindView(R.id.btn_details_all_episodes)
    protected Button btnAllEpisode;
    @BindView(R.id.recycler_view_details_episode_cast)
    protected RecyclerView recyclerViewEpisodeCast;
    @BindView(R.id.txt_details_other_information_title)
    TextView txtOtherInfoTitle;
    @BindView(R.id.recycler_view_details_additional_information)
    protected RecyclerView recyclerViewAdditionalInformation;
    @BindView(R.id.view_group_details)
    Group viewGroup;
    @BindView(R.id.btn_details_add_to_collection)
    Button btnAddToCollection;
    @BindView(R.id.btn_details_rate)
    Button btnRate;
    @BindView(R.id.card_view_details_members)
    protected CardView cardViewMembers;
    @BindView(R.id.card_view_details_other_information)
    protected CardView cardViewAdditionalInformation;

    protected ScreenCastAdapter screenCastAdapter;

    @Override
    public void setName(String name) {
        if (isAdded()) {
            txtName.setText(name);
        }
    }

    protected void showRatingSnackbar() {
        if (isAdded() && getActivity() != null) {
            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    getString(R.string.dialog_rating_congratulations_message),
                    Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    protected void showAddToWatchlistSnackbar(String name, String watchlistName, boolean isAlreadyAdded) {
        if (isAdded() && getActivity() != null) {
            if (isAlreadyAdded) {
                Snackbar.make(
                        getActivity().findViewById(android.R.id.content),
                        TextUtils.concat(name," ",
                                getString(R.string.dialog_add_to_watchlist_already_added)," ",watchlistName),
                        Snackbar.LENGTH_LONG)
                        .show();
            } else {
                Snackbar.make(
                        getActivity().findViewById(android.R.id.content),
                        TextUtils.concat(name," ",
                                getString(R.string.dialog_add_to_watchlist_is_added_to)," ",watchlistName),
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }

    @Override
    public void setReleaseDate(String date) {
        if (isAdded()) {
            txtReleaseDate.setText(date);
        }
    }

    @Override
    public void setMediaType(Integer mediaTypeId) {
        if (isAdded()) {
            txtMediaType.setText(getString(mediaTypeId));
        }
    }

    @Override
    public void setPosterImage(String url) {
        if (isAdded()) {
            PicassoImageLoader.setImage(imgOverviewCardPoster, url);
        }
    }

    @Override
    public void setOverview(String overview) {
        if (isAdded()) {
            txtOverview.setText(overview);
        }
    }

    @Override
    public void setAdditionalInformationTitle(Integer titleId) {
        if (isAdded()) {
            txtOtherInfoTitle.setText(getString(titleId));
        }
    }

    @Override
    public void showSimilarWatchable(Integer id, Integer mediaTypeId) {
        if (isAdded()) {
            Intent intent;
            Bundle bundle = new Bundle();
            String type = getString(mediaTypeId);
            if (mediaTypeId.equals(R.string.tv_series_media_type)) {
                bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, id);
                intent = new Intent(getActivity(), TvSeriesDetailsActivity.class);
            } else {
                bundle.putInt(MOVIE_ID_BUNDLE_KEY, id);
                intent = new Intent(getActivity(), MovieDetailsActivity.class);
            }

            bundle.putString(MEDIA_TYPE_BUNDLE_KEY, type);
            intent.putExtra(MEDIA_INTENT_TAG, bundle);
            startActivity(intent);
            resetSimilarWatchableId();
        }
    }

    @Override
    public void setProgressBarVisibility(boolean isVisible) {
        progressLayout.setLoading(isVisible);
        viewGroup.setVisibility(isVisible ? GONE : VISIBLE);
        btnRate.setVisibility(isVisible ? GONE : VISIBLE);
        btnAddToCollection.setVisibility(isVisible ? GONE : VISIBLE);
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.fragment_details;
    }

    @Override
    public void setAllEpisodesButtonVisibility(boolean isVisible) {
        btnAllEpisode.setVisibility(isVisible ? VISIBLE : GONE);
    }

    protected abstract void resetSimilarWatchableId();

    @Override
    public void hideSimilarMedia() {
        cardViewAdditionalInformation.setVisibility(GONE);
    }
}