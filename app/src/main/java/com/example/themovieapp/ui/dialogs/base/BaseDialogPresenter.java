package com.example.themovieapp.ui.dialogs.base;

import com.example.themovieapp.presenters.base.BasePresenter;

public abstract class BaseDialogPresenter extends BasePresenter {
    public BaseDialogPresenter() {
        super();
    }
}
