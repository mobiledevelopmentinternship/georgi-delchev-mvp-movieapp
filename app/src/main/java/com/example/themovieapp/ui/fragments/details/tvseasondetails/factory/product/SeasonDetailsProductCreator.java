package com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.product;

import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.TvSeasonEpisodeListPresenter;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.TvSeasonEpisodesListFragment;

import java.util.ArrayList;

public class SeasonDetailsProductCreator {

    private ArrayList<EpisodePersist> episodes;
    private Integer seasonId;
    private Integer seasonNumber;
    private Integer tvSeriesId;
    private Integer allSeasonsCount;
    private String title;

    public SeasonDetailsProductCreator(ArrayList<? extends Watchable> episodes,
                                       Integer seasonId,
                                       Integer seasonNumber,
                                       Integer tvSeriesId,
                                       Integer allSeasonsCount,
                                       String tvSeriesTitle) {
        this.episodes = (ArrayList<EpisodePersist>) episodes;
        this.seasonId = seasonId;
        this.seasonNumber = seasonNumber;
        this.tvSeriesId = tvSeriesId;
        this.allSeasonsCount = allSeasonsCount;
        this.title = tvSeriesTitle;
    }

    public TvSeasonEpisodesListFragment createSeasonDetailsFragment() {
        TvSeasonEpisodesListFragment tvSeasonEpisodesListFragment = TvSeasonEpisodesListFragment.newInstance();
        new TvSeasonEpisodeListPresenter(tvSeasonEpisodesListFragment,
                episodes, seasonId, seasonNumber, tvSeriesId, allSeasonsCount, title);

        return tvSeasonEpisodesListFragment;
    }
}