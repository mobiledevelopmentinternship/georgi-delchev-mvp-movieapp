package com.example.themovieapp.ui.dialogs.rating.presenter.contracts;

public interface RatingDialogContracts {

    interface View {

        void showError(Integer messageId);

        void showCongratulationsMessage(String message);

        void setPresenter(Presenter presenter);
    }

    interface Presenter {

        void setMediaType(String mediaType);

        void setMediaId(Integer id);

        void checkGuestSession();

        void setTitle(String title);

        void setRating(Integer rating);

        void cancelTasks();
    }
}
