package com.example.themovieapp.ui.activities.tvseasondetails;

import android.os.Bundle;

import com.example.themovieapp.ui.activities.base.BaseActivity;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.TvSeasonsFragment;

import static com.example.themovieapp.utils.Constants.NUMBER_OF_SEASON_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SEASONS_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_TITLE;

public class TvSeasonDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBottomNavigationVisibility(false);
        Bundle bundle = getIntent().getBundleExtra(TV_SEASONS_BUNDLE_KEY);
        if (bundle != null) {
            loadTvSeasonDetails(bundle.getInt(TV_SERIES_ID_BUNDLE_KEY),
                    bundle.getInt(NUMBER_OF_SEASON_BUNDLE_KEY),
                    bundle.getString(TV_SERIES_TITLE));
        }
    }

    private void loadTvSeasonDetails(Integer id, Integer numberOfSeasons, String title) {
        TvSeasonsFragment fragment = TvSeasonsFragment.newInstance(id, numberOfSeasons, title);
        commitFragmentTransaction(fragment);
    }
}
