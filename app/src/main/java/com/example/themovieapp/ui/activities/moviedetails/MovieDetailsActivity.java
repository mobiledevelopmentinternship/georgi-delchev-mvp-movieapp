package com.example.themovieapp.ui.activities.moviedetails;

import android.os.Bundle;

import com.example.themovieapp.R;
import com.example.themovieapp.ui.activities.base.BaseActivity;
import com.example.themovieapp.ui.fragments.details.moviedetails.MovieDetailsFragment;

import static com.example.themovieapp.utils.Constants.DISCOVERED_ID_DEFAULT_VALUE;
import static com.example.themovieapp.utils.Constants.DISCOVERED_MOVIE_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MEDIA_INTENT_TAG;
import static com.example.themovieapp.utils.Constants.MEDIA_TYPE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.MOVIE_ID_BUNDLE_KEY;

public class MovieDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBottomNavigationVisibility(false);
        Bundle bundle = getIntent().getBundleExtra(MEDIA_INTENT_TAG);
        if (bundle != null) {
            loadMovieDetailsFragment(bundle.getInt(MOVIE_ID_BUNDLE_KEY),
                    bundle.getString(MEDIA_TYPE_BUNDLE_KEY));
        } else {
            if (getIntent().getExtras() != null
                    && getIntent().getExtras().containsKey(DISCOVERED_MOVIE_ID_BUNDLE_KEY)) {
                loadMovieDetailsFragment(getIntent().getIntExtra(DISCOVERED_MOVIE_ID_BUNDLE_KEY, DISCOVERED_ID_DEFAULT_VALUE),
                        getString(R.string.movie_media_type));
            }
        }
    }

    private void loadMovieDetailsFragment(Integer id, String mediaType) {
        MovieDetailsFragment movieDetailsFragment = MovieDetailsFragment.newInstance(id, mediaType);
        commitFragmentTransaction(movieDetailsFragment);
    }
}