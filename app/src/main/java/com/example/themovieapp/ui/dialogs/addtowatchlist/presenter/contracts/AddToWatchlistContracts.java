package com.example.themovieapp.ui.dialogs.addtowatchlist.presenter.contracts;

import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;

import java.util.ArrayList;

public interface AddToWatchlistContracts {
    interface View {

        void setPresenter(AddToWatchlistContracts.Presenter presenter);

        void setRadioGroup(ArrayList<WatchlistPersist> watchlists);

        void showError(Integer messageId);

        void loadSuccessMessage(String watchlistName, String name, boolean isAlreadyAdded);
    }

    interface Presenter {

        void setContent(Watchable content);

        void addToWatchlist();

        void getWatchlists();

        void onWatchlistClicked(Integer position);

        void cancelTasks();

        String getSelectedWatchlistName();

        String getWatchableName();
    }
}
