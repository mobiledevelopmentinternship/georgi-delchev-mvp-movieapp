package com.example.themovieapp.ui.dialogs.rating;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.ui.dialogs.base.BaseDialogFragment;
import com.example.themovieapp.ui.dialogs.rating.presenter.RatingDialogPresenter;
import com.example.themovieapp.ui.dialogs.rating.presenter.contracts.RatingDialogContracts;

import butterknife.BindView;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.FRAGMENT_REQUEST_CODE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.RATING_DIALOG_MEDIA_TYPE_KEY;
import static com.example.themovieapp.utils.Constants.RATING_DIALOG_MOVIE_ID_KEY;
import static com.example.themovieapp.utils.Constants.RATING_DIALOG_TITLE_KEY;

public class RatingDialogFragment extends BaseDialogFragment implements RatingDialogContracts.View, RatingBar.OnRatingBarChangeListener {

    @BindView(R.id.rating_bar)
    RatingBar rating;
    @BindView(R.id.btn_dialog_rating_rate)
    Button btnRate;
    @BindView(R.id.btn_dialog_rating_cancel)
    Button btnCancel;
    @BindView(R.id.txt_dialog_rating_subheading)
    TextView txtSubheading;

    private RatingDialogContracts.Presenter presenter;
    private Integer requestCode;

    public static RatingDialogFragment newInstance(Integer watchableId,
                                                   String mediaType,
                                                   String title,
                                                   Integer requestCode) {
        RatingDialogFragment ratingDialogFragment = new RatingDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(RATING_DIALOG_MOVIE_ID_KEY, watchableId);
        bundle.putString(RATING_DIALOG_MEDIA_TYPE_KEY, mediaType);
        bundle.putString(RATING_DIALOG_TITLE_KEY, title);
        bundle.putInt(FRAGMENT_REQUEST_CODE_BUNDLE_KEY, requestCode);
        ratingDialogFragment.setArguments(bundle);
        new RatingDialogPresenter(ratingDialogFragment);

        return ratingDialogFragment;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(RATING_DIALOG_MEDIA_TYPE_KEY)
                && getArguments().containsKey(RATING_DIALOG_MOVIE_ID_KEY)
                && getArguments().containsKey(RATING_DIALOG_TITLE_KEY)) {
            presenter.setMediaId((Integer) getArguments().get(RATING_DIALOG_MOVIE_ID_KEY));
            presenter.setMediaType((String) getArguments().get(RATING_DIALOG_MEDIA_TYPE_KEY));
            presenter.setTitle((String) getArguments().get(RATING_DIALOG_TITLE_KEY));
            requestCode = (Integer) getArguments().get(FRAGMENT_REQUEST_CODE_BUNDLE_KEY);

            getDialog().setTitle(getString(R.string.dialog_rating_title));
            txtSubheading.setText(TextUtils.concat(getString(R.string.dialog_rating_subheading)," ", (CharSequence) getArguments().get(RATING_DIALOG_TITLE_KEY)));
            rating.setOnRatingBarChangeListener(this);
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_fragment_rating;
    }

    @Override
    public void showError(Integer messageId) {
        dismiss();
        loadErrorDialog(getString(messageId));
    }

    @OnClick(R.id.btn_dialog_rating_cancel)
    public void onCancelClicked() {
        dismiss();
    }

    @OnClick(R.id.btn_dialog_rating_rate)
    public void onRateClicked() {
        presenter.checkGuestSession();
    }

    @Override
    public void showCongratulationsMessage(String message) {
        dismiss();
        if (getTargetFragment() != null) {
            getTargetFragment().onActivityResult(getTargetRequestCode(),requestCode,null);
        }
    }

    @Override
    public void setPresenter(RatingDialogContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDestroy() {
        presenter.cancelTasks();
        super.onDestroy();
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean isChanged) {
        if (isChanged) {
            btnRate.setEnabled(true);
            presenter.setRating((int) rating);
        }
    }
}