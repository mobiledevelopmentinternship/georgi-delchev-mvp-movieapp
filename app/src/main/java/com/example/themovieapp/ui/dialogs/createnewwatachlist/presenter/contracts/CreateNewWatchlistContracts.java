package com.example.themovieapp.ui.dialogs.createnewwatachlist.presenter.contracts;

import com.example.themovieapp.persistance.models.WatchlistPersist;

public interface CreateNewWatchlistContracts {

    interface View {

        void setPresenter(CreateNewWatchlistContracts.Presenter presenter);

        void loadError(Integer messageId);

        void loadCreatedWatchlist(WatchlistPersist watchlistPersist);
    }

    interface Presenter {

        void createNewWatchlist(String watchlistName);

        void cancelTasks();
    }
}
