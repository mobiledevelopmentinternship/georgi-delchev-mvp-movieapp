package com.example.themovieapp.ui.fragments.details.tvseriesdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.example.themovieapp.R;
import com.example.themovieapp.adapters.screencast.ScreenCastAdapter;
import com.example.themovieapp.adapters.similarmedia.MediaRecyclerAdapter;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.presenters.details.tv.tvseriesdetails.TvSeriesDetailsPresenter;
import com.example.themovieapp.presenters.details.tv.tvseriesdetails.contracts.TvSeriesDetailsContracts;
import com.example.themovieapp.ui.activities.tvseasondetails.TvSeasonDetailsActivity;
import com.example.themovieapp.ui.dialogs.addtowatchlist.AddToWatchlistDialogFragment;
import com.example.themovieapp.ui.dialogs.rating.RatingDialogFragment;
import com.example.themovieapp.ui.fragments.base.BaseDetailsFragment;

import butterknife.OnClick;

import static android.view.View.GONE;
import static android.widget.LinearLayout.HORIZONTAL;
import static com.example.themovieapp.utils.Constants.ADD_TO_WATCHLIST_DIALOG_TAG;
import static com.example.themovieapp.utils.Constants.ADD_TO_WATCHLIST_REQUEST_CODE;
import static com.example.themovieapp.utils.Constants.IS_ADDED;
import static com.example.themovieapp.utils.Constants.NUMBER_OF_SEASON_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.RATING_DIALOG_REQUEST_CODE;
import static com.example.themovieapp.utils.Constants.RATING_DIALOG_TAG;
import static com.example.themovieapp.utils.Constants.TV_SEASONS_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_ID_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.TV_SERIES_REQUEST_CODE;
import static com.example.themovieapp.utils.Constants.TV_SERIES_TITLE;
import static com.example.themovieapp.utils.Constants.WATCHABLE_NAME_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.WATCHLIST_NAME_BUNDLE_KEY;

public class TvSeriesDetailsFragment extends BaseDetailsFragment implements TvSeriesDetailsContracts.View {

    private TvSeriesDetailsContracts.Presenter presenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.start();

        viewPager.setVisibility(GONE);
    }

    public static TvSeriesDetailsFragment newInstance(Integer id, String mediaType) {
        TvSeriesDetailsFragment fragment = new TvSeriesDetailsFragment();
        new TvSeriesDetailsPresenter(fragment, id, mediaType);
        return fragment;
    }

    @Override
    public void setPresenter(BaseDetailsContracts.Presenter presenter) {
        this.presenter = (TvSeriesDetailsContracts.Presenter) presenter;
    }

    @Override
    public void setupScreenCastAdapter() {
        if (isAdded()) {
            screenCastAdapter = new ScreenCastAdapter(presenter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(HORIZONTAL);
            recyclerViewEpisodeCast.setLayoutManager(linearLayoutManager);
            recyclerViewEpisodeCast.setAdapter(screenCastAdapter);
            screenCastAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setupSimilarWatchablesAdapter() {
        if (isAdded()) {
            MediaRecyclerAdapter similarMediaAdapter = new MediaRecyclerAdapter(presenter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(HORIZONTAL);
            recyclerViewAdditionalInformation.setLayoutManager(linearLayoutManager);
            recyclerViewAdditionalInformation.setAdapter(similarMediaAdapter);
            similarMediaAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.btn_details_add_to_collection)
    public void addToCollectionClicked() {
        if (isAdded()) {
            presenter.addToCollection();
        }
    }

    @Override
    public void showError(Integer messageId) {
        if (isAdded()) {
            loadErrorDialog(getString(messageId));
        }
    }

    @OnClick(R.id.btn_details_rate)
    public void rateTvSeries() {
        if (isAdded()) {
            presenter.loadRating();
        }
    }

    @OnClick(R.id.btn_details_all_episodes)
    public void allEpisodesClicked() {
        if (isAdded()) {
            presenter.loadEpisodes();
        }
    }

    @Override
    public void showRatingDialog(Integer id, String mediaType, String title) {
        if (isAdded()) {
            if (getFragmentManager() == null) {
                return;
            }

            RatingDialogFragment ratingDialogFragment = RatingDialogFragment.newInstance(id,
                    mediaType,
                    title,
                    RATING_DIALOG_REQUEST_CODE);
            ratingDialogFragment.setTargetFragment(this, RATING_DIALOG_REQUEST_CODE);
            ratingDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
            ratingDialogFragment.show(getFragmentManager(), RATING_DIALOG_TAG);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RATING_DIALOG_REQUEST_CODE) {
            showRatingSnackbar();
        } else if (requestCode == ADD_TO_WATCHLIST_REQUEST_CODE) {
            String watchlistName = data.getStringExtra(WATCHLIST_NAME_BUNDLE_KEY);
            String tvSeries = data.getStringExtra(WATCHABLE_NAME_BUNDLE_KEY);
            boolean isAlreadyAdded = data.getBooleanExtra(IS_ADDED, false);

            showAddToWatchlistSnackbar(tvSeries, watchlistName, isAlreadyAdded);
        }
    }

    @Override
    public void showAddToCollectionDialog(Watchable watchable) {
        if (isAdded()) {
            if (getFragmentManager() == null) {
                return;
            }

            AddToWatchlistDialogFragment addToWatchlistDialogFragment = AddToWatchlistDialogFragment.newInstance(TV_SERIES_REQUEST_CODE, watchable);
            addToWatchlistDialogFragment.setTargetFragment(this, ADD_TO_WATCHLIST_REQUEST_CODE);
            addToWatchlistDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
            addToWatchlistDialogFragment.show(getFragmentManager(), ADD_TO_WATCHLIST_DIALOG_TAG);
        }
    }

    @Override
    public void showEpisodeLists(Integer tvSeriesId, Integer numberOfSeasons, String title) {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), TvSeasonDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(TV_SERIES_ID_BUNDLE_KEY, tvSeriesId);
            bundle.putInt(NUMBER_OF_SEASON_BUNDLE_KEY, numberOfSeasons);
            bundle.putString(TV_SERIES_TITLE, title);
            intent.putExtra(TV_SEASONS_BUNDLE_KEY, bundle);

            startActivity(intent);
        }
    }

    @Override
    public void onDestroy() {
        presenter.cancelTasks();
        super.onDestroy();
    }

    @Override
    protected void resetSimilarWatchableId() {
        presenter.resetSimilarWatchableId();
    }
}