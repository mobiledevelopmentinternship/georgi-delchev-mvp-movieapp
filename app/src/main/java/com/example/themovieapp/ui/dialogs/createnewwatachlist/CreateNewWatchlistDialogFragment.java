package com.example.themovieapp.ui.dialogs.createnewwatachlist;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.ui.dialogs.base.BaseDialogFragment;
import com.example.themovieapp.ui.dialogs.createnewwatachlist.presenter.CreateNewWatchlistPresenter;
import com.example.themovieapp.ui.dialogs.createnewwatachlist.presenter.contracts.CreateNewWatchlistContracts;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.example.themovieapp.utils.Constants.FRAGMENT_REQUEST_CODE_BUNDLE_KEY;
import static com.example.themovieapp.utils.Constants.WATCHLIST_INTENT_KEY;

public class CreateNewWatchlistDialogFragment extends BaseDialogFragment implements CreateNewWatchlistContracts.View {

    @BindView(R.id.edt_dialog_new_watchlist_name)
    EditText editTxtNewWatchlistName;

    private CreateNewWatchlistContracts.Presenter presenter;
    private Integer requestCode;

    public static CreateNewWatchlistDialogFragment newInstance(Integer requestCode) {
        CreateNewWatchlistDialogFragment createNewWatchlistDialogFragment = new CreateNewWatchlistDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_REQUEST_CODE_BUNDLE_KEY, requestCode);
        createNewWatchlistDialogFragment.setArguments(bundle);
        new CreateNewWatchlistPresenter(createNewWatchlistDialogFragment);

        return createNewWatchlistDialogFragment;
    }

    public static CreateNewWatchlistDialogFragment newInstance() {
        return new CreateNewWatchlistDialogFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(FRAGMENT_REQUEST_CODE_BUNDLE_KEY)) {
            this.requestCode = getArguments().getInt(FRAGMENT_REQUEST_CODE_BUNDLE_KEY);
            getDialog().setTitle(getString(R.string.dialog_create_new_watchlist_title));
        }
    }

    @OnTextChanged(R.id.edt_dialog_new_watchlist_name)
    public void onTextChanged(CharSequence name) {
        if (TextUtils.isEmpty(String.valueOf(name).trim())) {
            ViewCompat.setBackgroundTintList(editTxtNewWatchlistName, ColorStateList.valueOf(
                    getResources().getColor(R.color.colorAccent)
            ));
        } else {
            ViewCompat.setBackgroundTintList(editTxtNewWatchlistName, ColorStateList.valueOf(
                    getResources().getColor(R.color.colorDiscoverSearchViewUnderline)
            ));
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_fragment_create_new_watchlist;
    }

    @OnClick(R.id.btn_dialog_create_new_watchlist)
    public void createNewWatchlist() {
        presenter.createNewWatchlist(String.valueOf(editTxtNewWatchlistName.getText()));
        dismiss();
    }

    @OnClick(R.id.btn_dialog_cancel_watchlist_creation)
    public void returnToPreviousDialog() {
        dismiss();
    }

    @Override
    public void setPresenter(CreateNewWatchlistContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void loadError(Integer messageId) {
        loadErrorDialog(getString(messageId));
    }

    @Override
    public void loadCreatedWatchlist(WatchlistPersist watchlistPersist) {
        if (getTargetFragment() != null) {
            Intent intent = new Intent();
            intent.putExtra(WATCHLIST_INTENT_KEY, watchlistPersist);
            getTargetFragment().onActivityResult(getTargetRequestCode(), requestCode, intent);
        }
    }

    @Override
    public void onDestroy() {
        presenter.cancelTasks();
        super.onDestroy();
    }
}