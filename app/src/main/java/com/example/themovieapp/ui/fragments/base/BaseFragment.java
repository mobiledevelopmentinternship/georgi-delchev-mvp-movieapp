package com.example.themovieapp.ui.fragments.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.themovieapp.ui.dialogs.error.ErrorDialogFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.example.themovieapp.utils.Constants.ERROR_DIALOG_TAG;

public abstract class BaseFragment extends Fragment {

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResId(), container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @LayoutRes
    protected abstract Integer getLayoutResId();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    protected void loadErrorDialog(String message) {
        if (getFragmentManager() == null) {
            return;
        }

        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(message);
        errorDialogFragment.show(getFragmentManager(), ERROR_DIALOG_TAG);
    }

}
