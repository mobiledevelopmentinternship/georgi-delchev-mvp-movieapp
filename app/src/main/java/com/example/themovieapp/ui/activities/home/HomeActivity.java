package com.example.themovieapp.ui.activities.home;


import android.os.Bundle;

import com.example.themovieapp.ui.activities.base.BaseActivity;
import com.example.themovieapp.ui.fragments.home.HomeFragment;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadHomeContainerFragment();
        setBottomNavigationVisibility(true);
    }

    private void loadHomeContainerFragment() {
        HomeFragment homeContainerFragment = HomeFragment.newInstance();
        commitFragmentTransaction(homeContainerFragment);
    }
}
