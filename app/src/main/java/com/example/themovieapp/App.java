package com.example.themovieapp;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.example.themovieapp.dagger.components.AppComponent;
import com.example.themovieapp.dagger.components.DaggerAppComponent;
import com.example.themovieapp.dagger.modules.AppModule;
import com.example.themovieapp.dagger.modules.RoomModule;
import com.example.themovieapp.utils.NetworkUtils;
import com.example.themovieapp.utils.SharedPrefsUtil;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

public class App extends Application {

    private static App INSTANCE;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setNightTheme();
        INSTANCE = this;
        setUpPicasso();
    }

    private void setNightTheme() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
    }

    private void setUpPicasso() {
        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(this, Integer.MAX_VALUE))
                .memoryCache(new LruCache(this))
                .build();
        Picasso.setSingletonInstance(picasso);
    }

    public static App getInstance() {
        return INSTANCE;
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .roomModule(new RoomModule(this))
                    .build();
        }

        return appComponent;
    }

    public SharedPrefsUtil getSharedPreferences(String name, Integer mode) {
        return (SharedPrefsUtil) this.getApplicationContext().getSharedPreferences(name, mode);
    }
}