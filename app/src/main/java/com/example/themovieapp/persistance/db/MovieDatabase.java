package com.example.themovieapp.persistance.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.themovieapp.persistance.daos.CrewMemberDao;
import com.example.themovieapp.persistance.daos.EpisodeDao;
import com.example.themovieapp.persistance.daos.GuestStarDao;
import com.example.themovieapp.persistance.daos.ScreenCastDao;
import com.example.themovieapp.persistance.daos.SeasonDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.SeasonPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.converters.DateConverter;
import com.example.themovieapp.persistance.daos.MovieDao;
import com.example.themovieapp.persistance.daos.TvSeriesDao;
import com.example.themovieapp.persistance.daos.WatchlistsDao;

@Database(entities = {
        CrewMemberPersist.class,
        EpisodePersist.class,
        GuestStarPersist.class,
        MoviePersist.class,
        ScreenCastMemberPersist.class,
        SeasonPersist.class,
        TvSeriesPersist.class,
        WatchlistPersist.class
},version = 1, exportSchema = false)
@TypeConverters(value = DateConverter.class)
public abstract class MovieDatabase extends RoomDatabase {

    public abstract MovieDao movieDao();

    public abstract TvSeriesDao tvSeriesDao();

    public abstract WatchlistsDao watchlistsDao();

    public abstract SeasonDao seasonDao();

    public abstract EpisodeDao episodeDao();

    public abstract CrewMemberDao crewMemberDao();

    public abstract GuestStarDao guestStarDao();

    public abstract ScreenCastDao screenCastDao();
}