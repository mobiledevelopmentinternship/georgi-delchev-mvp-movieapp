package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.themovieapp.persistance.models.markers.Watchable;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys =
@ForeignKey(entity = WatchlistPersist.class,
        parentColumns = "list_id",
        childColumns = "watchlist_id",
        onDelete = CASCADE,
        onUpdate = CASCADE))
public class TvSeriesPersist implements Watchable {

    @PrimaryKey
    @ColumnInfo(name = "tv_id")
    private Integer id;
    @ColumnInfo(name = "poster_url")
    private String posterUrl;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "episode_count")
    private Integer episodeCount;
    @ColumnInfo(name = "overview")
    private String overview;
    @ColumnInfo(name = "season_count")
    private Integer seasonCount;
    @ColumnInfo(name = "vote_average")
    private Double voteAverage;
    @ColumnInfo(name = "popularity")
    private Double popularity;
    @ColumnInfo(name = "airing_date")
    private Date airingDate;
    @ColumnInfo(name = "watchlist_id", index = true)
    private Long watchlistId;
    @Ignore
    public static final Parcelable.Creator CREATOR = new Creator<TvSeriesPersist>() {
        @Override
        public TvSeriesPersist createFromParcel(Parcel source) {
            return new TvSeriesPersist(source);
        }

        @Override
        public TvSeriesPersist[] newArray(int size) {
            return new TvSeriesPersist[size];
        }
    };

    public TvSeriesPersist(Integer id, String posterUrl, String title, Integer episodeCount, String overview, Integer seasonCount, Double voteAverage, Double popularity, Date airingDate, Long watchlistId) {
        this.id = id;
        this.posterUrl = posterUrl;
        this.title = title;
        this.episodeCount = episodeCount;
        this.overview = overview;
        this.seasonCount = seasonCount;
        this.voteAverage = voteAverage;
        this.popularity = popularity;
        this.airingDate = airingDate;
        this.watchlistId = watchlistId;
    }

    @Ignore
    public TvSeriesPersist(){}

    public Integer getId() {
        return id;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public Integer getEpisodeCount() {
        return episodeCount;
    }

    public String getOverview() {
        return overview;
    }

    public Long getWatchlistId() {
        return watchlistId;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public Double getPopularity() {
        return popularity;
    }

    public Date getAiringDate() {
        return airingDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setEpisodeCount(Integer episodeCount) {
        this.episodeCount = episodeCount;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public void setAiringDate(Date airingDate) {
        this.airingDate = airingDate;
    }

    public void setWatchlistId(Long watchlistId) {
        this.watchlistId = watchlistId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    private TvSeriesPersist(Parcel source) {
        this.id = source.readInt();
        this.overview = source.readString();
        this.posterUrl = source.readString();
        this.title = source.readString();
        this.seasonCount = source.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.overview);
        dest.writeString(this.posterUrl);
        dest.writeString(this.title);
        dest.writeInt(this.seasonCount);
    }

    public Integer getSeasonCount() {
        return seasonCount;
    }

    public void setSeasonCount(Integer seasonCount) {
        this.seasonCount = seasonCount;
    }

    public static class Builder {

        private Integer id;
        private String posterUrl;
        private String title;
        private Integer episodeCount;
        private String overview;
        private Double voteAverage;
        private Double popularity;
        private Date airingDate;
        private Long watchlistId;
        private Integer seasonCount;

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder withSeasonCount(Integer seasonCount) {
            this.seasonCount = seasonCount;
            return this;
        }

        public Builder withPosterUrl(String posterUrl) {
            this.posterUrl = posterUrl;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withEpisodeCount(Integer episodeCount) {
            this.episodeCount = episodeCount;
            return this;
        }

        public Builder withOverview(String overview) {
            this.overview = overview;
            return this;
        }

        public Builder withVoteAverage(Double voteAverage) {
            this.voteAverage = voteAverage;
            return this;
        }

        public Builder withPopularity(Double popularity) {
            this.popularity = popularity;
            return this;
        }

        public Builder withAiringDate(Date airingDate) {
            this.airingDate = airingDate;
            return this;
        }

        public Builder withWatchlistId(Long watchlistId) {
            this.watchlistId = watchlistId;
            return this;
        }

        public TvSeriesPersist build() {
            TvSeriesPersist tvSeriesPersist = new TvSeriesPersist();
            tvSeriesPersist.airingDate = this.airingDate;
            tvSeriesPersist.episodeCount = this.episodeCount;
            tvSeriesPersist.id = this.id;
            tvSeriesPersist.overview = this.overview;
            tvSeriesPersist.popularity = this.popularity;
            tvSeriesPersist.posterUrl = this.posterUrl;
            tvSeriesPersist.title = this.title;
            tvSeriesPersist.voteAverage = this.voteAverage;
            tvSeriesPersist.watchlistId = this.watchlistId;
            tvSeriesPersist.seasonCount = this.seasonCount;

            return tvSeriesPersist;
        }
    }
}
