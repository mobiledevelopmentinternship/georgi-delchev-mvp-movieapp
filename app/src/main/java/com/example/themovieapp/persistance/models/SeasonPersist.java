package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity
public class SeasonPersist {

    @SerializedName("id")
    @PrimaryKey
    @ColumnInfo(name = "s_id")
    private Integer id;
    @SerializedName("season_number")
    @ColumnInfo(name = "season_number")
    private Integer seasonNumber;
    @ColumnInfo(name = "tv_series_id")
    private Integer tvSeriesId;

    public SeasonPersist(Integer id, Integer seasonNumber, Integer tvSeriesId) {
        this.id = id;
        this.seasonNumber = seasonNumber;
        this.tvSeriesId = tvSeriesId;
    }

    @Ignore
    public SeasonPersist(){}

    public Integer getId() {
        return id;
    }

    public Integer getSeasonNumber() {
        return seasonNumber;
    }

    public Integer getTvSeriesId() {
        return tvSeriesId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setSeasonNumber(Integer seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public void setTvSeriesId(Integer tvSeriesId) {
        this.tvSeriesId = tvSeriesId;
    }

    public static class Builder {

        private Integer id;
        private Integer seasonNumber;
        private Integer tvSeriesId;

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder withSeasonNumber(Integer seasonNumber) {
            this.seasonNumber = seasonNumber;
            return this;
        }

        public Builder withTvSeriesId(Integer tvSeriesId) {
            this.tvSeriesId = tvSeriesId;
            return this;
        }

        public SeasonPersist build() {
            SeasonPersist seasonPersist = new SeasonPersist();
            seasonPersist.id = this.id;
            seasonPersist.seasonNumber = this.seasonNumber;
            seasonPersist.tvSeriesId = this.tvSeriesId;

            return seasonPersist;
        }
    }
}
