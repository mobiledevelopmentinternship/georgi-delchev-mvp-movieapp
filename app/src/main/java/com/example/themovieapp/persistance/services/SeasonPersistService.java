package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.SeasonDao;
import com.example.themovieapp.persistance.models.SeasonPersist;
import com.example.themovieapp.persistance.repositories.SeasonRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class SeasonPersistService implements SeasonRepository {

    private final SeasonDao seasonDao;

    @Inject
    public SeasonPersistService(SeasonDao seasonDao) {
        this.seasonDao = seasonDao;
    }

    @Override
    public void insert(final SeasonPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> seasonDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(final List<SeasonPersist> data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> seasonDao.insertAll(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void update(final SeasonPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> seasonDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void delete(final SeasonPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> seasonDao.delete(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public Single<List<SeasonPersist>> getTvSeriesSeasons(Integer tvSeriesId) {
        return seasonDao.getTvSeriesSeasons(tvSeriesId);
    }
}