package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

public interface ScreenCastRepository extends BaseRepository<ScreenCastMemberPersist> {
}