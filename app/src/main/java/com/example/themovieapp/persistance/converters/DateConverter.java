package com.example.themovieapp.persistance.converters;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class DateConverter {

    @TypeConverter
    public static Date toDate(long dateLong) {
        return new Date(dateLong);
    }

    @TypeConverter
    public static long fromDate(Date date) {
        if (date == null) {
            return System.currentTimeMillis();
        }
        return date.getTime();
    }
}
