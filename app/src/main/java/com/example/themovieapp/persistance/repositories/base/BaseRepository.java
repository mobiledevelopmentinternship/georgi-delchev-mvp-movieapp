package com.example.themovieapp.persistance.repositories.base;

import java.util.List;

public interface BaseRepository<T> {

    void insert(T data);

    void insertAll(List<T> data);

    void update(T data);

    void delete(T data);
}
