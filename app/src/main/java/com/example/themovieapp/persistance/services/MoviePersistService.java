package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.MovieDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.repositories.MovieRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class MoviePersistService implements MovieRepository {

    private final MovieDao movieDao;
    private final Calendar calendar;

    @Inject
    public MoviePersistService(MovieDao movieDao) {
        this.movieDao = movieDao;
        calendar = Calendar.getInstance();
    }

    @Override
    public void insert(final MoviePersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> movieDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(final List<MoviePersist> data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> movieDao.insertAll(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void update(final MoviePersist data) {
        movieDao.update(data);
    }

    @Override
    public void delete(final MoviePersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> movieDao.delete(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public Single<List<MoviePersist>> getUpcomingMovies() {
        return movieDao.getUpcomingMovies(calendar.getTimeInMillis());
    }

    @Override
    public Single<List<MoviePersist>> getTopRatedMovies() {
        return movieDao.getTopRatedMovies();
    }

    @Override
    public Single<List<MoviePersist>> getPopularMovies() {
        return movieDao.getMostPopularMovies();
    }

    @Override
    public Single<List<ScreenCastMemberPersist>> getMovieScreenCastMembers(String mediaName) {
        return movieDao.getMovieScreenCastMember(mediaName);
    }

    @Override
    public Single<List<CrewMemberPersist>> getMovieCrewMembers(String mediaName) {
        return movieDao.getMovieCrewMembers(mediaName);
    }

    @Override
    public Single<MoviePersist> getMovieById(Integer movieId) {
        return movieDao.getMovieById(movieId);
    }
}