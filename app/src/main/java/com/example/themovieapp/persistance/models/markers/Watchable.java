package com.example.themovieapp.persistance.models.markers;

import android.os.Parcelable;


public interface Watchable extends Parcelable {

    String getPosterUrl();

    String getTitle();
}
