package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
public class WatchlistPersist implements Parcelable, Comparable<WatchlistPersist> {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "list_id")
    private Long id;
    @ColumnInfo(name = "name")
    private String name;
    @Ignore
    public static final Parcelable.Creator CREATOR = new Creator<WatchlistPersist>() {
        @Override
        public WatchlistPersist createFromParcel(Parcel source) {
            return new WatchlistPersist(source);
        }

        @Override
        public WatchlistPersist[] newArray(int size) {
            return new WatchlistPersist[size];
        }
    };

    public WatchlistPersist(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Ignore
    public WatchlistPersist(String name) {
        this.name = name;
    }

    @Ignore
    public WatchlistPersist(Parcel source) {
        this.name = source.readString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    @Override
    public int compareTo(WatchlistPersist watchlistPersist) {
        return this.id.compareTo(watchlistPersist.getId());
    }
}
