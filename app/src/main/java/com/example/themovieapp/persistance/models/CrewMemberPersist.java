package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import com.example.themovieapp.persistance.models.markers.Member;

@Entity(primaryKeys = {"memberId", "mediaName"})
public class CrewMemberPersist implements Member {

    @NonNull
    private Integer memberId = 0;
    @NonNull
    private String mediaName = "";
    @ColumnInfo(name = "job")
    private String job;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "image_url")
    private String imageUrl;

    public CrewMemberPersist(@NonNull Integer memberId, String job, String name, String imageUrl, @NonNull String mediaName) {
        this.memberId = memberId;
        this.job = job;
        this.name = name;
        this.imageUrl = imageUrl;
        this.mediaName = mediaName;
    }

    @Ignore
    public CrewMemberPersist() {
    }

    public String getJob() {
        return job;
    }

    public String getName() {
        return name;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getMemberName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String getRole() {
        return job;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @NonNull
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(@NonNull Integer memberId) {
        this.memberId = memberId;
    }

    @NonNull
    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(@NonNull String mediaName) {
        this.mediaName = mediaName;
    }

    public static class Builder {

        private Integer id;
        private String job;
        private String name;
        private String imageUrl;

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder withJob(String job) {
            this.job = job;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public CrewMemberPersist build() {
            CrewMemberPersist crewMemberPersist = new CrewMemberPersist();
            crewMemberPersist.job = this.job;
            crewMemberPersist.name = this.name;
            crewMemberPersist.imageUrl = this.imageUrl;
            crewMemberPersist.memberId = this.id;

            return crewMemberPersist;
        }
    }
}
