package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.utils.Constants;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface TvSeriesDao extends BaseDao<TvSeriesPersist> {

    @Query("SELECT * FROM tvseriespersist WHERE tv_id == :id")
    Single<TvSeriesPersist> getTvSeriesById(Integer id);

    @Query("SELECT * FROM TvSeriesPersist ORDER BY TvSeriesPersist.vote_average DESC LIMIT 20")
    Single<List<TvSeriesPersist>> getTopRatedTvSeries();

    @Query("SELECT * FROM TvSeriesPersist ORDER BY TvSeriesPersist.popularity DESC LIMIT 20")
    Single<List<TvSeriesPersist>> getMostPopularTvSeries();

    @Query("SELECT * FROM TvSeriesPersist WHERE TvSeriesPersist.airing_date == :currentDate " +
            "ORDER BY airing_date DESC LIMIT 20")
    Single<List<TvSeriesPersist>> getTvSeriesAiringToday(Long currentDate);

    @Query("DELETE FROM TvSeriesPersist WHERE :currentDate - TvSeriesPersist.airing_date <=" + Constants.DAY_AS_MILLISECONDS)
    void deleteTvSeriesNotAiringToday(Long currentDate);

    @Query("SELECT * FROM ScreenCastMemberPersist AS scm WHERE scm.mediaName = :mediaName")
    Single<List<ScreenCastMemberPersist>> getTvSeriesScreenCastMembers(String mediaName);

    @Query("SELECT * FROM CrewMemberPersist AS cm WHERE cm.mediaName = :mediaName")
    Single<List<CrewMemberPersist>> getTvSeriesCrewMembers(String mediaName);
}