package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import com.example.themovieapp.persistance.models.markers.Member;

@Entity(primaryKeys = {"mediaName", "actorId"})
public class ScreenCastMemberPersist implements Member {

    @NonNull
    private String mediaName = "";
    @NonNull
    private Integer actorId = 0;
    @ColumnInfo(name = "image_url")
    private String imageUrl;
    @ColumnInfo(name = "actor_name")
    private String actorName;
    @ColumnInfo(name = "character_name")
    private String characterName;

    public ScreenCastMemberPersist(@NonNull String mediaName, @NonNull Integer actorId, String imageUrl, String actorName, String characterName) {
        this.mediaName = mediaName;
        this.actorId = actorId;
        this.imageUrl = imageUrl;
        this.actorName = actorName;
        this.characterName = characterName;
    }

    @Ignore
    public ScreenCastMemberPersist() {
    }

    @NonNull
    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(@NonNull String mediaName) {
        this.mediaName = mediaName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String getRole() {
        return characterName;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMemberName() {
        return actorName;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    @NonNull
    public Integer getActorId() {
        return actorId;
    }

    public void setActorId(@NonNull Integer actor_id) {
        this.actorId = actor_id;
    }

    public static class Builder {

        private Integer actorId;
        private String imageUrl;
        private String actorName;
        private String characterName;
        private Integer tvSeriesId;

        public Builder withActorId(Integer actorId) {
            this.actorId = actorId;
            return this;
        }

        public Builder withImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public Builder withActorName(String actorName) {
            this.actorName = actorName;
            return this;
        }

        public Builder withCharacterName(String characterName) {
            this.characterName = characterName;
            return this;
        }

        public ScreenCastMemberPersist build() {
            ScreenCastMemberPersist castMemberPersist = new ScreenCastMemberPersist();
            castMemberPersist.actorName = this.actorName;
            castMemberPersist.characterName = this.characterName;
            castMemberPersist.imageUrl = this.imageUrl;
            castMemberPersist.actorId = this.actorId;

            return castMemberPersist;
        }
    }
}
