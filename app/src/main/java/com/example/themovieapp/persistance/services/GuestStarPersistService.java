package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.GuestStarDao;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.repositories.GuestStarRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class GuestStarPersistService implements GuestStarRepository {

    private final GuestStarDao guestStarDao;

    @Inject
    public GuestStarPersistService(GuestStarDao guestStarDao) {
        this.guestStarDao = guestStarDao;
    }

    @Override
    public void insert(GuestStarPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> guestStarDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(List<GuestStarPersist> data) {
        guestStarDao.insertAll(data);
    }

    @Override
    public void update(GuestStarPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> guestStarDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void delete(GuestStarPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> guestStarDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }
}