package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.WatchlistsDao;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.repositories.WatchlistRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.internal.operators.completable.CompletableFromAction;
import io.reactivex.schedulers.Schedulers;

public class WatchlistPersistService implements WatchlistRepository {

    private final WatchlistsDao watchlistsDao;

    @Inject
    public WatchlistPersistService(WatchlistsDao dao) {
        this.watchlistsDao = dao;
    }

    @Override
    public void insert(final WatchlistPersist data) {
        watchlistsDao.insert(data);
    }

    @Override
    public void insertAll(final List<WatchlistPersist> data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> watchlistsDao.insertAll(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void update(final WatchlistPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> watchlistsDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void delete(final WatchlistPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> watchlistsDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public Single<List<MoviePersist>> getMoviesByWatchlistId(Long watchListId) {
        return watchlistsDao.getMoviesByWatchlistId(watchListId);
    }

    @Override
    public Single<List<TvSeriesPersist>> getTvSeriesByWatchlistId(Long watchlistId) {
        return watchlistsDao.getTvSeriesByWatchlistId(watchlistId);
    }

    @Override
    public Single<List<WatchlistPersist>> getAllWatchlists() {
        return watchlistsDao.getAllWatchlists();
    }

    @Override
    public Completable insertWithCompletable(WatchlistPersist watchlist) {
        return new CompletableFromAction(() -> watchlistsDao.insert(watchlist));
    }
}