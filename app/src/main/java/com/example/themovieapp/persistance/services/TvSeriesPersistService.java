package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.TvSeriesDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.repositories.TvSeriesRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class TvSeriesPersistService implements TvSeriesRepository {

    private final TvSeriesDao tvSeriesDao;

    @Inject
    public TvSeriesPersistService(TvSeriesDao tvSeriesDao) {
        this.tvSeriesDao = tvSeriesDao;
    }

    @Override
    public Single<TvSeriesPersist> getTvSeriesById(Integer id) {
        return tvSeriesDao.getTvSeriesById(id);
    }

    @Override
    public Single<List<TvSeriesPersist>> getTopRatedTvSeries() {
        return tvSeriesDao.getTopRatedTvSeries();
    }

    @Override
    public Single<List<TvSeriesPersist>> getMostPopularTvSeries() {
        return tvSeriesDao.getMostPopularTvSeries();
    }

    @Override
    public Single<List<TvSeriesPersist>> getTvSeriesAiringToday() {
        return tvSeriesDao.getTvSeriesAiringToday(Calendar.getInstance().getTimeInMillis());
    }

    @Override
    public Single<List<ScreenCastMemberPersist>> getTvSeriesScreenCastMembers(String mediaName) {
        return tvSeriesDao.getTvSeriesScreenCastMembers(mediaName);
    }

    @Override
    public Single<List<CrewMemberPersist>> getTvSeriesCrewMembers(String mediaName) {
        return tvSeriesDao.getTvSeriesCrewMembers(mediaName);
    }

    @Override
    public void deleteTvSeriesNotAiringTodayFromDb(final Long currentTime) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> tvSeriesDao.deleteTvSeriesNotAiringToday(currentTime))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insert(final TvSeriesPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> tvSeriesDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(final List<TvSeriesPersist> data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> tvSeriesDao.insertAll(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void update(final TvSeriesPersist data) {
        tvSeriesDao.update(data);
    }

    @Override
    public void delete(final TvSeriesPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> tvSeriesDao.delete(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }
}