package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

import java.util.List;

import io.reactivex.Single;

public interface EpisodeRepository extends BaseRepository<EpisodePersist> {

    Single<List<GuestStarPersist>> getEpisodeGuestStars(String episodeName);

    Single<List<EpisodePersist>> getAllEpisodesForASeason(Integer seasonId, Integer seasonNumber);
}
