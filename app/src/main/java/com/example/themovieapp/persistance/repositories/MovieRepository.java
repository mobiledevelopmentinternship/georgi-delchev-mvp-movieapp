package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

import java.util.List;

import io.reactivex.Single;

public interface MovieRepository extends BaseRepository<MoviePersist> {

    Single<List<MoviePersist>> getUpcomingMovies();

    Single<List<MoviePersist>> getTopRatedMovies();

    Single<List<MoviePersist>> getPopularMovies();

    Single<List<ScreenCastMemberPersist>> getMovieScreenCastMembers(String mediaName);

    Single<List<CrewMemberPersist>> getMovieCrewMembers(String mediaName);

    Single<MoviePersist> getMovieById(Integer movieId);
}