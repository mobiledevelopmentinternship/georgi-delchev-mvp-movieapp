package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.themovieapp.persistance.models.markers.Watchable;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity
public class MoviePersist implements Watchable {

    @PrimaryKey
    @ColumnInfo(name = "m_id")
    @SerializedName("id")
    private Integer id;
    @SerializedName("poster_path")
    @ColumnInfo(name = "poster_url")
    private String posterUrl;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    private String title;
    @SerializedName("original_language")
    @ColumnInfo(name = "language")
    private String language;
    @SerializedName("runtime")
    @ColumnInfo(name = "duration")
    private Integer duration;
    @SerializedName("overview")
    @ColumnInfo(name = "overview")
    private String overview;
    @SerializedName("release_date")
    @ColumnInfo(name = "release_date")
    private Date releaseDate;
    @SerializedName("vote_average")
    @ColumnInfo(name = "vote_average")
    private Double voteAverage;
    @SerializedName("vote_count")
    @ColumnInfo(name = "popularity")
    private Double popularity;
    @ColumnInfo(name = "watchlist_id")
    private Long watchlistId;
    @Ignore
    public static final Parcelable.Creator CREATOR = new Creator<MoviePersist>() {
        @Override
        public MoviePersist createFromParcel(Parcel source) {
            return new MoviePersist(source);
        }

        @Override
        public MoviePersist[] newArray(int size) {
            return new MoviePersist[size];
        }
    };

    public MoviePersist(Integer id, String posterUrl, String title, String language,
                        Integer duration, String overview, Date releaseDate,
                        Long watchlistId, Double voteAverage, Double popularity) {
        this.id = id;
        this.posterUrl = posterUrl;
        this.title = title;
        this.language = language;
        this.duration = duration;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.watchlistId = watchlistId;
        this.voteAverage = voteAverage;
        this.popularity = popularity;
    }

    @Ignore
    public MoviePersist(){}

    public Integer getId() {
        return id;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getLanguage() {
        return language;
    }

    public Integer getDuration() {
        return duration;
    }

    public String getOverview() {
        return overview;
    }

    public Long getWatchlistId() {
        return watchlistId;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setWatchlistId(Long watchlistId) {
        this.watchlistId = watchlistId;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    @Ignore
    public MoviePersist(Parcel source) {
        this.id = source.readInt();
        this.duration = source.readInt();
        this.overview = source.readString();
        this.posterUrl = source.readString();
        this.title = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.duration);
        dest.writeString(this.overview);
        dest.writeString(this.posterUrl);
        dest.writeString(this.title);
    }

    public static class Builder {

        private Integer id;
        private String posterUrl;
        private String title;
        private String language;
        private Integer duration;
        private String overview;
        private Date releaseDate;
        private Long watchlistId;
        private Double voteAverage;
        private Double popularity;

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder withPosterUrl(String posterUrl) {
            this.posterUrl = posterUrl;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withLanguage(String language) {
            this.language = language;
            return this;
        }

        public Builder withDuration(Integer duration) {
            this.duration = duration;
            return this;
        }

        public Builder withOverview(String overview) {
            this.overview = overview;
            return this;
        }

        public Builder withReleaseDate(Date releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public Builder withWatchlistId(Long watchlistId) {
            this.watchlistId = watchlistId;
            return this;
        }

        public Builder withVoteAverage(Double voteAverage) {
            this.voteAverage = voteAverage;
            return this;
        }

        public Builder withPopularity(Double popularity) {
            this.popularity = popularity;
            return this;
        }

        public MoviePersist build() {
            MoviePersist moviePersist = new MoviePersist();
            moviePersist.duration = this.duration;
            moviePersist.id = this.id;
            moviePersist.language = this.language;
            moviePersist.overview = this.overview;
            moviePersist.popularity = this.popularity;
            moviePersist.posterUrl = this.posterUrl;
            moviePersist.releaseDate = this.releaseDate;
            moviePersist.title = this.title;
            moviePersist.voteAverage = this.voteAverage;
            moviePersist.watchlistId = this.watchlistId;

            return moviePersist;
        }
    }
}
