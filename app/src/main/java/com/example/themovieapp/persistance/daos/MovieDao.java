package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface MovieDao extends BaseDao<MoviePersist> {

    @Query("SELECT * FROM MoviePersist AS m where m.m_id = :movieId")
    Single<MoviePersist> getMovieById(Integer movieId);

    @Query("SELECT * FROM ScreenCastMemberPersist as scm WHERE scm.mediaName = :mediaName")
    Single<List<ScreenCastMemberPersist>> getMovieScreenCastMember(String mediaName);

    @Query("SELECT * FROM CrewMemberPersist as cm WHERE cm.mediaName = :mediaName")
    Single<List<CrewMemberPersist>> getMovieCrewMembers(String mediaName);

    @Query("SELECT * FROM MoviePersist WHERE MoviePersist.release_date > :currentDate ORDER BY release_date DESC LIMIT 20")
    Single<List<MoviePersist>> getUpcomingMovies(Long currentDate);

    @Query("SELECT * FROM MoviePersist ORDER BY MoviePersist.vote_average DESC LIMIT 20")
    Single<List<MoviePersist>> getTopRatedMovies();

    @Query("SELECT * FROM MoviePersist ORDER BY MoviePersist.popularity DESC LIMIT 20")
    Single<List<MoviePersist>> getMostPopularMovies();
}
