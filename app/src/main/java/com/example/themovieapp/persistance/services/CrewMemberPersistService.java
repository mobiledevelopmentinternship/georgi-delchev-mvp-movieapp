package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.CrewMemberDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.repositories.CrewMemberRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.internal.operators.completable.CompletableFromAction;
import io.reactivex.schedulers.Schedulers;

public class CrewMemberPersistService implements CrewMemberRepository {

    private final CrewMemberDao crewMemberDao;

    @Inject
    public CrewMemberPersistService(CrewMemberDao crewMemberDao) {
        this.crewMemberDao = crewMemberDao;
    }

    @Override
    public void insert(CrewMemberPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        Completable completable = new CompletableFromAction(() -> crewMemberDao.insert(data));
        disposable = completable
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(List<CrewMemberPersist> data) {
        crewMemberDao.insertAll(data);
    }

    @Override
    public void update(CrewMemberPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> crewMemberDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void delete(CrewMemberPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> crewMemberDao.delete(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }
}