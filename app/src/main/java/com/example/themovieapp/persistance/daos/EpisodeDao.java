package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface EpisodeDao extends BaseDao<EpisodePersist> {

    @Query("SELECT * FROM GuestStarPersist AS gs WHERE gs.episodeName = :episodeName")
    Single<List<GuestStarPersist>> getEpisodeGuestStars(String episodeName);

    @Query("SELECT * FROM EpisodePersist AS e WHERE e.season_id = :seasonId")
    Single<List<EpisodePersist>> getAllEpisodesForASeason(Integer seasonId);
}