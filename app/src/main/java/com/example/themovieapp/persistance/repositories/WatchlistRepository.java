package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface WatchlistRepository extends BaseRepository<WatchlistPersist> {

    Single<List<MoviePersist>> getMoviesByWatchlistId(Long watchListId);

    Single<List<TvSeriesPersist>> getTvSeriesByWatchlistId(Long watchlistId);

    Single<List<WatchlistPersist>> getAllWatchlists();

    Completable insertWithCompletable(WatchlistPersist watchlist);
}