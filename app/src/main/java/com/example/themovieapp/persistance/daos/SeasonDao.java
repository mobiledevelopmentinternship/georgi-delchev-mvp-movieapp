package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.SeasonPersist;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface SeasonDao extends BaseDao<SeasonPersist> {

    @Query("SELECT * FROM EpisodePersist AS e WHERE e.season_id = :seasonId")
    Single<List<EpisodePersist>> getAllEpisodesForASeason(Integer seasonId);

    @Query("SELECT * FROM SeasonPersist as s WHERE s.tv_series_id = :tvSeriesId")
    Single<List<SeasonPersist>> getTvSeriesSeasons(Integer tvSeriesId);
}