package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.CrewMemberPersist;

@Dao
public interface CrewMemberDao extends BaseDao<CrewMemberPersist> {
}
