package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.GuestStarPersist;

@Dao
public interface GuestStarDao extends BaseDao<GuestStarPersist> {
}
