package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.themovieapp.persistance.models.markers.Watchable;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity
public class EpisodePersist implements Watchable, Comparable<EpisodePersist> {

    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @SerializedName("still_path")
    @ColumnInfo(name = "poster_url")
    private String posterUrl;
    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;
    @SerializedName("air_date")
    @ColumnInfo(name = "air_date")
    private Date airDate;
    @SerializedName("episode_number")
    @ColumnInfo(name = "episode_number")
    private Integer episodeNumber;
    @SerializedName("overview")
    @ColumnInfo(name = "overview")
    private String overview;
    @SerializedName("season_number")
    @ColumnInfo(name = "season_number")
    private Integer seasonNumber;
    @ColumnInfo(name = "season_id", index = true)
    private Integer seasonId;
    public static final Parcelable.Creator CREATOR = new Creator<EpisodePersist>() {
        @Override
        public EpisodePersist createFromParcel(Parcel source) {
            return new EpisodePersist(source);
        }

        @Override
        public EpisodePersist[] newArray(int size) {
            return new EpisodePersist[size];
        }
    };

    public EpisodePersist(Integer id, String posterUrl, String name, Date airDate, Integer episodeNumber, String overview, Integer seasonNumber, Integer seasonId) {
        this.id = id;
        this.posterUrl = posterUrl;
        this.name = name;
        this.airDate = airDate;
        this.episodeNumber = episodeNumber;
        this.overview = overview;
        this.seasonNumber = seasonNumber;
        this.seasonId = seasonId;
    }

    @Ignore
    public EpisodePersist(){}

    public Integer getId() {
        return id;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    @Override
    public String getTitle() {
        return name;
    }

    public String getName() {
        return name;
    }

    public Date getAirDate() {
        return airDate;
    }

    public Integer getEpisodeNumber() {
        return episodeNumber;
    }

    public String getOverview() {
        return overview;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAirDate(Date airDate) {
        this.airDate = airDate;
    }

    public void setEpisodeNumber(Integer episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    @Ignore
    public EpisodePersist(Parcel source) {
        this.id = source.readInt();
        this.posterUrl = source.readString();
        this.episodeNumber = source.readInt();
        this.overview = source.readString();
        this.seasonId = source.readInt();
        this.name = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(episodeNumber);
        dest.writeInt(seasonId);
        dest.writeString(posterUrl);
        dest.writeString(overview);
        dest.writeString(name);
    }

    public Integer getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(Integer seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    @Override
    public int compareTo(EpisodePersist episodePersist) {
        return this.episodeNumber.compareTo(episodePersist.episodeNumber);
    }

    public static class Builder {

        private Integer id;
        private String posterUrl;
        private String name;
        private Date airDate;
        private Integer episodeNumber;
        private String overview;
        private Integer seasonId;
        private Integer seasonNumber;

        public Builder withSeasonNumber(Integer seasonNumber) {
            this.seasonNumber = seasonNumber;
            return this;
        }

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder withPosterUrl(String posterUrl) {
            this.posterUrl = posterUrl;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withAirDate(Date airDate) {
            this.airDate = airDate;
            return this;
        }

        public Builder withEpisodeNumber(Integer episodeNumber) {
            this.episodeNumber = episodeNumber;
            return this;
        }

        public Builder withOverview(String overview) {
            this.overview = overview;
            return this;
        }

        public Builder withSeasonId(Integer seasonId) {
            this.seasonId = seasonId;
            return this;
        }

        public EpisodePersist build() {
            EpisodePersist episodePersist = new EpisodePersist();
            episodePersist.airDate = this.airDate;
            episodePersist.episodeNumber = this.episodeNumber;
            episodePersist.id = this.id;
            episodePersist.name = this.name;
            episodePersist.overview = this.overview;
            episodePersist.posterUrl = this.posterUrl;
            episodePersist.seasonId = this.seasonId;
            episodePersist.seasonNumber = this.seasonNumber;

            return episodePersist;
        }
    }
}
