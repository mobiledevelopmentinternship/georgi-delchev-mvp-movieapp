package com.example.themovieapp.persistance.daos.base;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

public interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<T> data);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(T data);

    @Delete
    void delete(T data);
}
