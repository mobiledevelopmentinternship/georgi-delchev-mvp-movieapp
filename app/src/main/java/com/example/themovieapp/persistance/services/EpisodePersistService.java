package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.EpisodeDao;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.repositories.EpisodeRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class EpisodePersistService implements EpisodeRepository {

    private final EpisodeDao episodeDao;

    @Inject
    public EpisodePersistService(EpisodeDao episodeDao) {
        this.episodeDao = episodeDao;
    }

    @Override
    public void insert(final EpisodePersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> episodeDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(final List<EpisodePersist> data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> episodeDao.insertAll(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void update(final EpisodePersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> episodeDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void delete(final EpisodePersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> episodeDao.delete(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public Single<List<GuestStarPersist>> getEpisodeGuestStars(String episodeName) {
        return episodeDao.getEpisodeGuestStars(episodeName);
    }

    @Override
    public Single<List<EpisodePersist>> getAllEpisodesForASeason(Integer seasonId, Integer seasonNumber) {
        return episodeDao.getAllEpisodesForASeason(seasonId);
    }
}