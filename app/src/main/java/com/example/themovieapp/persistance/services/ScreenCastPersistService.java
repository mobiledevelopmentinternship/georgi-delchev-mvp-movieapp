package com.example.themovieapp.persistance.services;

import com.example.themovieapp.persistance.daos.ScreenCastDao;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.repositories.ScreenCastRepository;
import com.example.themovieapp.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.EmptyCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class ScreenCastPersistService implements ScreenCastRepository {

    private final ScreenCastDao screenCastDao;

    @Inject
    public ScreenCastPersistService(ScreenCastDao screenCastDao) {
        this.screenCastDao = screenCastDao;
    }

    @Override
    public void insert(ScreenCastMemberPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> screenCastDao.insert(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void insertAll(List<ScreenCastMemberPersist> data) {
        screenCastDao.insertAll(data);
    }

    @Override
    public void update(ScreenCastMemberPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> screenCastDao.update(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }

    @Override
    public void delete(ScreenCastMemberPersist data) {
        Disposable disposable = new EmptyCompletableObserver();
        disposable = Completable.fromAction(() -> screenCastDao.delete(data))
                .subscribeOn(Schedulers.io())
                .doFinally(disposable::dispose)
                .subscribe();
    }
}