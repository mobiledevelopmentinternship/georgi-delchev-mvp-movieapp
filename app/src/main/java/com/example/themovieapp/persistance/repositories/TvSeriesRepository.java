package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

import java.util.List;

import io.reactivex.Single;

public interface TvSeriesRepository extends BaseRepository<TvSeriesPersist> {

    Single<TvSeriesPersist> getTvSeriesById(Integer id);

    Single<List<TvSeriesPersist>> getTopRatedTvSeries();

    Single<List<TvSeriesPersist>> getMostPopularTvSeries();

    Single<List<TvSeriesPersist>> getTvSeriesAiringToday();

    Single<List<ScreenCastMemberPersist>> getTvSeriesScreenCastMembers(String mediaName);

    Single<List<CrewMemberPersist>> getTvSeriesCrewMembers(String mediaName);

    void deleteTvSeriesNotAiringTodayFromDb(Long currentTime);
}
