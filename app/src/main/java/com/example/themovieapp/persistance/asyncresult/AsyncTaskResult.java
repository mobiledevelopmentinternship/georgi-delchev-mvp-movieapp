package com.example.themovieapp.persistance.asyncresult;

public interface AsyncTaskResult<T> {

    void onSuccess(T result);

    void onError(Throwable throwable);
}