package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.SeasonPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

import java.util.List;

import io.reactivex.Single;

public interface SeasonRepository extends BaseRepository<SeasonPersist> {

  Single<List<SeasonPersist>> getTvSeriesSeasons(Integer tvSeriesId);
}
