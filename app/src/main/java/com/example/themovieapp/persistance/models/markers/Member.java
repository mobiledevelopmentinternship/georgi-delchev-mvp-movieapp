package com.example.themovieapp.persistance.models.markers;

public interface Member {

    String getMemberName();

    String getImageUrl();

    String getRole();
}
