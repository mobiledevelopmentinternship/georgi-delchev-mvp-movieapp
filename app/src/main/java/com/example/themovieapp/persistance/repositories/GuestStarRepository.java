package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

public interface GuestStarRepository extends BaseRepository<GuestStarPersist> {
}