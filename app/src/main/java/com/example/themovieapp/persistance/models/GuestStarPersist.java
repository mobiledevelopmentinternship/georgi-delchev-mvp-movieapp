package com.example.themovieapp.persistance.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import com.example.themovieapp.persistance.models.markers.Member;

@Entity(primaryKeys = {"guestStarId", "episodeName"})
public class GuestStarPersist implements Member {

    @NonNull
    private Integer guestStarId = 0;
    @NonNull
    private String episodeName = " ";
    @ColumnInfo(name = "actor_name")
    private String actorName;
    @ColumnInfo(name = "image_url")
    private String imageUrl;
    @ColumnInfo(name = "character_name")
    private String characterName;

    public GuestStarPersist(@NonNull Integer guestStarId, @NonNull String episodeName, String actorName, String imageUrl, String characterName) {
        this.guestStarId = guestStarId;
        this.actorName = actorName;
        this.characterName = characterName;
        this.episodeName = episodeName;
        this.imageUrl = imageUrl;
    }

    @Ignore
    public GuestStarPersist() {
    }

    public String getMemberName() {
        return actorName;
    }

    public String getCharacterName() {
        return characterName;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String getRole() {
        return characterName;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @NonNull
    public Integer getGuestStarId() {
        return guestStarId;
    }

    public void setGuestStarId(@NonNull Integer guestStarId) {
        this.guestStarId = guestStarId;
    }

    @NonNull
    public String getEpisodeName() {
        return episodeName;
    }

    public void setEpisodeName(@NonNull String episodeName) {
        this.episodeName = episodeName;
    }

    public static class Builder {

        private Integer guestStarId;
        private String actorName;
        private String characterName;
        private String imageUrl;

        public Builder withId(Integer id) {
            this.guestStarId = id;
            return this;
        }

        public Builder withActorName(String actorName) {
            this.actorName = actorName;
            return this;
        }

        public Builder withCharacterName(String characterName) {
            this.characterName = characterName;
            return this;
        }

        public Builder withImageUrl(String url) {
            this.imageUrl = url;
            return this;
        }

        public GuestStarPersist build() {
            GuestStarPersist guestStarPersist = new GuestStarPersist();
            guestStarPersist.actorName = this.actorName;
            guestStarPersist.characterName = this.characterName;
            guestStarPersist.guestStarId = this.guestStarId;
            guestStarPersist.imageUrl = this.imageUrl;

            return guestStarPersist;
        }
    }
}