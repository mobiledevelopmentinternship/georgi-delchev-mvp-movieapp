package com.example.themovieapp.persistance.repositories;

import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.repositories.base.BaseRepository;

public interface CrewMemberRepository extends BaseRepository<CrewMemberPersist> {
}