package com.example.themovieapp.persistance.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.themovieapp.persistance.daos.base.BaseDao;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.WatchlistPersist;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface WatchlistsDao extends BaseDao<WatchlistPersist> {

    @Query("SELECT * FROM MoviePersist AS m WHERE m.watchlist_id = :watchlistId")
    Single<List<MoviePersist>> getMoviesByWatchlistId(Long watchlistId);

    @Query("SELECT * FROM TvSeriesPersist AS tv WHERE tv.watchlist_id = :watchlistId")
    Single<List<TvSeriesPersist>> getTvSeriesByWatchlistId(Long watchlistId);

    @Query("SELECT * FROM watchlistpersist")
    Single<List<WatchlistPersist>> getAllWatchlists();
}