package com.example.themovieapp.utils;

import android.widget.ImageView;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import static android.view.View.GONE;

public class PicassoImageLoader {

    public static void setImage(final ImageView imageView, final String imageUrl) {
        if ("".equals(imageUrl)){
            imageView.setVisibility(GONE);
            return;
        }

        Picasso.get()
                .load(imageUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        //not used
                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get()
                                .load(imageUrl)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        // not used
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        imageView.setVisibility(GONE);
                                    }
                                });
                    }
                });
    }
}
