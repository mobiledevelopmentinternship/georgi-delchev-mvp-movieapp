package com.example.themovieapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.themovieapp.App;
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.disposables.Disposable;

@Singleton
public class NetworkUtils {

    private boolean isNetworkStateSaved;
    private Disposable networkStatusDisposable;
    private Disposable internetStatusDisposable;

    @Inject
    public NetworkUtils() {
        isNetworkStateSaved = getInitialNetworkState();
        observeNetwork();
    }

    private boolean getInitialNetworkState() {
        ConnectivityManager manager = (ConnectivityManager) App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info != null) {
            return info.isConnected();
        } else {
            return false;
        }
    }

    private void observeNetwork() {
        networkStatusDisposable = ReactiveNetwork
                .observeNetworkConnectivity(App.getInstance().getBaseContext())
                .compose(RxUtils.applyObservableSchedulers())
                .subscribe(
                        this::getConnectivityEvent,
                        this::onConnectivityError
                );
    }

    private void onConnectivityError(Throwable throwable) {
        changeStatus(false);
    }

    private void getConnectivityEvent(Connectivity connectivity) {
        if (connectivity.state() == NetworkInfo.State.CONNECTED) {
            internetStatusDisposable = ReactiveNetwork
                    .observeInternetConnectivity()
                    .compose(RxUtils.applyObservableSchedulers())
                    .subscribe(
                            this::changeStatus,
                            this::onConnectivityError
                    );
        } else if (connectivity.state() == NetworkInfo.State.DISCONNECTED) {
            changeStatus(false);
        }
    }

    private void changeStatus(Boolean currentState) {
        if (isNetworkStateSaved != currentState) {
            isNetworkStateSaved = currentState;
        }
    }

    public boolean getNetworkStatus() {
        return isNetworkStateSaved;
    }
}
