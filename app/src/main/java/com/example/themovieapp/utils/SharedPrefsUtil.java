package com.example.themovieapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.themovieapp.App;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.example.themovieapp.utils.Constants.SHARED_PREFERENCES_DATE_KEY;
import static com.example.themovieapp.utils.Constants.SHARED_PREFERENCES_NAME;
import static com.example.themovieapp.utils.Constants.SHARED_PREFERENCES_SESSION_ID_KEY;

@Singleton
public class SharedPrefsUtil {

    @Inject
    public SharedPrefsUtil() {
    }

    public void saveGuestSessionId(String sessionId) {
        saveStringSharedPref(SHARED_PREFERENCES_SESSION_ID_KEY, sessionId);
    }

    public void saveSessionExpireTimestamp(String expireDate) {
        saveStringSharedPref(SHARED_PREFERENCES_DATE_KEY, expireDate);
    }

    public String getGuestSessionId() {
        return getStringSharedPreference(SHARED_PREFERENCES_SESSION_ID_KEY);
    }

    public String getSessionExpireDate() {
        return getStringSharedPreference(SHARED_PREFERENCES_DATE_KEY);
    }

    private static void saveStringSharedPref(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        editor.putString(key, value);

        editor.commit();
    }

    private static String getStringSharedPreference(String key) {
        return getSharedPreferences().getString(key, "");
    }

    private static SharedPreferences getSharedPreferences() {
        return App.getInstance().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getSharedPreferencesEditor() {
        return getSharedPreferences().edit();
    }
}
