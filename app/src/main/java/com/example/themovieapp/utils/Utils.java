package com.example.themovieapp.utils;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.example.themovieapp.adapters.watchlist.models.WatchlistEntryModel;
import com.example.themovieapp.networking.util.ImageUrlBuilder;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.persistance.models.markers.Watchable;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.example.themovieapp.utils.Constants.DATE_FORMAT;
import static com.example.themovieapp.utils.Constants.DATE_STRING_BEGIN_INDEX;
import static com.example.themovieapp.utils.Constants.DATE_STRING_END_INDEX;
import static com.example.themovieapp.utils.Constants.FIRST;
import static com.example.themovieapp.utils.Constants.STRING_TO_DATE_FORMATTER_PATTERN;
import static com.example.themovieapp.utils.Constants.WITH;

public class Utils {

    public static Date convertStringToDate(String dateString) throws ParseException {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat(STRING_TO_DATE_FORMATTER_PATTERN);
        if (dateString.isEmpty()) {
            return new Date();
        }

        return formatter.parse(dateString);
    }

    public static String convertDateToString(Date date) {
        @SuppressLint("SimpleDateFormat")
        DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        if (date == null) {
            return formatter.format(new Date(System.currentTimeMillis()));
        }

        return formatter.format(date);
    }

    public static String convertQuery(String query) {
        StringBuilder builder = new StringBuilder();
        String commaLessQuery = query.replace(",", "");
        String[] splitted = commaLessQuery.split(" ");
        int lastElement = splitted.length - 1;
        for (int i = 0; i < splitted.length; i++) {
            if (i != lastElement) {
                builder.append(splitted[i]);
                builder.append(Constants.SPACE_IN_HEX);
            } else {
                builder.append(splitted[i]);
            }
        }

        return builder.toString();
    }

    public static String appendSelectedGenresToUrl(List<Integer> movieGenreIds) {
        StringBuilder builder = new StringBuilder();
        int lastElement = movieGenreIds.size() - 1;
        for (int i = 0; i < movieGenreIds.size(); i++) {
            if (i != lastElement) {
                builder.append(movieGenreIds.get(i));
                builder.append(Constants.COMMA_IN_HEX);
            } else {
                builder.append(movieGenreIds.get(i));
            }
        }

        return builder.toString();
    }

    public static String constructCongratulationsMessage(Integer result, String title) {
        StringBuilder builder = new StringBuilder();
        builder.append(App.getInstance().getString(R.string.dialog_rating_congratulations_message));
        builder.append(title);
        builder.append(" ");
        builder.append(WITH);
        builder.append(" ");
        builder.append(result);

        return builder.toString();
    }

    public static List<Member> convertToMember(List<? extends Member> result) {
        return new ArrayList<>(result);
    }

    public static String createEpisodeName(Integer episodeNumber) {
        return App.getInstance().getString(R.string.details_other_episodes_name_prefix) + " " + episodeNumber;
    }

    public static List<String> buildFullImageUrls(List<String> result, Integer imageWidth) {
        List<String> fullUrls = new ArrayList<>();
        for (String url : result) {
            fullUrls.add(buildFullImageUrl(url, imageWidth));
        }
        return fullUrls;
    }

    public static String buildFullImageUrl(String url, Integer imageWidth) {
        return ImageUrlBuilder.getInstance()
                .withImageWidth(imageWidth)
                .withImageUrl(url)
                .build();
    }

    public static List<Watchable> convertToWatchable(List<? extends Watchable> watchables) {
        return new ArrayList<>(watchables);
    }

    public static List<MoviePersist> convertFromWatchableToMovie(List<Watchable> watchables) {
        List<MoviePersist> parsedMovies = new ArrayList<>();
        for (Watchable watchable : watchables) {
            parsedMovies.add((MoviePersist) watchable);
        }

        return parsedMovies;
    }

    public static List<TvSeriesPersist> convertFromWatchableToTvSeries(List<Watchable> watchables) {
        List<TvSeriesPersist> parsedTvSeries = new ArrayList<>();
        for (Watchable watchable : watchables) {
            parsedTvSeries.add((TvSeriesPersist) watchable);
        }

        return parsedTvSeries;
    }

    public static List<WatchlistEntryModel> convertToWatchableModel(List<? extends Watchable> watchables) {
        List<WatchlistEntryModel> watchableModels = new ArrayList<>();
        for (Watchable watchable : watchables) {
            if (watchable instanceof MoviePersist) {
                MoviePersist movie = (MoviePersist) watchable;
                watchableModels.add(buildWatchableModelFromMovie(movie));
            } else {
                TvSeriesPersist tvSeries = (TvSeriesPersist) watchable;
                watchableModels.add(buildWatchableModelFromTvSeries(tvSeries));
            }
        }

        return watchableModels;
    }

    private static WatchlistEntryModel buildWatchableModelFromMovie(MoviePersist moviePersist) {
        return new WatchlistEntryModel.Builder()
                .withBriefOverview(moviePersist.getOverview())
                .withMediaType(App.getInstance().getString(R.string.movie_media_type))
                .withName(moviePersist.getTitle())
                .withReleaseDate(convertDateToString(moviePersist.getReleaseDate()))
                .withWatchableId(Long.valueOf(moviePersist.getId()))
                .withPosterUrl(moviePersist.getPosterUrl())
                .build();
    }

    private static WatchlistEntryModel buildWatchableModelFromTvSeries(TvSeriesPersist tv) {
        return new WatchlistEntryModel.Builder()
                .withBriefOverview(tv.getOverview())
                .withMediaType(App.getInstance().getString(R.string.tv_series_media_type))
                .withName(tv.getTitle())
                .withReleaseDate(convertDateToString(tv.getAiringDate()))
                .withWatchableId(Long.valueOf(tv.getId()))
                .withPosterUrl(tv.getPosterUrl())
                .build();
    }

    public static List<ScreenCastMemberPersist> convertFromMemberToScreenCast(List<Member> members) {
        List<ScreenCastMemberPersist> screenCastMembers = new ArrayList<>();
        for (Member member : members) {
            screenCastMembers.add((ScreenCastMemberPersist) member);
        }

        return screenCastMembers;
    }

    public static List<CrewMemberPersist> convertFromMemberToCrew(List<Member> members) {
        List<CrewMemberPersist> crew = new ArrayList<>();
        for (Member member : members) {
            crew.add((CrewMemberPersist) member);
        }

        return crew;
    }

    public static void setScreenCastMediaType(List<ScreenCastMemberPersist> screenCast, Watchable watchable) {
        for (ScreenCastMemberPersist screenCastMemberPersist : screenCast) {
            if (watchable instanceof MoviePersist) {
                MoviePersist movie = (MoviePersist) watchable;
                screenCastMemberPersist.setMediaName(movie.getTitle());
            } else {
                TvSeriesPersist tvSeries = (TvSeriesPersist) watchable;
                screenCastMemberPersist.setMediaName(tvSeries.getTitle());
            }
        }
    }

    public static void setCrewMembersMediaType(List<CrewMemberPersist> crew, Watchable watchable) {
        for (CrewMemberPersist crewMember : crew) {
            if (watchable instanceof MoviePersist) {
                MoviePersist movie = (MoviePersist) watchable;
                crewMember.setMediaName(movie.getTitle());
            } else {
                TvSeriesPersist tvSeries = (TvSeriesPersist) watchable;
                crewMember.setMediaName(tvSeries.getTitle());
            }
        }
    }

    public static void setGuestStarEpisodeName(List<GuestStarPersist> guestStars, EpisodePersist episodeToShow) {
        for (GuestStarPersist guestStar : guestStars) {
            guestStar.setEpisodeName(episodeToShow.getName());
        }
    }

    private static String reworkDate(String guestSessionExpireDate) {
        return guestSessionExpireDate.substring(DATE_STRING_BEGIN_INDEX, DATE_STRING_END_INDEX);
    }

    public static MoviePersist convertFromWatchableToMovie(Watchable watchable) {
        return (MoviePersist) watchable;
    }

    public static TvSeriesPersist convertFromWatchableToTvSeries(Watchable watchable) {
        return (TvSeriesPersist) watchable;
    }

    public static boolean compareGuestSessionExpireTimestamp(String guestSessionExpireDate) {
        if (TextUtils.equals(guestSessionExpireDate, "")) {
            return false;
        }

        String reworkedDate = reworkDate(guestSessionExpireDate);
        Timestamp sessionTimeStamp = Timestamp.valueOf(reworkedDate);
        Timestamp currentTimeTimeStamp = new Timestamp(System.currentTimeMillis());

        return currentTimeTimeStamp.equals(sessionTimeStamp);
    }

    public static List<CrewMemberPersist> filterCrewMemebers(List<CrewMemberPersist> crewMembers) {
        List<CrewMemberPersist> membersWithImage = new ArrayList<>();
        for (CrewMemberPersist crewMember : crewMembers) {
            if (!isImageUrlEmpty(crewMember.getImageUrl())) {
                membersWithImage.add(crewMember);
            }
        }

        return membersWithImage;
    }

    private static boolean isImageUrlEmpty(String imageUrl) {
        return TextUtils.isEmpty(imageUrl) || imageUrl == null;
    }

    public static List<ScreenCastMemberPersist> filterScreenCastMembers(List<ScreenCastMemberPersist> screenCast) {
        List<ScreenCastMemberPersist> membersWithImage = new ArrayList<>();
        for (ScreenCastMemberPersist member : screenCast) {
            if (!isImageUrlEmpty(member.getImageUrl())) {
                membersWithImage.add(member);
            }
        }

        return membersWithImage;
    }

    public static List<GuestStarPersist> filterGuestStars(List<GuestStarPersist> guestStars) {
        List<GuestStarPersist> membersWithImage = new ArrayList<>();
        for (GuestStarPersist crewMember : guestStars) {
            if (isImageUrlEmpty(crewMember.getImageUrl())) {
                membersWithImage.add(crewMember);
            }
        }

        return membersWithImage;
    }

    public static String buildTitle(String name, String mediaType) {
        return name +
                " (" +
                mediaType +
                ")";
    }

    public static List<Integer> extractIds(List<? extends Watchable> watchables) {
        List<Integer> ids = new ArrayList<>();
        if (watchables.get(FIRST) instanceof MoviePersist) {
            List<MoviePersist> movies = (List<MoviePersist>) watchables;
            for (MoviePersist movie : movies) {
                ids.add(movie.getId());
            }
        } else {
            List<TvSeriesPersist> tvSeries = (List<TvSeriesPersist>) watchables;
            for (TvSeriesPersist tv : tvSeries) {
                ids.add(tv.getId());
            }
        }

        return ids;
    }

    public static List<String> transformSeasonTitles(Set<Integer> seasonNumbers) {
        List<String> seasonNames = new ArrayList<>();
        for (Integer seasonNumber : seasonNumbers) {
            seasonNames.add(String.valueOf(seasonNumber));
        }

        return seasonNames;
    }
}