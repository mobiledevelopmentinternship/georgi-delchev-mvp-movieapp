package com.example.themovieapp.utils;

public enum Category {
    TOP_RATED,
    MOST_POPULAR,
    UPCOMING
}
