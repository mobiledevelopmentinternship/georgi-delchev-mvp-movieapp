package com.example.themovieapp.networking.retrofit.response.similarmedia;

import com.example.themovieapp.persistance.models.MoviePersist;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SimilarMoviesResponse {

    @SerializedName("results")
    private List<MoviePersist> movies;

    public SimilarMoviesResponse(List<MoviePersist> movies) {
        this.movies = movies;
    }

    public List<MoviePersist> getMovies() {
        return movies;
    }

    public void setMovies(List<MoviePersist> movies) {
        this.movies = movies;
    }
}
