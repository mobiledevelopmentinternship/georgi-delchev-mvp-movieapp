package com.example.themovieapp.networking.services.common;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.search.DiscoverResponseList;

import javax.inject.Inject;

import io.reactivex.Single;

public class DiscoverService {

    private MovieApiService movieApiService;

    @Inject
    public DiscoverService(MovieApiService movieApiService) {
        this.movieApiService = movieApiService;
    }

    public Single<DiscoverResponseList> discover(String searchPhrase, Integer page) {
        return movieApiService.discover(searchPhrase, page);
    }
}
