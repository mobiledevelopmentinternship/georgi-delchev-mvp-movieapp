package com.example.themovieapp.networking.retrofit.response.episode;

import com.example.themovieapp.persistance.models.EpisodePersist;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EpisodesResponse {

    @SerializedName("episodes")
    private List<EpisodePersist> episodes;

    @SerializedName("id")
    private Integer seasonId;

    @SerializedName("season_number")
    private Integer seasonNumber;

    public EpisodesResponse(List<EpisodePersist> episodes, Integer seasonId, Integer seasonNumber) {
        this.episodes = episodes;
        this.seasonId = seasonId;
        this.seasonNumber = seasonNumber;
    }

    public List<EpisodePersist> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<EpisodePersist> episodes) {
        this.episodes = episodes;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(Integer seasonNumber) {
        this.seasonNumber = seasonNumber;
    }
}
