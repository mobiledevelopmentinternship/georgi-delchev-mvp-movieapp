package com.example.themovieapp.networking.services.tvseries.episodes;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.casts.EpisodeCastResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class TvEpisodeCreditsService {

    private final MovieApiService movieApiService;

    @Inject
    public TvEpisodeCreditsService(MovieApiService movieApiService){
        this.movieApiService = movieApiService;
    }

    public Single<EpisodeCastResponse> getTvSeriesCreditsResponse(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        return movieApiService.getEpisodeScreenCast(tvSeriesId, seasonNumber, episodeNumber);
    }
}