package com.example.themovieapp.networking.services.base;

import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.persistance.models.MoviePersist;

import io.reactivex.Single;

public interface DetailsResponseReader {

    Single<MoviePersist> getMovieDetailsResponse(Integer id);

    Single<SimilarMoviesResponse> getSimilarMovies(Integer id);

    Single<TvSeriesResponse> getTvSeriesDetailsResponse(Integer id);

    Single<SimilarTvSeriesResponse> getSimilarTvSeries(Integer id);
}
