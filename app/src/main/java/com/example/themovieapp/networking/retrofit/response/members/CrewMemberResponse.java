package com.example.themovieapp.networking.retrofit.response.members;

import com.google.gson.annotations.SerializedName;

public class CrewMemberResponse {

    @SerializedName("id")
    private Integer id;
    @SerializedName("job")
    private String job;
    @SerializedName("name")
    private String name;
    @SerializedName("profile_path")
    private String imageUrl;

    public CrewMemberResponse(Integer id, String job, String name, String imageUrl) {
        this.id = id;
        this.job = job;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
