package com.example.themovieapp.networking.services.movie;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.rated.RatedMoviesResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class MovieHomeService {

    private final MovieApiService movieApiService;

    @Inject
    public MovieHomeService(MovieApiService movieApiService){
        this.movieApiService = movieApiService;
    }

    public Single<RatedMoviesResponse> getTopRatedResponse() {
        return movieApiService.getTopRatedMovies();
    }

    public Single<RatedMoviesResponse> getPopularResponse() {
        return movieApiService.getMostPopularMovies();
    }

    public Single<RatedMoviesResponse> getNewestResponse() {
        return movieApiService.getUpcomingMovies();
    }
}