package com.example.themovieapp.networking.retrofit.response.image;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImagesListResponse {

    @SerializedName("stills")
    private List<ImageResponse> images;

    public ImagesListResponse(List<ImageResponse> imageUrl) {
        this.images = imageUrl;
    }

    public List<ImageResponse> getImages() {
        return images;
    }

    public void setImages(List<ImageResponse> images) {
        this.images = images;
    }
}