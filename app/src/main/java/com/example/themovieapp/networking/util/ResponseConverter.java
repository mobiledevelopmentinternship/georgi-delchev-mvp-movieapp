package com.example.themovieapp.networking.util;

import android.text.TextUtils;

import com.example.themovieapp.networking.retrofit.response.episode.EpisodesResponse;
import com.example.themovieapp.networking.retrofit.response.image.ImageResponse;
import com.example.themovieapp.networking.retrofit.response.image.ImagesListResponse;
import com.example.themovieapp.networking.retrofit.response.members.CrewMemberResponse;
import com.example.themovieapp.networking.retrofit.response.members.GuestStarResponse;
import com.example.themovieapp.networking.retrofit.response.members.ScreenCastMemberResponse;
import com.example.themovieapp.networking.retrofit.response.rated.RatedMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.rated.RatedTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.example.themovieapp.utils.Constants.MOVIE_MEDIA_TYPE;

public class ResponseConverter {

    public static List<CrewMemberPersist> convertCrewMemberResponseToPersist(List<CrewMemberResponse> response, String mediaType, Integer id) {
        List<CrewMemberPersist> crewMemberPersists = new ArrayList<>();
        for (CrewMemberResponse crewMemberResponse : response) {
            if (TextUtils.equals(crewMemberResponse.getImageUrl(), "")) {
                continue;
            }

            crewMemberPersists.add(buildCrewMember(crewMemberResponse, mediaType, id));
        }

        return crewMemberPersists;
    }

    private static CrewMemberPersist buildCrewMember(CrewMemberResponse crewMemberResponse, String mediaType, Integer id) {
        if (mediaType.equals(MOVIE_MEDIA_TYPE)) {
            return new CrewMemberPersist.Builder()
                    .withId(crewMemberResponse.getId())
                    .withImageUrl(crewMemberResponse.getImageUrl())
                    .withJob(crewMemberResponse.getJob())
                    .withName(crewMemberResponse.getName())
                    .build();
        } else {
            return new CrewMemberPersist.Builder()
                    .withId(crewMemberResponse.getId())
                    .withImageUrl(crewMemberResponse.getImageUrl())
                    .withJob(crewMemberResponse.getJob())
                    .withName(crewMemberResponse.getName())
                    .build();
        }
    }

    public static List<GuestStarPersist> convertGuestStarResponseToPersist(List<GuestStarResponse> response, Integer id) {
        List<GuestStarPersist> guestStarPersists = new ArrayList<>();
        for (GuestStarResponse guestStarResponse : response) {
            if (TextUtils.equals(guestStarResponse.getImageUrl(), "")) {
                continue;
            }

            guestStarPersists.add(buildGuestStar(guestStarResponse, id));
        }

        return guestStarPersists;
    }

    private static GuestStarPersist buildGuestStar(GuestStarResponse crewMemberResponse, Integer id) {
        return new GuestStarPersist.Builder()
                .withId(crewMemberResponse.getId())
                .withImageUrl(crewMemberResponse.getImageUrl())
                .withActorName(crewMemberResponse.getName())
                .withCharacterName(crewMemberResponse.getCharacter())
                .build();
    }

    public static List<ScreenCastMemberPersist> convertScreenCastMemberResponseToPersist(List<ScreenCastMemberResponse> response, Integer id, String mediaType) {
        List<ScreenCastMemberPersist> screenCastMemberPersists = new ArrayList<>();
        for (ScreenCastMemberResponse screenCastMember : response) {
            if (TextUtils.equals(screenCastMember.getImageUrl(), "")) {
                continue;
            }

            screenCastMemberPersists.add(buildScreenCast(screenCastMember, id, mediaType));
        }

        return screenCastMemberPersists;
    }

    private static ScreenCastMemberPersist buildScreenCast(ScreenCastMemberResponse crewMemberResponse, Integer id, String mediaType) {
        if (mediaType.equals(MOVIE_MEDIA_TYPE)) {
            return new ScreenCastMemberPersist.Builder()
                    .withActorId(crewMemberResponse.getId())
                    .withImageUrl(crewMemberResponse.getImageUrl())
                    .withActorName(crewMemberResponse.getName())
                    .withCharacterName(crewMemberResponse.getCharacter())
                    .build();
        } else {
            return new ScreenCastMemberPersist.Builder()
                    .withActorId(crewMemberResponse.getId())
                    .withImageUrl(crewMemberResponse.getImageUrl())
                    .withActorName(crewMemberResponse.getName())
                    .withCharacterName(crewMemberResponse.getCharacter())
                    .build();
        }
    }

    public static List<MoviePersist> convertRatedMoviesResponseToMoviePersistList(RatedMoviesResponse ratedMoviesResponse) {
        List<MoviePersist> movies = new ArrayList<>();
        for (MoviePersist ratedMovie : ratedMoviesResponse.getRatedMovies()) {
            movies.add(buildMovie(ratedMovie));
        }

        return movies;
    }

    private static MoviePersist buildMovie(MoviePersist ratedMovie) {
        return new MoviePersist.Builder()
                .withOverview(ratedMovie.getOverview())
                .withTitle(ratedMovie.getTitle())
                .withPosterUrl(ratedMovie.getPosterUrl())
                .withReleaseDate(ratedMovie.getReleaseDate())
                .withId(ratedMovie.getId())
                .withPopularity(ratedMovie.getPopularity())
                .withDuration(ratedMovie.getDuration())
                .withVoteAverage(ratedMovie.getVoteAverage())
                .build();
    }

    public static List<TvSeriesPersist> converRatedTvSeriesToTvSeriesPersistList(RatedTvSeriesResponse ratedTvSeriesResponse) throws ParseException {
        List<TvSeriesPersist> tvSeries = new ArrayList<>();
        for (TvSeriesResponse tv : ratedTvSeriesResponse.getRatedTvSeries()) {
            tvSeries.add(buildTvSeries(tv));
        }

        return tvSeries;
    }

    public static TvSeriesPersist convertTvResponseToPersist(TvSeriesResponse response) throws ParseException {
        return buildTvSeries(response);
    }

    private static TvSeriesPersist buildTvSeries(TvSeriesResponse tvSeries) throws ParseException {
        return new TvSeriesPersist.Builder()
                .withAiringDate(Utils.convertStringToDate(tvSeries.getDate()))
                .withId(tvSeries.getId())
                .withOverview(tvSeries.getOverview())
                .withPopularity(tvSeries.getPopularity())
                .withPosterUrl(tvSeries.getPosterUrl())
                .withTitle(tvSeries.getName())
                .withVoteAverage(tvSeries.getVoteAverage())
                .withSeasonCount(tvSeries.getNumberOfSeasons())
                .build();
    }

    public static List<EpisodePersist> convertEpisodesResponseToEpisodePersist(EpisodesResponse episodeResponse) {
        List<EpisodePersist> episodes = new ArrayList<>();
        for (EpisodePersist episode : episodeResponse.getEpisodes()) {
            episodes.add(buildEpisode(episode, episodeResponse.getSeasonId(), episodeResponse.getSeasonNumber()));
        }

        return episodes;
    }

    private static EpisodePersist buildEpisode(EpisodePersist episode, Integer seasonId, Integer seasonNumber) {
        return new EpisodePersist.Builder()
                .withSeasonId(seasonId)
                .withAirDate(episode.getAirDate())
                .withEpisodeNumber(episode.getEpisodeNumber())
                .withSeasonNumber(seasonNumber)
                .withId(episode.getId())
                .withName(episode.getName())
                .withOverview(episode.getOverview())
                .withPosterUrl(episode.getPosterUrl())
                .build();
    }

    public static List<String> extractImageUrls(ImagesListResponse imagesResponse) {
        List<String> imagesUrls = new ArrayList<>();
        for (ImageResponse image : imagesResponse.getImages()) {
            if (image.getImageUrl() == null) {
                continue;
            }
            imagesUrls.add(image.getImageUrl());
        }

        return imagesUrls;
    }
}
