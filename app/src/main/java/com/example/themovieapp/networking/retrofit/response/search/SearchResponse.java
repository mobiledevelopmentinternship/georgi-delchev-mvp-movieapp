package com.example.themovieapp.networking.retrofit.response.search;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class SearchResponse {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("title")
    private String title;

    @SerializedName("media_type")
    private String mediaType;

    @SerializedName("poster_path")
    private String posterUrl;

    @SerializedName("overview")
    private String overview;

    public SearchResponse(String name, Integer id, String title, String mediaType, String posterUrl, String overview) {
        this.name = name;
        this.id = id;
        this.title = title;
        this.mediaType = mediaType;
        this.posterUrl = posterUrl;
        this.overview = overview;
    }

    public SearchResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        SearchResponse that = (SearchResponse) object;
        return Objects.equals(getId(), that.getId())
                && Objects.equals(getName(), that.getName())
                && Objects.equals(getTitle(), that.getTitle())
                && Objects.equals(getMediaType(), that.getMediaType())
                && Objects.equals(getPosterUrl(), that.getPosterUrl())
                && Objects.equals(getOverview(), that.getOverview());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getTitle(), getMediaType(), getPosterUrl(), getOverview());
    }
}
