package com.example.themovieapp.networking.retrofit.response.tv;

import com.google.gson.annotations.SerializedName;

public class TvSeriesResponse {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("first_air_date")
    private String date;
    @SerializedName("overview")
    private String overview;
    @SerializedName("poster_path")
    private String posterUrl;
    @SerializedName("popularity")
    private Double popularity;
    @SerializedName("vote_average")
    private Double voteAverage;
    @SerializedName("number_of_seasons")
    private Integer numberOfSeasons;

    public TvSeriesResponse(Integer id, String name, String date, String overview, String posterUrl, Double popularity, Double voteAverage, Integer numberOfSeasons) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.overview = overview;
        this.posterUrl = posterUrl;
        this.popularity = popularity;
        this.voteAverage = voteAverage;
        this.numberOfSeasons = numberOfSeasons;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getNumberOfSeasons() {
        return numberOfSeasons;
    }

    public void setNumberOfSeasons(Integer numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }
}
