package com.example.themovieapp.networking.retrofit.response.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DiscoverResponseList {

    @SerializedName("results")
    private List<SearchResponse> result;

    @SerializedName("total_pages")
    private Integer totalPages;

    public DiscoverResponseList(List<SearchResponse> result, Integer totalPages) {
        this.result = result;
        this.totalPages = totalPages;
    }

    public List<SearchResponse> getResult() {
        return result;
    }

    public void setResult(List<SearchResponse> result) {
        this.result = result;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
