package com.example.themovieapp.networking.retrofit.response.members;

import com.google.gson.annotations.SerializedName;

public class GuestStarResponse {

    @SerializedName("id")
    private Integer id;
    @SerializedName("character")
    private String character;
    @SerializedName("name")
    private String name;
    @SerializedName("profile_path")
    private String imageUrl;

    public GuestStarResponse(Integer id, String character, String name, String imageUrl) {
        this.id = id;
        this.character = character;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
