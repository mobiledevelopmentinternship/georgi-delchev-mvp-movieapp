package com.example.themovieapp.networking.retrofit.request;

import com.google.gson.annotations.SerializedName;

public class RatingRequest {

    @SerializedName("value")
    private Integer value;

    public RatingRequest(Integer value) {
        setValue(value);
    }

    public Integer getValue() {
        return value;
    }

    private void setValue(Integer value) {
        this.value = value;
    }
}
