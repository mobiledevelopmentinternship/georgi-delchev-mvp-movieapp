package com.example.themovieapp.networking.services.common;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.request.RatingRequest;
import com.example.themovieapp.networking.retrofit.response.rating.GuestSessionResponse;

import javax.inject.Inject;

import io.reactivex.Single;

import static com.example.themovieapp.utils.Constants.MOVIE_MEDIA_TYPE;

public class RatingService {

    private final MovieApiService movieApiService;

    @Inject
    public RatingService(MovieApiService movieApiService){
        this.movieApiService = movieApiService;
    }

    public Single<RatingRequest> postRating(Integer rating,
                                            String mediaType,
                                            Integer id,
                                            String guestSessionId) {
        if (mediaType.equals(MOVIE_MEDIA_TYPE)) {
            return movieApiService.postMovieRating(id, new RatingRequest(rating), guestSessionId);
        }

        return movieApiService.postTvSeriesRating(id, new RatingRequest(rating), guestSessionId);
    }

    public Single<GuestSessionResponse> getGuestSessionId() {
        return movieApiService.getGuestSessionId();
    }
}