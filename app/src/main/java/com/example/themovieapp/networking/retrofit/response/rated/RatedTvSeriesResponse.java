package com.example.themovieapp.networking.retrofit.response.rated;

import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatedTvSeriesResponse {

    @SerializedName("results")
    private List<TvSeriesResponse> ratedTvSeries;

    public RatedTvSeriesResponse(List<TvSeriesResponse> ratedTvSeries) {
        this.ratedTvSeries = ratedTvSeries;
    }

    public List<TvSeriesResponse> getRatedTvSeries() {
        return ratedTvSeries;
    }

    public void setRatedTvSeries(List<TvSeriesResponse> ratedTvSeries) {
        this.ratedTvSeries = ratedTvSeries;
    }
}