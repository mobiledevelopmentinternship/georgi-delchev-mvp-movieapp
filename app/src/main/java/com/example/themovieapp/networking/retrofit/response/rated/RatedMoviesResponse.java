package com.example.themovieapp.networking.retrofit.response.rated;

import com.example.themovieapp.persistance.models.MoviePersist;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatedMoviesResponse {

    @SerializedName("results")
    private List<MoviePersist> ratedMovies;

    public RatedMoviesResponse(List<MoviePersist> ratedMovies) {
        this.ratedMovies = ratedMovies;
    }

    public List<MoviePersist> getRatedMovies() {
        return ratedMovies;
    }

    public void setRatedMovies(List<MoviePersist> ratedMovies) {
        this.ratedMovies = ratedMovies;
    }
}
