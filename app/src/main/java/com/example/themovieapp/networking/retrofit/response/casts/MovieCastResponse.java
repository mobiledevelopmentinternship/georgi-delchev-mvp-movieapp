package com.example.themovieapp.networking.retrofit.response.casts;

import com.example.themovieapp.networking.retrofit.response.members.CrewMemberResponse;
import com.example.themovieapp.networking.retrofit.response.members.ScreenCastMemberResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieCastResponse {

    @SerializedName("cast")
    private List<ScreenCastMemberResponse> screenCastMembers;
    @SerializedName("crew")
    private List<CrewMemberResponse> crewMembers;

    public MovieCastResponse(List<ScreenCastMemberResponse> screenCastMembers, List<CrewMemberResponse> crewMembers) {
        this.screenCastMembers = screenCastMembers;
        this.crewMembers = crewMembers;
    }

    public List<ScreenCastMemberResponse> getScreenCastMembers() {
        return screenCastMembers;
    }

    public void setScreenCastMembers(List<ScreenCastMemberResponse> screenCastMembers) {
        this.screenCastMembers = screenCastMembers;
    }

    public List<CrewMemberResponse> getCrewMembers() {
        return crewMembers;
    }

    public void setCrewMembers(List<CrewMemberResponse> crewMembers) {
        this.crewMembers = crewMembers;
    }
}
