package com.example.themovieapp.networking.retrofit.response.rating;

import com.google.gson.annotations.SerializedName;

public class GuestSessionResponse {

    @SerializedName("guest_session_id")
    private String guestSessionId;
    @SerializedName("expires_at")
    private String guestSessionExpireDate;

    public GuestSessionResponse(String guestSessionId, String guestSessionExpireDate) {
        this.guestSessionExpireDate = guestSessionExpireDate;
        this.guestSessionId = guestSessionId;
    }

    public String getGuestSessionId() {
        return guestSessionId;
    }

    public void setGuestSessionId(String guestSessionId) {
        this.guestSessionId = guestSessionId;
    }

    public String getGuestSessionExpireDate() {
        return guestSessionExpireDate;
    }

    public void setGuestSessionExpireDate(String guestSessionExpireDate) {
        this.guestSessionExpireDate = guestSessionExpireDate;
    }
}
