package com.example.themovieapp.networking.retrofit.response.members;

import com.google.gson.annotations.SerializedName;

public class ScreenCastMemberResponse {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("character")
    private String character;
    @SerializedName("profile_path")
    private String imageUrl;

    public ScreenCastMemberResponse(Integer id, String name, String character, String imageUrl) {
        this.id = id;
        this.name = name;
        this.character = character;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
