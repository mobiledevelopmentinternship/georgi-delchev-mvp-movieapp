package com.example.themovieapp.networking.retrofit.base;

import com.example.themovieapp.networking.retrofit.request.RatingRequest;
import com.example.themovieapp.networking.retrofit.response.casts.EpisodeCastResponse;
import com.example.themovieapp.networking.retrofit.response.casts.MovieCastResponse;
import com.example.themovieapp.networking.retrofit.response.casts.TvCastResponse;
import com.example.themovieapp.networking.retrofit.response.episode.EpisodesResponse;
import com.example.themovieapp.networking.retrofit.response.image.ImagesListResponse;
import com.example.themovieapp.networking.retrofit.response.rated.RatedMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.rated.RatedTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.search.DiscoverResponseList;
import com.example.themovieapp.networking.retrofit.response.rating.GuestSessionResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.MoviePersist;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApi {

    @GET("movie/{movieId}")
    Single<MoviePersist> getMovieDetails(@Path("movieId") Integer movieId, @Query("api_key") String apiKey);

    @GET("movie/{movieId}/credits")
    Single<MovieCastResponse> getMovieCredits(@Path("movieId") Integer movieId,
                                              @Query("api_key") String apiKey);

    @GET("movie/popular")
    Single<RatedMoviesResponse> getPopularMovies(@Query("api_key") String apiKey);

    @GET("movie/top_rated")
    Single<RatedMoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/upcoming")
    Single<RatedMoviesResponse> getUpcomingMovies(@Query("api_key") String apiKey);

    @GET("movie/{movieId}/similar")
    Single<SimilarMoviesResponse> getSimilarMovies(@Path("movieId") Integer movieId, @Query("api_key") String apiKey);

    @POST("movie/{movieId}/rating")
    Single<RatingRequest> postMovieRating(@Body RatingRequest rating,
                                          @Path("movieId") Integer movieId,
                                          @Query("api_key") String apiKey,
                                          @Query("guest_session_id") String guestSessionId);

    @GET("tv/{tvId}")
    Single<TvSeriesResponse> getTvSeriesDetails(@Path("tvId") Integer tvSeriesId, @Query("api_key") String apiKey);

    @GET("tv/{tvId}/credits")
    Single<TvCastResponse> getTvSeriesCredits(@Path("tvId") Integer tvSeriesId,
                                              @Query("api_key") String apiKey);

    @GET("tv/airing_today")
    Single<RatedTvSeriesResponse> getTvSeriesAiringToday(@Query("api_key") String apiKey);

    @GET("tv/popular")
    Single<RatedTvSeriesResponse> getPopularTvSeries(@Query("api_key") String apiKey);

    @GET("tv/top_rated")
    Single<RatedTvSeriesResponse> getTopRatedTvSeries(@Query("api_key") String apiKey);

    @GET("tv/{tvId}/similar")
    Single<SimilarTvSeriesResponse> getSimilarTvSeries(@Path("tvId") Integer tvSeriesId, @Query("api_key") String apiKey);

    @GET("tv/{tvId}/season/{seasonNumber}")
    Single<EpisodesResponse> getAllEpisodesInASeason(@Path("tvId") Integer tvSeriesId,
                                                     @Path("seasonNumber") Integer seasonNumber,
                                                     @Query("api_key") String apiKey);

    @GET("tv/{tvId}/season/{seasonNumber}/episode/{episodeNumber}")
    Single<EpisodePersist> getEpisodeByEpisodeNumber(@Path("tvId") Integer tvSeriesId,
                                                     @Path("seasonNumber") Integer seasonNumber,
                                                     @Path("episodeNumber") Integer episodeNumber,
                                                     @Query("api_key") String apiKey);

    @GET("tv/{tvId}/season/{seasonNumber}/episode/{episodeNumber}/credits")
    Single<EpisodeCastResponse> getEpisodeScreenCast(@Path("tvId") Integer tvSeriesId,
                                                     @Path("seasonNumber") Integer seasonNumber,
                                                     @Path("episodeNumber") Integer episodeNumber,
                                                     @Query("api_key") String apiKey);

    @GET("tv/{tvId}/season/{seasonNumber}/episode/{episodeNumber}/images")
    Single<ImagesListResponse> getEpisodeImages(@Path("tvId") Integer tvSeriesId,
                                                @Path("seasonNumber") Integer seasonNumber,
                                                @Path("episodeNumber") Integer episodeNumber,
                                                @Query("api_key") String apiKey);

    @GET("search/multi")
    Single<DiscoverResponseList> search(@Query("api_key") String apiKey,
                                        @Query("query") String query,
                                        @Query("page") Integer page);
    @POST("tv/{tvId}/rating")
    Single<RatingRequest> postTvSeriesRating(@Body RatingRequest rating,
                                             @Path("tvId") Integer tvSeriesId,
                                             @Query("api_key") String apiKey,
                                             @Query("guest_session_id") String guestSessionId);

    @GET("authentication/guest_session/new")
    Single<GuestSessionResponse> getGuestSession(@Query("api_key") String apiKey);
}