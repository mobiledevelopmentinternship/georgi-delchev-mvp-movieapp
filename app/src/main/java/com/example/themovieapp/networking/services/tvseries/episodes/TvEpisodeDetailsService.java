package com.example.themovieapp.networking.services.tvseries.episodes;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.image.ImagesListResponse;
import com.example.themovieapp.persistance.models.EpisodePersist;

import javax.inject.Inject;

import io.reactivex.Single;

public class TvEpisodeDetailsService {

    private final MovieApiService movieApiService;

    @Inject
    public TvEpisodeDetailsService(MovieApiService movieApiService){
        this.movieApiService = movieApiService;
    }

    public Single<EpisodePersist> getTvSeriesEpisodeDetails(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        return movieApiService.getEpisodeByEpisodeNumber(tvSeriesId, seasonNumber, episodeNumber);
    }

    public Single<ImagesListResponse> getTvEpisodeImageUrls(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        return movieApiService.getEpisodeImages(tvSeriesId, seasonNumber, episodeNumber);
    }
}