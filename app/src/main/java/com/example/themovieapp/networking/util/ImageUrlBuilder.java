package com.example.themovieapp.networking.util;

import static com.example.themovieapp.utils.Constants.IMAGE_BASE_URL;

public class ImageUrlBuilder {

    private static ImageUrlBuilder imageUrlBuilder;

    private Integer imageWidth;
    private String imageUrl;

    private ImageUrlBuilder() {
    }

    public static ImageUrlBuilder getInstance() {
        if (imageUrlBuilder == null) {
            imageUrlBuilder = new ImageUrlBuilder();
        }

        return imageUrlBuilder;
    }

    public ImageUrlBuilder withImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
        return this;
    }

    public ImageUrlBuilder withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String build() {
        StringBuilder builder = new StringBuilder();
        builder.append(IMAGE_BASE_URL);
        builder.append(this.imageWidth);
        builder.append(this.imageUrl);

        return builder.toString();
    }
}