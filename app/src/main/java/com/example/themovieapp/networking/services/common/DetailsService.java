package com.example.themovieapp.networking.services.common;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.networking.services.base.DetailsResponseReader;
import com.example.themovieapp.persistance.models.MoviePersist;

import javax.inject.Inject;

import io.reactivex.Single;

public class DetailsService implements DetailsResponseReader {

    private final MovieApiService movieApiService;

    @Inject
    public DetailsService(MovieApiService movieApiService) {
        this.movieApiService = movieApiService;
    }

    @Override
    public Single<MoviePersist> getMovieDetailsResponse(Integer id) {
        return movieApiService.getMovieDetails(id);
    }

    @Override
    public Single<SimilarMoviesResponse> getSimilarMovies(Integer id) {
        return movieApiService.getSimilarMovies(id);
    }

    @Override
    public Single<TvSeriesResponse> getTvSeriesDetailsResponse(Integer id) {
        return movieApiService.getTvSeriesDetails(id);
    }

    @Override
    public Single<SimilarTvSeriesResponse> getSimilarTvSeries(Integer id) {
        return movieApiService.getSimilarTvSeries(id);
    }
}