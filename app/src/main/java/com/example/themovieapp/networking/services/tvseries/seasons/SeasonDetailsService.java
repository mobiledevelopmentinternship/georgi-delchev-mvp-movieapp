package com.example.themovieapp.networking.services.tvseries.seasons;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.episode.EpisodesResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class SeasonDetailsService {

    private final MovieApiService movieApiService;

    @Inject
    public SeasonDetailsService(MovieApiService movieApiService){
        this.movieApiService = movieApiService;
    }

    public Single<EpisodesResponse> getEpisodeList(Integer tvSeriesId, Integer seasonNumber) {
        return movieApiService.getAllEpisodesInASeason(tvSeriesId, seasonNumber);
    }
}
