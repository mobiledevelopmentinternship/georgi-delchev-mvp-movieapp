package com.example.themovieapp.networking.retrofit.response.image;

import com.google.gson.annotations.SerializedName;

public class ImageResponse {

    @SerializedName("file_path")
    private String imageUrl;

    public ImageResponse(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
