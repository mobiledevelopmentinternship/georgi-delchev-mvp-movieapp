package com.example.themovieapp.networking.services.tvseries;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.rated.RatedTvSeriesResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class TvSeriesHomeService {

    private final MovieApiService movieApiService;

    @Inject
    public TvSeriesHomeService(MovieApiService movieApiService){
        this.movieApiService = movieApiService;
    }

    public Single<RatedTvSeriesResponse> getTopRatedResponse() {
        return movieApiService.getTopRatedTvSeries();
    }

    public Single<RatedTvSeriesResponse> getPopularResponse() {
        return movieApiService.getMostPopularTvSeries();
    }

    public Single<RatedTvSeriesResponse> getNewestResponse() {
        return movieApiService.getTvSeriesAiringToday();
    }
}