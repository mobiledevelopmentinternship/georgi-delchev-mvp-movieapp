package com.example.themovieapp.networking.retrofit.response.similarmedia;

import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SimilarTvSeriesResponse {

    @SerializedName("results")
    private List<TvSeriesResponse> tvSeries;

    public SimilarTvSeriesResponse(List<TvSeriesResponse> tvSeries) {
        this.tvSeries = tvSeries;
    }

    public List<TvSeriesResponse> getTvSeries() {
        return tvSeries;
    }

    public void setTvSeries(List<TvSeriesResponse> tvSeries) {
        this.tvSeries = tvSeries;
    }
}
