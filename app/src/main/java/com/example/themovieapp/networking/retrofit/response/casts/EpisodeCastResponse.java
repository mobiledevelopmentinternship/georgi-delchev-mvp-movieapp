package com.example.themovieapp.networking.retrofit.response.casts;

import com.example.themovieapp.networking.retrofit.response.members.GuestStarResponse;
import com.example.themovieapp.networking.retrofit.response.members.ScreenCastMemberResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EpisodeCastResponse {

    @SerializedName("guest_stars")
    private List<GuestStarResponse> guestStars;

    @SerializedName("cast")
    private List<ScreenCastMemberResponse> screenMembers;

    public EpisodeCastResponse(List<GuestStarResponse> guestStars, List<ScreenCastMemberResponse> screenMembers) {
        this.screenMembers = screenMembers;
        this.guestStars = guestStars;
    }

    public List<GuestStarResponse> getGuestStars() {
        return guestStars;
    }

    public void setGuestStars(List<GuestStarResponse> guestStars) {
        this.guestStars = guestStars;
    }

    public List<ScreenCastMemberResponse> getScreenMembers() {
        return screenMembers;
    }

    public void setScreenMembers(List<ScreenCastMemberResponse> screenMembers) {
        this.screenMembers = screenMembers;
    }
}
