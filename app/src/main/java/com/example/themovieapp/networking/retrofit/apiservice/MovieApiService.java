package com.example.themovieapp.networking.retrofit.apiservice;

import com.example.themovieapp.networking.retrofit.base.MovieApi;
import com.example.themovieapp.networking.retrofit.request.RatingRequest;
import com.example.themovieapp.networking.retrofit.response.casts.EpisodeCastResponse;
import com.example.themovieapp.networking.retrofit.response.casts.MovieCastResponse;
import com.example.themovieapp.networking.retrofit.response.casts.TvCastResponse;
import com.example.themovieapp.networking.retrofit.response.episode.EpisodesResponse;
import com.example.themovieapp.networking.retrofit.response.image.ImagesListResponse;
import com.example.themovieapp.networking.retrofit.response.rated.RatedMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.rated.RatedTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.search.DiscoverResponseList;
import com.example.themovieapp.networking.retrofit.response.rating.GuestSessionResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.MoviePersist;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

import static com.example.themovieapp.BuildConfig.API_KEY;

@Singleton
public class MovieApiService {

    private final MovieApi movieApi;

    @Inject
    public MovieApiService(MovieApi movieApi) {
        this.movieApi = movieApi;
    }

    public Single<MoviePersist> getMovieDetails(Integer movieId) {
        return movieApi.getMovieDetails(movieId, API_KEY);
    }

    public Single<MovieCastResponse> getMovieScreenCast(Integer id) {
        return movieApi.getMovieCredits(id, API_KEY);
    }

    public Single<RatedMoviesResponse> getTopRatedMovies() {
        return movieApi.getTopRatedMovies(API_KEY);
    }

    public Single<RatedMoviesResponse> getMostPopularMovies() {
        return movieApi.getPopularMovies(API_KEY);
    }

    public Single<RatedMoviesResponse> getUpcomingMovies() {
        return movieApi.getUpcomingMovies(API_KEY);
    }

    public Single<SimilarMoviesResponse> getSimilarMovies(Integer id) {
        return movieApi.getSimilarMovies(id, API_KEY);
    }

    public Single<TvSeriesResponse> getTvSeriesDetails(Integer id) {
        return movieApi.getTvSeriesDetails(id, API_KEY);
    }

    public Single<TvCastResponse> getTvSeriesScreenCast(Integer id) {
        return movieApi.getTvSeriesCredits(id, API_KEY);
    }

    public Single<RatedTvSeriesResponse> getTopRatedTvSeries() {
        return movieApi.getTopRatedTvSeries(API_KEY);
    }

    public Single<RatedTvSeriesResponse> getMostPopularTvSeries() {
        return movieApi.getPopularTvSeries(API_KEY);
    }

    public Single<RatedTvSeriesResponse> getTvSeriesAiringToday() {
        return movieApi.getTvSeriesAiringToday(API_KEY);
    }

    public Single<SimilarTvSeriesResponse> getSimilarTvSeries(Integer id) {
        return movieApi.getSimilarTvSeries(id, API_KEY);
    }

    public Single<EpisodesResponse> getAllEpisodesInASeason(Integer id, Integer seasonNumber) {
        return movieApi.getAllEpisodesInASeason(id, seasonNumber, API_KEY);
    }

    public Single<EpisodePersist> getEpisodeByEpisodeNumber(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        return movieApi.getEpisodeByEpisodeNumber(tvSeriesId, seasonNumber, episodeNumber, API_KEY);
    }

    public Single<EpisodeCastResponse> getEpisodeScreenCast(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        return movieApi.getEpisodeScreenCast(tvSeriesId, seasonNumber, episodeNumber, API_KEY);
    }

    public Single<ImagesListResponse> getEpisodeImages(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        return movieApi.getEpisodeImages(tvSeriesId, seasonNumber, episodeNumber, API_KEY);
    }

    public Single<RatingRequest> postMovieRating(Integer movieId,
                                                 RatingRequest rating,
                                                 String guestSessionId) {
        return movieApi.postMovieRating(rating, movieId, API_KEY, guestSessionId);
    }

    public Single<RatingRequest> postTvSeriesRating(Integer tvSeriesId,
                                                    RatingRequest rating,
                                                    String guestSessionId) {
        return movieApi.postTvSeriesRating(rating, tvSeriesId, API_KEY, guestSessionId);
    }

    public Single<GuestSessionResponse> getGuestSessionId() {
        return movieApi.getGuestSession(API_KEY);
    }

    public Single<DiscoverResponseList> discover(String searchPhrase, Integer page) {
        return movieApi.search(API_KEY, searchPhrase, page);
    }
}