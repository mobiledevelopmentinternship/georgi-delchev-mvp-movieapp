package com.example.themovieapp.networking.services.common;

import com.example.themovieapp.networking.retrofit.apiservice.MovieApiService;
import com.example.themovieapp.networking.retrofit.response.casts.MovieCastResponse;
import com.example.themovieapp.networking.retrofit.response.casts.TvCastResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class CreditsService {

    private final MovieApiService movieApiService;

    @Inject
    public CreditsService(MovieApiService movieApiService) {
        this.movieApiService = movieApiService;
    }

    public Single<MovieCastResponse> getMovieCreditsResponse(Integer id) {
        return movieApiService.getMovieScreenCast(id);
    }

    public Single<TvCastResponse> getTvSeriesCreditsResponse(Integer id) {
        return movieApiService.getTvSeriesScreenCast(id);
    }
}