package com.example.themovieapp.presenters.details.tv.tvseasondetails;

import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts.SeasonEpisodeListContracts;

import java.util.List;

import javax.inject.Inject;

public class TvSeasonEpisodeListPresenter extends BasePresenter implements SeasonEpisodeListContracts.Presenter {

    private final SeasonEpisodeListContracts.View view;
    private final List<EpisodePersist> episodes;
    private final Integer seasonId;
    private final Integer seasonNumber;
    private final Integer tvSeriesId;
    private final Integer allSeasonsCount;
    private final String tvSeriesTitle;

    @Inject
    public TvSeasonEpisodeListPresenter(SeasonEpisodeListContracts.View view,
                                        List<EpisodePersist> episodes,
                                        Integer seasonId,
                                        Integer seasonNumber,
                                        Integer tvSeriesId,
                                        Integer allSeasonsCount,
                                        String title) {
        super();
        this.view = view;
        this.episodes = episodes;
        this.seasonId = seasonId;
        this.seasonNumber = seasonNumber;
        this.tvSeriesId = tvSeriesId;
        this.allSeasonsCount = allSeasonsCount;
        this.tvSeriesTitle = title;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        view.loadEpisodeList(episodes);
    }

    @Override
    public void onSelected(Integer position) {
        view.showEpisodeDetails(seasonId, seasonNumber, position, tvSeriesId, allSeasonsCount, tvSeriesTitle);
    }

    @Override
    public List<EpisodePersist> getEpisodes() {
        return episodes;
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}
