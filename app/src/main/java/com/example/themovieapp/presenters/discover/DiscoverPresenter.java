package com.example.themovieapp.presenters.discover;

import android.text.TextUtils;
import android.util.Log;

import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.search.DiscoverResponseList;
import com.example.themovieapp.networking.retrofit.response.search.SearchResponse;
import com.example.themovieapp.networking.services.common.DiscoverService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.discover.contracts.DiscoverContracts;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.example.themovieapp.utils.Constants.CURRENT_PAGE_INITIAL_VALUE;
import static com.example.themovieapp.utils.Constants.MAX_PAGES_INITIAL_VALUE;
import static com.example.themovieapp.utils.Constants.TV_SERIES_MEDIA_TYPE;

public class DiscoverPresenter extends BasePresenter implements DiscoverContracts.Presenter {

    private final DiscoverContracts.View view;

    @Inject
    DiscoverService discoverNetworkService;

    private Integer currentPage;
    private String searchPhrase;
    private Integer totalPages;
    private boolean shouldIgnoreResults;
    private List<SearchResponse> previousSearchResult;

    public DiscoverPresenter(DiscoverContracts.View view) {
        super();
        this.view = view;
        view.setPresenter(this);
        currentPage = CURRENT_PAGE_INITIAL_VALUE;
        totalPages = MAX_PAGES_INITIAL_VALUE;
        previousSearchResult = new ArrayList<>();
    }

    @Override
    public void start() {
        view.setResultListVisibility(false);
    }

    @Override
    public void onSearchResultClick(SearchResponse response) {
        if (TextUtils.equals(response.getMediaType(), TV_SERIES_MEDIA_TYPE)) {
            view.loadTvSeriesDetails(response.getId());
        } else {
            view.loadMovieDetails(response.getId());
        }
    }

    @Override
    public void discover(String searchPhrase) {
        if (TextUtils.equals(this.searchPhrase, searchPhrase) && currentPage <= totalPages) {
            discoverResults(this.searchPhrase);
            view.incrementCurrentPageNumber();
        } else {
            resetDiscoverQuery(searchPhrase);
        }
    }

    private void resetDiscoverQuery(String searchPhrase) {
        resetFields();
        view.setResultListVisibility(false);
        view.setProgressBarVisibility(true);
        discoverResults(searchPhrase);
        this.searchPhrase = searchPhrase;
    }

    private void resetFields() {
        previousSearchResult = new ArrayList<>();
        currentPage = CURRENT_PAGE_INITIAL_VALUE;
        view.resetPagesForNewQuery();
    }

    private void discoverResults(String searchPhrase) {
        subscribeSingle(
                discoverNetworkService.discover(searchPhrase, currentPage),
                this::onRequestSuccess,
                this::onRequestError
        );
    }

    @Override
    public void showResults() {
        view.setProgressBarVisibility(false);
        view.setResultListVisibility(true);
    }

    @Override
    public void loadMoreResults() {
        discover(searchPhrase);
    }

    @Override
    public void setLoadingCompleted() {
        view.setLoading(false);
    }

    @Override
    public void onStopButtonClick(boolean isStopped) {
        this.shouldIgnoreResults = isStopped;
    }

    @Override
    public void onQueryCleared() {
        resetFields();
    }

    private void onRequestError(Throwable throwable) {
        Log.e(this.getClass().getSimpleName(), throwable.getMessage());
        view.showError(R.string.discover_default_error_message);
    }

    private void onRequestSuccess(DiscoverResponseList queryResult) {
        if (queryResult.getResult().isEmpty()) {
            view.showError(R.string.error_discover_cant_find);
            view.setProgressBarVisibility(false);
        } else if (currentPage == CURRENT_PAGE_INITIAL_VALUE
                && !shouldIgnoreResults
                && !queryResult.getResult().isEmpty()) {
            this.totalPages = queryResult.getTotalPages();
            view.setMaxPagesForQuery(totalPages);
            view.setResultsAdapter(queryResult.getResult());
            previousSearchResult.addAll(queryResult.getResult());
        } else if (currentPage > CURRENT_PAGE_INITIAL_VALUE && !shouldIgnoreResults) {
            if (checkForDuplicateSearch(queryResult)) {
                return;
            } else {
                view.addResults(queryResult.getResult());
            }
        } else {
            shouldIgnoreResults = false;
            this.searchPhrase = "";
        }

        checkPageNumber();
    }

    private boolean checkForDuplicateSearch(DiscoverResponseList queryResult) {
        List<SearchResponse> discovered = queryResult.getResult();
        for (SearchResponse searchResponse : discovered) {
            for (SearchResponse response : previousSearchResult) {
                if (response.equals(searchResponse)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void checkPageNumber() {
        if (currentPage < totalPages) {
            currentPage++;
        }
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}