package com.example.themovieapp.presenters.home.tvserieshome.contracts;

import com.example.themovieapp.presenters.home.base.BaseHomeContracts;

public interface TVSeriesHomeContracts {

    interface View extends BaseHomeContracts.View {

        void loadSelectedTvSeries(Integer tvSeriesId, Integer mediaTypeId);

        void showError(Integer messageId);
    }

    interface Presenter extends BaseHomeContracts.Presenter {

    }
}
