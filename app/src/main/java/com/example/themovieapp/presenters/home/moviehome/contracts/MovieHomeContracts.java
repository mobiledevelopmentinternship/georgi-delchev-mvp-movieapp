package com.example.themovieapp.presenters.home.moviehome.contracts;

import com.example.themovieapp.presenters.home.base.BaseHomeContracts;

public interface MovieHomeContracts extends BaseHomeContracts {

    interface View extends BaseHomeContracts.View {

        void loadSelectedMovie(Integer movieId, Integer mediaTypeId);

        void showError(Integer messageId);
    }

    interface Presenter extends BaseHomeContracts.Presenter {

    }
}
