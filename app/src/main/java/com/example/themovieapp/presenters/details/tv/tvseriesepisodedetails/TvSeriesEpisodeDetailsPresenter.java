package com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.casts.EpisodeCastResponse;
import com.example.themovieapp.networking.retrofit.response.episode.EpisodesResponse;
import com.example.themovieapp.networking.retrofit.response.image.ImagesListResponse;
import com.example.themovieapp.networking.services.tvseries.episodes.TvEpisodeCreditsService;
import com.example.themovieapp.networking.services.tvseries.episodes.TvEpisodeDetailsService;
import com.example.themovieapp.networking.services.tvseries.seasons.SeasonDetailsService;
import com.example.themovieapp.networking.util.ResponseConverter;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.GuestStarPersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.persistance.services.EpisodePersistService;
import com.example.themovieapp.persistance.services.GuestStarPersistService;
import com.example.themovieapp.persistance.services.ScreenCastPersistService;
import com.example.themovieapp.persistance.services.TvSeriesPersistService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.contracts.TvSeriesEpisodeDetailsContracts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.internal.operators.completable.CompletableFromAction;

import static com.example.themovieapp.utils.Constants.API_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Constants.CAST_TYPES_COUNT;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.convertDateToString;
import static com.example.themovieapp.utils.Utils.convertToMember;
import static com.example.themovieapp.utils.Utils.filterGuestStars;
import static com.example.themovieapp.utils.Utils.filterScreenCastMembers;
import static com.example.themovieapp.utils.Utils.setGuestStarEpisodeName;

public class TvSeriesEpisodeDetailsPresenter extends BasePresenter implements TvSeriesEpisodeDetailsContracts.Presenter {

    private final TvSeriesEpisodeDetailsContracts.View view;
    @Inject
    EpisodePersistService episodeDbService;
    @Inject
    TvEpisodeDetailsService tvEpisodeDetailsNetworkService;
    @Inject
    TvEpisodeCreditsService tvEpisodeNetworkCreditsService;
    @Inject
    SeasonDetailsService seasonDetailsNetworkService;
    @Inject
    ScreenCastPersistService screenCastPersistService;
    @Inject
    GuestStarPersistService guestStarPersistService;
    @Inject
    TvSeriesPersistService tvSeriesPersistService;

    private EpisodePersist episodeToShow;
    private final Integer seasonId;
    private final Integer seasonNumber;
    private final Integer episodeNumber;
    private final Integer tvSeriesId;
    private final Integer numberOfSeasons;
    private final String tvSeriesTitle;
    private final List<Member> members;
    private List<EpisodePersist> allEpisodesInASeason;
    private List<Member> episodeGuestStars;
    private List<Member> episodeScreenCast;
    private List<String> episodeImages;
    private String episodeCoverImageUrl;
    private int castCounter;

    @Inject
    public TvSeriesEpisodeDetailsPresenter(TvSeriesEpisodeDetailsContracts.View view,
                                           Integer seasonId,
                                           Integer seasonNumber,
                                           Integer episodeNumber,
                                           Integer tvSeriesId,
                                           Integer numberOfSeasons, String title) {
        super();
        this.view = view;
        view.setPresenter(this);
        this.seasonId = seasonId;
        this.seasonNumber = seasonNumber;
        this.episodeNumber = episodeNumber;
        this.tvSeriesId = tvSeriesId;
        this.numberOfSeasons = numberOfSeasons;
        this.tvSeriesTitle = title;
        castCounter = CAST_TYPES_COUNT;
        episodeImages = new ArrayList<>();
        allEpisodesInASeason = new ArrayList<>();
        members = new ArrayList<>();
        episodeGuestStars = new ArrayList<>();
        episodeScreenCast = new ArrayList<>();
    }

    @Override
    public void start() {
        view.setProgressBarVisibility(true);

        getEpisodeFromDb();
    }

    private void getEpisodeFromDb() {
        subscribeSingle(
                episodeDbService.getAllEpisodesForASeason(seasonId, seasonNumber),
                this::onAllEpisodesQuerySuccess,
                this::onAllEpisodesQueryError
        );
    }

    private void onAllEpisodesQueryError(Throwable throwable) {
        getEpisodeFromApi(tvSeriesId, seasonNumber, episodeNumber);
    }

    private void onAllEpisodesQuerySuccess(List<EpisodePersist> episodes) {
        allEpisodesInASeason = episodes;
        Collections.sort(allEpisodesInASeason);
        findCorrectEpisode(allEpisodesInASeason, episodeNumber);
    }

    private void findCorrectEpisode(List<EpisodePersist> result, Integer episodeNumber) {
        for (EpisodePersist episodePersist : result) {
            if (episodePersist.getEpisodeNumber().equals(episodeNumber)) {
                episodeToShow = episodePersist;
                break;
            }
        }

        if (episodeToShow == null) {
            getEpisodeFromApi(tvSeriesId, seasonNumber, episodeNumber);
        } else {
            loadDataFromPersist();
        }
    }

    private void loadDataFromPersist() {
        episodeCoverImageUrl = episodeToShow.getPosterUrl();
        view.setEpisodeName(episodeToShow.getName());
        view.setEpisodeOverview(episodeToShow.getOverview());
        view.setEpisodeReleaseDate(convertDateToString(episodeToShow.getAirDate()));
        view.setMediaType(R.string.tv_episode_media_type);
        view.setAdditionalInformationTitle(R.string.additional_information_title_episode);
        getTvSeriesPoster();
        getImageUrls();
        getOtherEpisodesNames();
        getEpisodeScreenCast();
    }

    private void getTvSeriesPoster() {
        subscribeSingle(
                tvSeriesPersistService.getTvSeriesById(tvSeriesId),
                this::onGetTvSeriesByIdSuccess,
                this::onGetTvSeriesByIdError
        );
    }

    private void onGetTvSeriesByIdError(Throwable throwable) {
        view.setPosterImageInOverviewCard(buildFullImageUrl(episodeToShow.getPosterUrl(), API_IMAGE_WIDTH));
    }

    private void onGetTvSeriesByIdSuccess(TvSeriesPersist tvSeries) {
        view.setPosterImageInOverviewCard(buildFullImageUrl(tvSeries.getPosterUrl(), API_IMAGE_WIDTH));
    }

    private void getImageUrls() {
        subscribeSingle(
                tvEpisodeDetailsNetworkService.getTvEpisodeImageUrls(tvSeriesId, seasonNumber, episodeNumber),
                this::onTvEpisodeImageUrlsRequestSuccess,
                this::onTvEpisodeImageUrlsRequestError
        );
    }

    private void onTvEpisodeImageUrlsRequestError(Throwable throwable) {
        view.hideViewPager();
    }

    private void onTvEpisodeImageUrlsRequestSuccess(ImagesListResponse imagesListResponse) {
        if (imagesListResponse.getImages().isEmpty()) {
            view.hideViewPager();
        } else {
            episodeImages = ResponseConverter.extractImageUrls(imagesListResponse);
            view.setupEpisodeImagesAdapter();
        }
    }

    private void getOtherEpisodesNames() {
        view.setupEpisodesAdapter();
    }

    private void getEpisodeGuestStars() {
        subscribeSingle(
                episodeDbService.getEpisodeGuestStars(episodeToShow.getName()),
                this::onEpisodeGuestStarsQuerySuccess,
                this::onEpisodeGuestStarsQueryError
        );
    }

    private void onEpisodeGuestStarsQueryError(Throwable throwable) {
        getEpisodeCreditsFromApi(tvSeriesId, seasonNumber, episodeNumber);
    }

    private void onEpisodeGuestStarsQuerySuccess(List<GuestStarPersist> guestStars) {
        if (guestStars.isEmpty() && isConnected()) {
            getEpisodeCreditsFromApi(tvSeriesId, seasonNumber, episodeNumber);
        } else if (guestStars.isEmpty()) {
            castCounter--;
            checkCounter();
        } else {
            episodeGuestStars.addAll(filterGuestStars(guestStars));
            castCounter--;
            members.addAll(episodeGuestStars);
            checkCounter();
        }
    }

    private void checkCounter() {
        if (castCounter == 0) {
            view.setupScreenCastAdapter();
            view.setProgressBarVisibility(false);
        }
    }

    private void getEpisodeScreenCast() {
        subscribeSingle(
                tvSeriesPersistService.getTvSeriesScreenCastMembers(tvSeriesTitle),
                this::onEpisodeScreenCastQuerySuccess,
                this::onEpisodeScreenCastQueryError
        );
    }

    private void onEpisodeScreenCastQueryError(Throwable throwable) {
        getEpisodeCreditsFromApi(tvSeriesId, seasonNumber, episodeNumber);
    }

    private void onEpisodeScreenCastQuerySuccess(List<ScreenCastMemberPersist> screenCastMemberPersists) {
        if (screenCastMemberPersists.isEmpty()) {
            getEpisodeCreditsFromApi(tvSeriesId, seasonNumber, episodeNumber);
        } else {
            episodeScreenCast.addAll(filterScreenCastMembers(screenCastMemberPersists));
            castCounter--;
            checkCounter();
            members.addAll(episodeScreenCast);
            getEpisodeGuestStars();
        }
    }

    private void getEpisodeFromApi(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        subscribeSingle(
                tvEpisodeDetailsNetworkService.getTvSeriesEpisodeDetails(tvSeriesId, seasonNumber, episodeNumber),
                this::onEpisodeRequestSuccess,
                this::onEpisodeRequestError
        );
    }

    private void onEpisodeRequestError(Throwable throwable) {
        view.showError(R.string.error_network_connection_unavailable);
    }

    private void loadDataFromApi() {
        episodeCoverImageUrl = episodeToShow.getPosterUrl();
        view.setEpisodeName(episodeToShow.getName());
        view.setEpisodeOverview(episodeToShow.getOverview());
        view.setEpisodeReleaseDate(convertDateToString(episodeToShow.getAirDate()));
        view.setMediaType(R.string.tv_episode_media_type);
        view.setAdditionalInformationTitle(R.string.additional_information_title_episode);
        view.setPosterImageInOverviewCard(buildFullImageUrl(episodeCoverImageUrl, API_IMAGE_WIDTH));
        getImageUrls();
        getAllEpisodesInASeasonFromApi();
        getEpisodeCreditsFromApi(tvSeriesId, seasonNumber, episodeNumber);
    }

    private void getAllEpisodesInASeasonFromApi() {
        subscribeSingle(
                seasonDetailsNetworkService.getEpisodeList(tvSeriesId, seasonNumber),
                this::onEpisodeListRequestSuccess,
                this::onEpisodeListRequestError
        );
    }

    private void onEpisodeListRequestError(Throwable throwable) {
        view.showError(R.string.error_network_connection_unavailable);
    }

    private void onEpisodeListRequestSuccess(EpisodesResponse episodesResponse) {
        allEpisodesInASeason = episodesResponse.getEpisodes();
        Collections.sort(allEpisodesInASeason);
        view.setupEpisodesAdapter();
    }

    private void onEpisodeRequestSuccess(EpisodePersist episodePersist) {
        episodeToShow = episodePersist;
        loadDataFromApi();
    }

    private void getEpisodeCreditsFromApi(Integer tvSeriesId, Integer seasonNumber, Integer episodeNumber) {
        subscribeSingle(
                tvEpisodeNetworkCreditsService.getTvSeriesCreditsResponse(tvSeriesId, seasonNumber, episodeNumber),
                this::onEpisodeCreditsRequestSuccess,
                this::onEpisodeCreditsRequestError
        );
    }

    private void onEpisodeCreditsRequestError(Throwable throwable) {
        view.setupScreenCastAdapter();
    }

    private void onEpisodeCreditsRequestSuccess(EpisodeCastResponse episodeCastResponse) {
        List<ScreenCastMemberPersist> screenCast = null;
        List<GuestStarPersist> guestStars;

        if (!this.episodeScreenCast.isEmpty()) {
            guestStars = ResponseConverter.
                    convertGuestStarResponseToPersist(episodeCastResponse.getGuestStars(),
                            tvSeriesId);

            episodeGuestStars = convertToMember(ResponseConverter.
                    convertGuestStarResponseToPersist(episodeCastResponse.getGuestStars(),
                            tvSeriesId));

            setGuestStarEpisodeName(guestStars, episodeToShow);

            members.addAll(episodeGuestStars);
            saveGuestStars(guestStars);
        } else {
            if (episodeCastResponse.getScreenMembers() != null && !episodeCastResponse.getScreenMembers().isEmpty()) {
                screenCast = ResponseConverter.
                        convertScreenCastMemberResponseToPersist(episodeCastResponse.getScreenMembers(),
                                tvSeriesId, App.getInstance().getString(R.string.tv_series_media_type));

                members.addAll(screenCast);
            }

            if (episodeCastResponse.getGuestStars() != null && !episodeCastResponse.getGuestStars().isEmpty()) {
                guestStars = ResponseConverter.
                        convertGuestStarResponseToPersist(episodeCastResponse.getGuestStars(),
                                tvSeriesId);

                members.addAll(guestStars);
                saveCastMembers(screenCast, guestStars);
            } else {
                saveScreenCast(screenCast);
            }
        }
    }

    private void saveScreenCast(List<ScreenCastMemberPersist> screenCast) {
        subscribeCompletable(
                createCompletable(() -> screenCastPersistService.insertAll(screenCast)),
                this::onCastInsertSuccess);
    }

    private void saveGuestStars(List<GuestStarPersist> guestStars) {
        subscribeCompletable(createCompletable(() -> guestStarPersistService.insertAll(guestStars)),
                this::onCastInsertSuccess);
    }

    private void saveCastMembers(List<ScreenCastMemberPersist> screenCast, List<GuestStarPersist> guestStars) {
        subscribeCompletable(chainCompletables(
                new CompletableFromAction(() -> guestStarPersistService.insertAll(guestStars)),
                new CompletableFromAction(() -> screenCastPersistService.insertAll(screenCast))
                ),
                this::onCastInsertSuccess);
    }

    private void onCastInsertSuccess() {
        view.setupScreenCastAdapter();
        view.setProgressBarVisibility(false);
    }

    @Override
    public void cancelTasks() {
        dispose();
    }

    @Override
    public void loadAllEpisodes() {
        view.showAllEpisodes(tvSeriesId, numberOfSeasons);
    }

    @Override
    public void onEpisodeClicked(Integer episodeNumber) {
        if (!episodeNumber.equals(this.episodeNumber)) {
            view.showAnotherEpisodeFromTheSeason(tvSeriesId, seasonId, seasonNumber, episodeNumber, numberOfSeasons);
        }
    }

    @Override
    public List<EpisodePersist> getEpisodesInSeason() {
        return allEpisodesInASeason;
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }

    @Override
    public List<String> getEpisodeImages() {
        return episodeImages;
    }

    @Override
    public List<Member> getScreenCast() {
        return members;
    }
}