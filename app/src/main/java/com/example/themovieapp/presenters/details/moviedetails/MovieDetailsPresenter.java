package com.example.themovieapp.presenters.details.moviedetails;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.MoviePersistService;
import com.example.themovieapp.presenters.details.base.BaseDetailsPresenter;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.presenters.details.moviedetails.contracts.MovieDetailsContracts;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.example.themovieapp.utils.Constants.BIG_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.convertDateToString;
import static com.example.themovieapp.utils.Utils.convertFromWatchableToMovie;

public class MovieDetailsPresenter extends BaseDetailsPresenter implements MovieDetailsContracts.Presenter {

    private final MovieDetailsContracts.View view;
    @Inject
    MoviePersistService moviePersistService;

    private final Integer id;
    private final String mediaType;
    private final List<Member> movieCastMembers;

    @Inject
    public MovieDetailsPresenter(BaseDetailsContracts.View view, Integer id, String mediaType) {
        super(view, id, mediaType);
        this.id = id;
        this.view = (MovieDetailsContracts.View) view;
        view.setPresenter(this);
        this.mediaType = mediaType;
        movieCastMembers = new ArrayList<>();
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }

    @Override
    protected void getDetailsFromDb() {
        subscribeSingle(
                moviePersistService.getMovieById(id),
                this::onQuerySuccess,
                this::onQueryError
        );
    }

    private void onQueryError(Throwable throwable) {
        getDetailsFromApi();
    }

    @Override
    protected void persistWatchable(Watchable watchable) {
        if (watchable instanceof MoviePersist) {
            moviePersistService.insert(convertFromWatchableToMovie(watchable));
        }
    }

    private void onQuerySuccess(MoviePersist result) {
        watchable = result;
        title = result.getTitle();
        if (!isConnected()) {
            view.setupScreenCastAdapter();
            view.hideSimilarMedia();
        } else {
            getMovieCrewMembersFromDb();
        }
        getSimilarMedia();
        view.setName(title);
        view.setMediaType(R.string.movie_media_type);
        view.setOverview(result.getOverview());
        view.setAdditionalInformationTitle(R.string.details_similar_movies_title);
        view.setPosterImage(buildFullImageUrl(result.getPosterUrl(), BIG_IMAGE_WIDTH));
        view.setReleaseDate(convertDateToString(result.getReleaseDate()));
    }

    private void getMovieScreenCastFromDb() {
        subscribeSingle(
                moviePersistService.getMovieScreenCastMembers(convertFromWatchableToMovie(watchable).getTitle()),
                this::onMovieScreenCastMembersQuerySuccess,
                this::onMovieScreenCastMembersQueryError
        );
    }

    private void onMovieScreenCastMembersQueryError(Throwable throwable) {
        getScreenCastFromApi(mediaType);
    }

    private void onMovieScreenCastMembersQuerySuccess(List<ScreenCastMemberPersist> screenCastMembers) {
        if (screenCastMembers.isEmpty()) {
            getScreenCastFromApi(mediaType);
        } else {
            screenCastCounter--;
            this.screenCast.addAll(screenCastMembers);
            checkCastCounter();
        }
    }

    private void getMovieCrewMembersFromDb() {
        subscribeSingle(
                moviePersistService.getMovieCrewMembers(convertFromWatchableToMovie(watchable).getTitle()),
                this::onMovieCrewMembersQuerySuccess,
                this::onMovieCrewMembersQueryError
        );
    }

    private void onMovieCrewMembersQueryError(Throwable throwable) {
        getScreenCastFromApi(mediaType);
    }

    private void onMovieCrewMembersQuerySuccess(List<CrewMemberPersist> crewMembers) {
        if (crewMembers.isEmpty()) {
            getScreenCastFromApi(mediaType);
        } else {
            screenCastCounter--;
            this.crewMembers.addAll(crewMembers);
            checkCastCounter();
            getMovieScreenCastFromDb();
        }
    }

    private void checkCastCounter() {
        if (screenCastCounter == 0) {
            view.setupScreenCastAdapter();
        }
    }

    @Override
    public void loadRating() {
        if (!isConnected()){
            view.showError(R.string.error_network_connection_unavailable);
        } else {
            view.loadRatingDialog(id, mediaType, convertFromWatchableToMovie(watchable).getTitle());
        }
    }

    @Override
    public void addToCollection() {
        view.loadAddToCollectionDialog(watchable);
    }

    @Override
    public List<Watchable> getWatchables() {
        return similarWatchables;
    }

    @Override
    public List<Member> getScreenCast() {
        movieCastMembers.addAll(screenCast);
        movieCastMembers.addAll(crewMembers);
        return movieCastMembers;
    }
}