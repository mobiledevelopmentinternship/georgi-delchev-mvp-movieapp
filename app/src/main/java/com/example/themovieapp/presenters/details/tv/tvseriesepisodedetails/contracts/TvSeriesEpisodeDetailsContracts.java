package com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.contracts;

import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.ui.fragments.base.BaseView;

import java.util.List;

public interface TvSeriesEpisodeDetailsContracts {

    interface View extends BaseView<Presenter> {

        void showError(Integer messageId);

        void setupEpisodeImagesAdapter();

        void hideViewPager();

        void setEpisodeName(String name);

        void setEpisodeReleaseDate(String date);

        void setMediaType(Integer mediaTypeId);

        void setPosterImageInOverviewCard(String url);

        void setEpisodeOverview(String overview);

        void setAdditionalInformationTitle(Integer titleId);

        void setupScreenCastAdapter();

        void setupEpisodesAdapter();

        void showAnotherEpisodeFromTheSeason(Integer tvSeriesId,
                                             Integer seasonId,
                                             Integer seasonNumber,
                                             Integer episodeNumber,
                                             Integer numberOfSeasons);

        void showAllEpisodes(Integer tvSeriesId, Integer numberOfSeasons);
    }

    interface Presenter extends IBasePresenter {

        void cancelTasks();

        void loadAllEpisodes();

        void onEpisodeClicked(Integer episodeNumber);

        List<EpisodePersist> getEpisodesInSeason();

        List<String> getEpisodeImages();

        List<Member> getScreenCast();
    }
}
