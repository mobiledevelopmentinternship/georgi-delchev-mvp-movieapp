package com.example.themovieapp.presenters.details.base;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.casts.MovieCastResponse;
import com.example.themovieapp.networking.retrofit.response.casts.TvCastResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarMoviesResponse;
import com.example.themovieapp.networking.retrofit.response.similarmedia.SimilarTvSeriesResponse;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.networking.services.common.CreditsService;
import com.example.themovieapp.networking.services.common.DetailsService;
import com.example.themovieapp.networking.util.ResponseConverter;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.CrewMemberPersistService;
import com.example.themovieapp.persistance.services.ScreenCastPersistService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.internal.operators.completable.CompletableFromAction;

import static com.example.themovieapp.utils.Constants.BIG_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Constants.CAST_TYPES_COUNT;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.convertToWatchable;
import static com.example.themovieapp.utils.Utils.filterCrewMemebers;
import static com.example.themovieapp.utils.Utils.filterScreenCastMembers;
import static com.example.themovieapp.utils.Utils.setCrewMembersMediaType;
import static com.example.themovieapp.utils.Utils.setScreenCastMediaType;

public abstract class BaseDetailsPresenter extends BasePresenter implements BaseDetailsContracts.Presenter {

    private final BaseDetailsContracts.View view;
    @Inject
    CreditsService creditsNetworkService;
    @Inject
    DetailsService detailsNetworkService;
    @Inject
    ScreenCastPersistService screenCastPersistService;
    @Inject
    CrewMemberPersistService crewMemberPersistService;

    private final Integer id;
    private final String mediaType;
    protected Watchable watchable;
    protected String title;
    protected List<Watchable> similarWatchables;
    protected List<CrewMemberPersist> crewMembers;
    protected List<ScreenCastMemberPersist> screenCast;
    protected int screenCastCounter;
    private Integer similarMediaId;

    protected BaseDetailsPresenter(BaseDetailsContracts.View view, Integer id, String mediaType) {
        super();
        this.view = view;
        view.setPresenter(this);
        this.id = id;
        this.mediaType = mediaType;
        crewMembers = new ArrayList<>();
        screenCast = new ArrayList<>();
        screenCastCounter = CAST_TYPES_COUNT;
        similarMediaId = 0;
    }

    @Override
    public void start() {
        view.setProgressBarVisibility(true);

        getDetailsFromDb();
    }

    protected abstract void getDetailsFromDb();

    protected void getDetailsFromApi() {
        if (mediaType.equals(App.getInstance().getString(R.string.tv_series_media_type))) {
            subscribeSingle(
                    detailsNetworkService.getTvSeriesDetailsResponse(id),
                    this::setTvSeriesDataInView,
                    this::onDetailsNetworkError
            );
        } else {
            subscribeSingle(
                    detailsNetworkService.getMovieDetailsResponse(id),
                    this::setMovieDataInView,
                    this::onDetailsNetworkError
            );
        }
    }

    private void setTvSeriesDataInView(TvSeriesResponse response) {
        try {
            TvSeriesPersist result = ResponseConverter.convertTvResponseToPersist(response);
            persistWatchable(result);
            getScreenCastFromApi(mediaType);
            getSimilarMedia();
            watchable = result;
            view.setName(result.getTitle());
            view.setOverview(result.getOverview());
            view.setPosterImage(buildFullImageUrl(result.getPosterUrl(), BIG_IMAGE_WIDTH));
            view.setReleaseDate(Utils.convertDateToString(result.getAiringDate()));
            view.setMediaType(R.string.tv_series_media_type);
            view.setAdditionalInformationTitle(R.string.details_similar_tv_series_title);
        } catch (Throwable throwable) {
            view.showError(R.string.error_network_connection_unavailable);
        }
    }

    protected abstract void persistWatchable(Watchable watchable);

    private void setMovieDataInView(MoviePersist result) {
        persistWatchable(result);
        getScreenCastFromApi(mediaType);
        watchable = result;
        view.setName(title);
        view.setOverview(result.getOverview());
        view.setName(result.getTitle());
        view.setPosterImage(buildFullImageUrl(result.getPosterUrl(), BIG_IMAGE_WIDTH));
        view.setReleaseDate(Utils.convertDateToString(result.getReleaseDate()));
        view.setMediaType(R.string.movie_media_type);
        view.setAdditionalInformationTitle(R.string.details_similar_movies_title);
    }

    protected void getSimilarMedia() {
        if (mediaType.equals(App.getInstance().getString(R.string.tv_series_media_type))) {
            subscribeSingle(
                    detailsNetworkService.getSimilarTvSeries(id),
                    this::onSimilarTvSeriesSuccess,
                    this::onDetailsNetworkError
            );
        } else {
            subscribeSingle(
                    detailsNetworkService.getSimilarMovies(id),
                    this::onSimilarMoviesSuccess,
                    this::onDetailsNetworkError
            );
        }
    }

    private void onSimilarMoviesSuccess(SimilarMoviesResponse similarMoviesResponse) {
        if (similarMoviesResponse.getMovies().isEmpty()) {
            view.hideSimilarMedia();
            return;
        }

        similarWatchables = convertToWatchable(similarMoviesResponse.getMovies());
        view.setupSimilarWatchablesAdapter();
        view.setProgressBarVisibility(false);
        view.setAllEpisodesButtonVisibility(false);
    }

    private void onSimilarTvSeriesSuccess(SimilarTvSeriesResponse similarTvSeriesResponse) {
        if (similarTvSeriesResponse.getTvSeries().isEmpty()) {
            view.hideSimilarMedia();
            return;
        }

        List<TvSeriesPersist> similarTvSeries = new ArrayList<>();
        for (TvSeriesResponse tv : similarTvSeriesResponse.getTvSeries()) {
            try {
                similarTvSeries.add(ResponseConverter.convertTvResponseToPersist(tv));
            } catch (Throwable ignored) {
            }
        }
        similarWatchables = convertToWatchable(similarTvSeries);
        view.setupSimilarWatchablesAdapter();
        view.setProgressBarVisibility(false);
        view.setAllEpisodesButtonVisibility(true);
    }

    protected void getScreenCastFromApi(String mediaType) {
        if (mediaType.equals(App.getInstance().getString(R.string.tv_series_media_type))) {
            subscribeSingle(
                    creditsNetworkService.getTvSeriesCreditsResponse(id),
                    this::onTvSeriesCastSuccess,
                    this::onDetailsNetworkError
            );
        } else {
            subscribeSingle(
                    creditsNetworkService.getMovieCreditsResponse(id),
                    this::onMovieCastRequestSuccess,
                    this::onDetailsNetworkError
            );
        }
    }

    private void onMovieCastRequestSuccess(MovieCastResponse movieCastResponse) {

        if (movieCastResponse.getScreenCastMembers() != null && !movieCastResponse.getScreenCastMembers().isEmpty()) {
            screenCast = ResponseConverter.
                    convertScreenCastMemberResponseToPersist(movieCastResponse.getScreenCastMembers(), id, mediaType);

            setScreenCastMediaType(screenCast, watchable);
        }

        if (movieCastResponse.getCrewMembers() != null && !movieCastResponse.getCrewMembers().isEmpty()) {
            crewMembers = ResponseConverter.
                    convertCrewMemberResponseToPersist(movieCastResponse.getCrewMembers(), mediaType, id);

            setCrewMembersMediaType(crewMembers, watchable);
        }

        saveCast();
        getSimilarMedia();
        view.setupScreenCastAdapter();
    }

    private void onTvSeriesCastSuccess(TvCastResponse tvCastResponse) {
        if (tvCastResponse.getScreenCastMembers() != null && !tvCastResponse.getScreenCastMembers().isEmpty()) {
            screenCast = ResponseConverter.
                    convertScreenCastMemberResponseToPersist(tvCastResponse.getScreenCastMembers(), id, mediaType);

            setScreenCastMediaType(screenCast, watchable);
        }

        if (tvCastResponse.getCrewMembers() != null && !tvCastResponse.getCrewMembers().isEmpty()) {
            crewMembers = ResponseConverter.
                    convertCrewMemberResponseToPersist(tvCastResponse.getCrewMembers(), mediaType, id);

            setCrewMembersMediaType(crewMembers, watchable);
        }

        saveCast();
        view.setupScreenCastAdapter();
    }

    @Override
    public void onSimilarMediaClicked(Integer id, Integer mediaTypeId) {
        if (!similarMediaId.equals(id)) {
            similarMediaId = id;
            view.showSimilarWatchable(id, mediaTypeId);
        }
    }

    private void saveCast() {
        subscribeCompletable(chainCompletables(
                new CompletableFromAction(() -> screenCastPersistService.insertAll(filterScreenCastMembers(screenCast))),
                new CompletableFromAction(() -> crewMemberPersistService.insertAll(filterCrewMemebers(crewMembers)))));
    }

    @Override
    public void cancelTasks() {
        dispose();
    }

    private void onDetailsNetworkError(Throwable throwable) {
        view.hideSimilarMedia();
        view.showError(R.string.error_network_connection_unavailable);
        view.setProgressBarVisibility(false);
    }

    @Override
    public void resetSimilarWatchableId() {
        this.similarMediaId = 0;
    }
}