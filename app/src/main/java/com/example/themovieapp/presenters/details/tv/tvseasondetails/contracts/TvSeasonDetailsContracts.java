package com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts;

import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.ui.fragments.base.BaseView;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.TvSeasonEpisodesListFragment;

import java.util.TreeMap;

public interface TvSeasonDetailsContracts extends IBasePresenter {

    interface View extends BaseView<Presenter> {

        void setSeasonAdapter();

        void showError(Integer messageId);
    }

    interface Presenter extends IBasePresenter {

        void cancelRequests();

        void loadingOfFragmentsIsFinished(boolean isFinished);

        TreeMap<Integer, TvSeasonEpisodesListFragment> getSeasons();
    }
}
