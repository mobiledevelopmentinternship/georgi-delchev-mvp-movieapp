package com.example.themovieapp.presenters.home.moviehome;

import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.rated.RatedMoviesResponse;
import com.example.themovieapp.networking.services.movie.MovieHomeService;
import com.example.themovieapp.networking.util.ResponseConverter;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.MoviePersistService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.home.moviehome.contracts.MovieHomeContracts;
import com.example.themovieapp.utils.Category;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

import static com.example.themovieapp.utils.Constants.FIRST_LIST_ELEMENT;
import static com.example.themovieapp.utils.Constants.MOST_POPULAR_RESPONSE_IS_EMPTY;
import static com.example.themovieapp.utils.Constants.NOT_EMPTY_QUERY;
import static com.example.themovieapp.utils.Constants.NO_INTERNET;
import static com.example.themovieapp.utils.Constants.TOP_RATED_RESPONSE_IS_EMPTY;
import static com.example.themovieapp.utils.Constants.UPCOMING_RESPONSE_IS_EMPTY;
import static com.example.themovieapp.utils.Utils.convertToWatchable;

public class MovieHomePresenter extends BasePresenter implements MovieHomeContracts.Presenter {

    private final MovieHomeContracts.View movieHomeView;
    @Inject
    MoviePersistService moviePersistService;
    @Inject
    MovieHomeService movieHomeNetworkService;

    private boolean topRatedAdapterFlag;
    private boolean mostPopularAdapterFlag;
    private boolean upcomingAdapterFlag;
    private Integer selectedMovieId;
    private List<Watchable> topRatedMovies;
    private List<Watchable> mostPopularMovies;
    private List<Watchable> upcomingMovies;

    @Inject
    public MovieHomePresenter(MovieHomeContracts.View view) {
        super();
        this.movieHomeView = view;
        view.setPresenter(this);
        upcomingMovies = new ArrayList<>();
        topRatedMovies = new ArrayList<>();
        mostPopularMovies = new ArrayList<>();
    }

    @Override
    public void start() {
        movieHomeView.setProgressBarVisibility(true);
        if (!isConnected()) {
            movieHomeView.showError(R.string.error_network_connection_lost);
            getMissingCategoryFromDb(NO_INTERNET);
        } else {
            subscribeSingle(
                    Single.zip(
                            movieHomeNetworkService.getTopRatedResponse(),
                            movieHomeNetworkService.getPopularResponse(),
                            movieHomeNetworkService.getNewestResponse(),
                            (topRatedResponse, popularMoviesResponse, newestMoviesResponse) -> {
                                checkResponseFromApi(topRatedResponse,
                                        TOP_RATED_RESPONSE_IS_EMPTY,
                                        Category.TOP_RATED);

                                checkResponseFromApi(popularMoviesResponse,
                                        MOST_POPULAR_RESPONSE_IS_EMPTY,
                                        Category.MOST_POPULAR);

                                checkResponseFromApi(newestMoviesResponse,
                                        UPCOMING_RESPONSE_IS_EMPTY,
                                        Category.UPCOMING);

                                return true;
                            }
                    ),
                    this::onRequestSuccess,
                    this::onRequestError
            );
        }
    }

    private void checkResponseFromApi(RatedMoviesResponse ratedResponse,
                                      String responseIsEmpty,
                                      Category category) {
        if (!ratedResponse.getRatedMovies().isEmpty() &&
                ratedResponse.getRatedMovies().get(FIRST_LIST_ELEMENT).getId() != null) {

            switch (category) {
                case MOST_POPULAR:
                    mostPopularMovies.addAll(convertToWatchable(
                            ResponseConverter.convertRatedMoviesResponseToMoviePersistList(
                                    ratedResponse
                            )));
                    break;
                case TOP_RATED:
                    topRatedMovies.addAll(convertToWatchable(
                            ResponseConverter.convertRatedMoviesResponseToMoviePersistList(
                                    ratedResponse
                            )));
                    break;
                case UPCOMING:
                    upcomingMovies.addAll(convertToWatchable(
                            ResponseConverter.convertRatedMoviesResponseToMoviePersistList(
                                    ratedResponse
                            )));
                    break;
            }
        } else {
            getMissingCategoryFromDb(responseIsEmpty);
        }
    }

    private void onRequestError(Throwable throwable) {
        getMissingCategoryFromDb(throwable.getMessage());
    }

    private void getMissingCategoryFromDb(String missingCategory) {
        switch (missingCategory) {
            case TOP_RATED_RESPONSE_IS_EMPTY:
                getTopRatedFromDb();
                break;
            case MOST_POPULAR_RESPONSE_IS_EMPTY:
                getMostPopularFromDb();
                break;
            case UPCOMING_RESPONSE_IS_EMPTY:
                getUpcomingFromDb();
                break;
            case NO_INTERNET:
                getTopRatedFromDb();
                getMostPopularFromDb();
                getUpcomingFromDb();
                break;
        }
    }

    private void getUpcomingFromDb() {
        subscribeSingle(
                moviePersistService.getUpcomingMovies(),
                this::onUpcomingQuerySuccess,
                this::onUpcomingQueryError
        );
    }

    private void getMostPopularFromDb() {
        subscribeSingle(
                moviePersistService.getPopularMovies(),
                this::onMostPopularMoviesQuerySuccess,
                this::onMostPopularMoviesQueryError
        );
    }

    private void getTopRatedFromDb() {
        subscribeSingle(
                moviePersistService.getTopRatedMovies(),
                this::onTopRatedMoviesQuerySuccess,
                this::onTopRatedMoviesQueryError
        );
    }

    private void onRequestSuccess(Boolean ignored) {
        movieHomeView.setupTopRatedAdapter(Category.TOP_RATED);
        movieHomeView.setupMostPopularAdapter(Category.MOST_POPULAR);
        movieHomeView.setupUpcomingAdapter(Category.UPCOMING);
    }

    private void onTopRatedMoviesQueryError(Throwable throwable) {
        movieHomeView.setupMostPopularAdapter(null);
    }

    private void onTopRatedMoviesQuerySuccess(List<MoviePersist> movies) {
        if (movies.size() > NOT_EMPTY_QUERY && movies.get(FIRST_LIST_ELEMENT).getId() != null) {
            topRatedMovies.addAll(movies);
            movieHomeView.setupTopRatedAdapter(Category.TOP_RATED);
        } else {
            movieHomeView.setupTopRatedAdapter(null);
        }
    }

    private void onMostPopularMoviesQueryError(Throwable throwable) {
        movieHomeView.setupMostPopularAdapter(null);
    }

    private void onMostPopularMoviesQuerySuccess(List<MoviePersist> movies) {
        if (movies.size() > NOT_EMPTY_QUERY && movies.get(FIRST_LIST_ELEMENT).getId() != null) {
            mostPopularMovies.addAll(movies);
            movieHomeView.setupMostPopularAdapter(Category.MOST_POPULAR);
        } else {
            movieHomeView.setupMostPopularAdapter(null);
        }
    }

    private void onUpcomingQueryError(Throwable throwable) {
        movieHomeView.setupUpcomingAdapter(null);
    }

    private void onUpcomingQuerySuccess(List<MoviePersist> movies) {
        if (movies.size() > NOT_EMPTY_QUERY && movies.get(FIRST_LIST_ELEMENT).getId() != null) {
            upcomingMovies.addAll(movies);
            movieHomeView.setupUpcomingAdapter(Category.UPCOMING);
        } else {
            movieHomeView.setupUpcomingAdapter(null);
        }
    }

    @Override
    public void cancelRequests() {
        dispose();
    }

    @Override
    public void setTopRatedAdapterFlag() {
        topRatedAdapterFlag = true;
        checkAdapterFlags();
    }

    @Override
    public void setMostPopularAdapterFlag() {
        mostPopularAdapterFlag = true;
        checkAdapterFlags();
    }

    @Override
    public void setUpcomingAdapterFlag() {
        upcomingAdapterFlag = true;
        checkAdapterFlags();
    }

    @Override
    public void onPosterClicked(Integer id) {
        if (!isConnected()) {
            selectedMovieId = id;
            checkIfMovieIsStoredInPersist(id);
        } else {
            movieHomeView.loadSelectedMovie(id, R.string.movie_media_type);
        }
    }

    @Override
    public List<Watchable> getUpcoming() {
        return upcomingMovies;
    }

    @Override
    public List<Watchable> getTopRated() {
        return topRatedMovies;
    }

    @Override
    public List<Watchable> getMostPopular() {
        return mostPopularMovies;
    }

    private void checkIfMovieIsStoredInPersist(Integer id) {
        subscribeSingle(
                moviePersistService.getMovieById(id),
                this::onMovieExistQuerySuccess,
                this::onMovieExistQueryError
        );
    }

    private void onMovieExistQueryError(Throwable throwable) {
        movieHomeView.showError(R.string.error_network_connection_unavailable);
    }

    private void onMovieExistQuerySuccess(MoviePersist movie) {
        if (movie != null && movie.getId() != null) {
            movieHomeView.loadSelectedMovie(movie.getId(), R.string.movie_media_type);
        } else {
            movieHomeView.loadSelectedMovie(selectedMovieId, R.string.movie_media_type);
        }
    }

    private void checkAdapterFlags() {
        if (topRatedAdapterFlag
                && mostPopularAdapterFlag
                && upcomingAdapterFlag) {
            movieHomeView.setProgressBarVisibility(false);
            resetFlags();
        }
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }

    private void resetFlags() {
        topRatedAdapterFlag = false;
        mostPopularAdapterFlag = false;
        upcomingAdapterFlag = false;
    }
}