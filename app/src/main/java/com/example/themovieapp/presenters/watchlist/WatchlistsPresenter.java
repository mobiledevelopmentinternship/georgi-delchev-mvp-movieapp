package com.example.themovieapp.presenters.watchlist;

import android.text.TextUtils;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.example.themovieapp.adapters.watchlist.models.ExpandableWatchlistModel;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.WatchlistPersistService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.watchlist.contracts.WatchlistsContracts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static com.example.themovieapp.utils.Constants.INDEX_OFFSET;
import static com.example.themovieapp.utils.Utils.convertToWatchableModel;

public class WatchlistsPresenter extends BasePresenter implements WatchlistsContracts.Presenter {

    private final WatchlistsContracts.View view;
    @Inject
    WatchlistPersistService watchlistPersistService;

    private WatchlistPersist defaultWatchlist;
    private List<ExpandableWatchlistModel> watchlistItems;
    private Integer watchlistCounter;

    @Inject
    public WatchlistsPresenter(WatchlistsContracts.View view) {
        super();
        this.view = view;
        view.setPresenter(this);
        watchlistItems = new ArrayList<>();
    }

    @Override
    public void start() {
        view.setProgressBarVisibility(true);
    }

    @Override
    public void getWatchlistsFromDb() {
        subscribeSingle(
                watchlistPersistService.getAllWatchlists(),
                this::onGetAllWatchlistsQuerySuccess,
                this::onGetAllWatchlistsQueryError
        );
    }

    private void onGetAllWatchlistsQueryError(Throwable throwable) {
        createDefaultWatchlist();
    }

    private void onGetAllWatchlistsQuerySuccess(List<WatchlistPersist> watchlists) {
        if (watchlists.isEmpty()) {
            defaultWatchlist = new WatchlistPersist(App.getInstance()
                    .getString(R.string.default_watchlist_name));
            createDefaultWatchlist();
            return;
        }

        watchlistCounter = watchlists.size();
        for (WatchlistPersist watchlist : watchlists) {
            if (TextUtils.equals(watchlist.getName(), "")) {
                continue;
            }

            getMoviesInWatchlist(watchlist.getId(), watchlist.getName());
        }
    }

    private void createDefaultWatchlist() {
        if (defaultWatchlist == null) {
            defaultWatchlist = new WatchlistPersist(App.getInstance()
                    .getString(R.string.default_watchlist_name));
        }

        subscribeCompletable(watchlistPersistService.insertWithCompletable(defaultWatchlist),
                this::getWatchlistsFromDb);
    }

    private void getMoviesInWatchlist(final Long id, String name) {
        subscribeSingle(
                watchlistPersistService.getMoviesByWatchlistId(id),
                result -> onMoviesByWatchlistIdQuerySuccess(result, name, id),
                throwable -> onMoviesByWatchlistIdQueryError(throwable, name, id)
        );
    }

    private void onMoviesByWatchlistIdQueryError(Throwable throwable, String name, Long id) {
        List<Watchable> watchlistWatchables = new ArrayList<>();

        subscribeSingle(
                watchlistPersistService.getTvSeriesByWatchlistId(id),
                tvSeries -> onTvSeriesByWatchlistIdQuerySuccess(tvSeries, name, watchlistWatchables, id),
                error -> onTvSeriesByWatchlistIdQueryError(name, watchlistWatchables, id)
        );
    }

    private void onMoviesByWatchlistIdQuerySuccess(List<MoviePersist> movieLists, String name, Long id) {
        List<Watchable> watchlistWatchables = new ArrayList<>(movieLists);

        subscribeSingle(
                watchlistPersistService.getTvSeriesByWatchlistId(id),
                result -> onTvSeriesByWatchlistIdQuerySuccess(result, name, watchlistWatchables, id),
                throwable -> onTvSeriesByWatchlistIdQueryError(name, watchlistWatchables, id)
        );
    }

    private void onTvSeriesByWatchlistIdQueryError(String name, List<Watchable> watchlistWatchables, Long id) {
        createWatchlistItem(watchlistWatchables, name, id);
    }

    private void onTvSeriesByWatchlistIdQuerySuccess(List<TvSeriesPersist> tvSeries, String name, List<Watchable> watchlistWatchables, Long id) {
        watchlistWatchables.addAll(tvSeries);
        createWatchlistItem(watchlistWatchables, name, id);
    }

    private void createWatchlistItem(List<Watchable> watchlistWatchables, String name, Long id) {
        ExpandableWatchlistModel watchlist = new ExpandableWatchlistModel(
                name,
                new ArrayList<>(convertToWatchableModel(watchlistWatchables)),
                id
        );

        if (watchlistItems.contains(watchlist)) {
            watchlistItems.set(watchlistItems.indexOf(watchlist), watchlist);
        } else {
            watchlistItems.add(watchlist);
        }
        watchlistCounter--;

        if (watchlistCounter == 0) {
            Collections.sort(watchlistItems);
            view.setWatchlistAdapter();
        }
    }

    @Override
    public void loadNewlyCreatedWatchlist(WatchlistPersist watchlistPersist) {
        ExpandableWatchlistModel expandableWatchlistItem = new ExpandableWatchlistModel(
                watchlistPersist.getName(),
                new ArrayList<>(), watchlistPersist.getId());
        watchlistItems.add(expandableWatchlistItem);
        view.addNewlyCreatedWatchlist(watchlistItems.size() - INDEX_OFFSET);
    }

    @Override
    public void onEntrySelected(Integer id, String mediaType) {
        view.showWatchlistEntryDetails(id, mediaType);
    }

    @Override
    public void onEmptyWatchlistSelected(String watchlistName) {
        view.showEmptyWatchlistMessage(watchlistName);
    }

    @Override
    public void cancelTasks() {
        dispose();
    }

    @Override
    public void showWatchlists() {
        view.setProgressBarVisibility(false);
    }

    @Override
    public List<ExpandableWatchlistModel> getWatchlists() {
        return watchlistItems;
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}