package com.example.themovieapp.presenters.details.base.contracts;

import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.ui.fragments.base.BaseView;

import java.util.List;

public interface BaseDetailsContracts {

    interface View extends BaseView<Presenter> {

        void setName(String name);

        void setReleaseDate(String date);

        void setMediaType(Integer mediaTypeId);

        void setPosterImage(String url);

        void setOverview(String overview);

        void setAdditionalInformationTitle(Integer titleId);

        void showSimilarWatchable(Integer id, Integer mediaType);

        void setAllEpisodesButtonVisibility(boolean isVisible);

        void setupScreenCastAdapter();

        void setupSimilarWatchablesAdapter();

        void showError(Integer messageId);

        void hideSimilarMedia();
    }

    interface Presenter extends IBasePresenter {

        void onSimilarMediaClicked(Integer id, Integer mediaTypeId);

        void cancelTasks();

        List<Watchable> getWatchables();

        List<Member> getScreenCast();

        void resetSimilarWatchableId();
    }
}
