package com.example.themovieapp.presenters.details.tv.tvseasondetails;

import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.episode.EpisodesResponse;
import com.example.themovieapp.networking.services.tvseries.seasons.SeasonDetailsService;
import com.example.themovieapp.networking.util.ResponseConverter;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.persistance.models.SeasonPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.EpisodePersistService;
import com.example.themovieapp.persistance.services.SeasonPersistService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts.TvSeasonDetailsContracts;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.TvSeasonEpisodesListFragment;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.base.BaseFragmentFactory;
import com.example.themovieapp.ui.fragments.details.tvseasondetails.factory.creator.TvSeasonFragmentFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import javax.inject.Inject;

import static com.example.themovieapp.utils.Constants.FIRST;

public class TvSeasonsPresenter extends BasePresenter implements TvSeasonDetailsContracts.Presenter {

    private final TvSeasonDetailsContracts.View view;
    @Inject
    SeasonDetailsService seasonDetailsNetworkService;
    @Inject
    SeasonPersistService seasonPersistService;
    @Inject
    EpisodePersistService episodePersistService;

    private final BaseFragmentFactory tvSeasonFactory;
    private Integer tvSeriesId;
    private Integer numberOfSeasons;
    private final TreeMap<Integer, TvSeasonEpisodesListFragment> tvSeasons;
    private Integer seasonCounter;
    private String tvSeriesTitle;

    public TvSeasonsPresenter(TvSeasonDetailsContracts.View view, Integer tvSeriesId, Integer numberOfSeasons, String title) {
        super();
        this.view = view;
        view.setPresenter(this);
        tvSeasons = new TreeMap<>();
        tvSeasonFactory = new TvSeasonFragmentFactory();
        this.tvSeriesId = tvSeriesId;
        this.numberOfSeasons = numberOfSeasons;
        this.tvSeriesTitle = title;
        seasonCounter = numberOfSeasons;
    }

    @Override
    public void start() {
        view.setProgressBarVisibility(true);

        getTvSeriesSeasonsFromPersist();
    }

    private void getTvSeriesSeasonsFromPersist() {
        subscribeSingle(
                seasonPersistService.getTvSeriesSeasons(tvSeriesId),
                this::onSeasonQuerySuccess,
                this::onSeasonQueryError
        );
    }

    private void onSeasonQueryError(Throwable throwable) {
        getEpisodeListsFromApi(numberOfSeasons);
    }

    private void onSeasonQuerySuccess(List<SeasonPersist> seasons) {
        if (seasons.isEmpty()) {
            getEpisodeListsFromApi(numberOfSeasons);
            return;
        }

        for (SeasonPersist season : seasons) {
            subscribeSingle(
                    episodePersistService.getAllEpisodesForASeason(season.getId(), season.getSeasonNumber()),
                    this::onEpisodeListRequestSuccess,
                    this::onEpisodeListRequestError
            );
        }
    }

    private void onEpisodeListRequestError(Throwable throwable) {
        getEpisodeListsFromApi(numberOfSeasons);
    }

    private void onEpisodeListRequestSuccess(List<EpisodePersist> episodes) {
        if (episodes.isEmpty()) {
            getEpisodeListsFromApi(numberOfSeasons);
        } else {
            Collections.sort(episodes);
            EpisodePersist episode = episodes.get(FIRST);
            createSeasonDetailsFragment((ArrayList<? extends Watchable>) episodes, episode);
        }
    }

    private void createSeasonDetailsFragment(ArrayList<? extends Watchable> episodes, EpisodePersist episode) {
        tvSeasons.put(episode.getSeasonNumber(),
                (TvSeasonEpisodesListFragment) tvSeasonFactory.createSeasonDetailsFragment(
                episodes,
                episode.getSeasonId(),
                episode.getSeasonNumber(),
                tvSeriesId,
                tvSeriesTitle,
                numberOfSeasons
        ));

        seasonCounter--;
        if (seasonCounter == 0) {
            view.setSeasonAdapter();
        }
    }

    private void getEpisodeListsFromApi(final Integer numberOfSeasons) {
        for (int seasonNumber = 1; seasonNumber <= numberOfSeasons; seasonNumber++) {
                subscribeSingle(
                        seasonDetailsNetworkService.getEpisodeList(tvSeriesId, seasonNumber),
                        this::onEpisodeListNetworkRequestSuccess,
                        throwable -> view.showError(R.string.error_network_connection_unavailable)
                );
        }
    }

    private void onEpisodeListNetworkRequestSuccess(EpisodesResponse episodesResponse) {
        SeasonPersist season = (new SeasonPersist(
                episodesResponse.getSeasonId(),
                episodesResponse.getSeasonNumber(),
                tvSeriesId));

        if (episodesResponse.getEpisodes().isEmpty()) {
            seasonCounter--;
            return;
        }

        List<EpisodePersist> episodes = ResponseConverter
                .convertEpisodesResponseToEpisodePersist(episodesResponse);
        Collections.sort(episodes);
        episodePersistService.insertAll(episodes);
        seasonPersistService.insert(season);
        createSeasonDetailsFragment((ArrayList<? extends Watchable>) episodes, episodes.get(FIRST));
    }

    @Override
    public void cancelRequests() {
        dispose();
    }

    @Override
    public void loadingOfFragmentsIsFinished(boolean isFinished) {
        if (isFinished) {
            view.setProgressBarVisibility(false);
        }
    }

    @Override
    public TreeMap<Integer, TvSeasonEpisodesListFragment> getSeasons() {
        return tvSeasons;
    }

    public void setTvSeriesId(Integer tvSeriesId) {
        this.tvSeriesId = tvSeriesId;
    }

    public void setNumberOfSeasons(Integer numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}