package com.example.themovieapp.presenters.details.tv.tvseriesdetails.contracts;

import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;

public interface TvSeriesDetailsContracts {

    interface View extends BaseDetailsContracts.View {

        void showRatingDialog(Integer id, String mediaType, String title);

        void showAddToCollectionDialog(Watchable watchable);

        void showEpisodeLists(Integer tvSeriesId, Integer numberOfSeasons, String title);
    }

    interface Presenter extends BaseDetailsContracts.Presenter {

        void loadRating();

        void addToCollection();

        void loadEpisodes();
    }
}
