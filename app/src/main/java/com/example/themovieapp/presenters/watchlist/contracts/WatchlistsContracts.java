package com.example.themovieapp.presenters.watchlist.contracts;

import com.example.themovieapp.adapters.watchlist.models.ExpandableWatchlistModel;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.ui.fragments.base.BaseView;

import java.util.List;

// TODO: 4/2/2019 Replace strings when models are ready
public interface WatchlistsContracts {

    interface View extends BaseView<Presenter> {

        void setWatchlistAdapter();

        void showWatchlistEntryDetails(Integer id, String mediaType);

        void addNewlyCreatedWatchlist(int position);

        void showEmptyWatchlistMessage(String watchlistName);
    }

    interface Presenter extends IBasePresenter {

        void getWatchlistsFromDb();

        void loadNewlyCreatedWatchlist(WatchlistPersist watchlistPersist);

        void onEntrySelected(Integer id, String mediaType);

        void onEmptyWatchlistSelected(String watchlistName);

        void cancelTasks();

        void showWatchlists();

        List<ExpandableWatchlistModel> getWatchlists();
    }
}