package com.example.themovieapp.presenters.base;

public interface IBasePresenter {

    void start();
}
