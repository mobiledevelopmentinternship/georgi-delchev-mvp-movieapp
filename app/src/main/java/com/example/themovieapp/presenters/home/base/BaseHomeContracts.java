package com.example.themovieapp.presenters.home.base;

import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.ui.fragments.base.BaseView;
import com.example.themovieapp.utils.Category;

import java.util.List;

public interface BaseHomeContracts {

    interface View extends BaseView<Presenter> {

        void setupTopRatedAdapter(Category category);

        void setupMostPopularAdapter(Category category);

        void setupUpcomingAdapter(Category category);
    }

    interface Presenter extends IBasePresenter {

        void cancelRequests();

        void setTopRatedAdapterFlag();

        void setMostPopularAdapterFlag();

        void setUpcomingAdapterFlag();

        void onPosterClicked(Integer id);

        List<Watchable> getUpcoming();

        List<Watchable> getTopRated();

        List<Watchable> getMostPopular();
    }
}
