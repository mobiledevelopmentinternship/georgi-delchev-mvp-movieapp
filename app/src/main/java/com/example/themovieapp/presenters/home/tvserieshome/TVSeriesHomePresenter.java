package com.example.themovieapp.presenters.home.tvserieshome;

import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.rated.RatedTvSeriesResponse;
import com.example.themovieapp.networking.services.tvseries.TvSeriesHomeService;
import com.example.themovieapp.networking.util.ResponseConverter;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.TvSeriesPersistService;
import com.example.themovieapp.presenters.base.BasePresenter;
import com.example.themovieapp.presenters.home.tvserieshome.contracts.TVSeriesHomeContracts;
import com.example.themovieapp.utils.Category;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

import static com.example.themovieapp.utils.Constants.FIRST_LIST_ELEMENT;
import static com.example.themovieapp.utils.Constants.MOST_POPULAR_RESPONSE_IS_EMPTY;
import static com.example.themovieapp.utils.Constants.NOT_EMPTY_QUERY;
import static com.example.themovieapp.utils.Constants.NO_INTERNET;
import static com.example.themovieapp.utils.Constants.TOP_RATED_RESPONSE_IS_EMPTY;
import static com.example.themovieapp.utils.Constants.UPCOMING_RESPONSE_IS_EMPTY;
import static com.example.themovieapp.utils.Utils.convertToWatchable;

public class TVSeriesHomePresenter extends BasePresenter implements TVSeriesHomeContracts.Presenter {

    private final TVSeriesHomeContracts.View tvSeriesView;
    @Inject
    TvSeriesHomeService tvSeriesHomeNetworkService;
    @Inject
    TvSeriesPersistService tvSeriesPersistService;

    private boolean topRatedAdapterFlag;
    private boolean mostPopularAdapterFlag;
    private boolean upcomingAdapterFlag;
    private Integer selectedTvSeries;
    private List<Watchable> airingTodayTvSeries;
    private List<Watchable> mostPopularTvSeries;
    private List<Watchable> topRatedTvSeries;

    @Inject
    public TVSeriesHomePresenter(TVSeriesHomeContracts.View view) {
        super();
        this.tvSeriesView = view;
        view.setPresenter(this);
        airingTodayTvSeries = new ArrayList<>();
        mostPopularTvSeries = new ArrayList<>();
        topRatedTvSeries = new ArrayList<>();
    }

    @Override
    public void start() {
        tvSeriesView.setProgressBarVisibility(true);
        if (!isConnected()) {
            getMissingCategoryFromDb(NO_INTERNET);
        } else {
            subscribeSingle(
                    Single.zip(
                            tvSeriesHomeNetworkService.getNewestResponse(),
                            tvSeriesHomeNetworkService.getPopularResponse(),
                            tvSeriesHomeNetworkService.getTopRatedResponse(),
                            (airingTodayTvSeriesResponse, popularTvSeriesResponse, topRatedTvSeriesResponse) -> {
                                TVSeriesHomePresenter.this.checkResponseFromApi(topRatedTvSeriesResponse,
                                        TOP_RATED_RESPONSE_IS_EMPTY, Category.TOP_RATED);

                                TVSeriesHomePresenter.this.checkResponseFromApi(popularTvSeriesResponse,
                                        MOST_POPULAR_RESPONSE_IS_EMPTY, Category.MOST_POPULAR);

                                TVSeriesHomePresenter.this.checkResponseFromApi(airingTodayTvSeriesResponse,
                                        UPCOMING_RESPONSE_IS_EMPTY, Category.UPCOMING);

                                return true;
                            }
                    ),
                    this::onRequestSuccess,
                    this::onRequestError
            );
        }
    }

    private void checkResponseFromApi(RatedTvSeriesResponse ratedTvSeriesResponse,
                                      String responseIsEmpty,
                                      Category category) throws Exception {
        if (!ratedTvSeriesResponse.getRatedTvSeries().isEmpty() &&
                ratedTvSeriesResponse.getRatedTvSeries().get(FIRST_LIST_ELEMENT).getId() != null) {
            switch (category) {
                case UPCOMING:
                    airingTodayTvSeries.addAll(convertToWatchable(ResponseConverter
                            .converRatedTvSeriesToTvSeriesPersistList(ratedTvSeriesResponse)));
                    break;
                case TOP_RATED:
                    topRatedTvSeries.addAll(convertToWatchable(ResponseConverter
                            .converRatedTvSeriesToTvSeriesPersistList(ratedTvSeriesResponse)));
                    break;
                case MOST_POPULAR:
                    mostPopularTvSeries.addAll(convertToWatchable(ResponseConverter
                            .converRatedTvSeriesToTvSeriesPersistList(ratedTvSeriesResponse)));
                    break;
            }
        } else {
            getMissingCategoryFromDb(responseIsEmpty);
        }
    }

    private void onRequestError(Throwable throwable) {
        getMissingCategoryFromDb(throwable.getMessage());
    }

    /**
     * Information about Upcoming Tv Series should always be downloaded from API
     *
     * @param missingCategory -> category not found in DB
     */
    private void getMissingCategoryFromDb(String missingCategory) {
        switch (missingCategory) {
            case TOP_RATED_RESPONSE_IS_EMPTY:
                getTopRatedFromDb();
                break;
            case MOST_POPULAR_RESPONSE_IS_EMPTY:
                getMostPopularFromDb();
                break;
            case NO_INTERNET:
                getTopRatedFromDb();
                getMostPopularFromDb();
                tvSeriesView.setupUpcomingAdapter(Category.UPCOMING);
                break;
        }
    }

    private void getTopRatedFromDb() {
        subscribeSingle(
                tvSeriesPersistService.getTopRatedTvSeries(),
                this::onTopRatedQuerySuccess,
                this::onTopRatedQueryError
        );
    }

    private void getMostPopularFromDb() {
        subscribeSingle(
                tvSeriesPersistService.getMostPopularTvSeries(),
                this::onPopularQuerySuccess,
                this::onPopularQueryError
        );
    }

    private void onRequestSuccess(Boolean ignored) {
        tvSeriesView.setupTopRatedAdapter(Category.TOP_RATED);
        tvSeriesView.setupMostPopularAdapter(Category.MOST_POPULAR);
        tvSeriesView.setupUpcomingAdapter(Category.UPCOMING);
    }

    private void onPopularQuerySuccess(List<TvSeriesPersist> result) {
        if (result.size() > NOT_EMPTY_QUERY && result.get(FIRST_LIST_ELEMENT).getId() != null) {
            mostPopularTvSeries.addAll(result);
            tvSeriesView.setupMostPopularAdapter(Category.MOST_POPULAR);
        } else {
            tvSeriesView.setupMostPopularAdapter(null);
        }
    }

    private void onPopularQueryError(Throwable throwable) {
        tvSeriesView.setupMostPopularAdapter(null);
    }

    private void onTopRatedQueryError(Throwable throwable) {
        tvSeriesView.setupTopRatedAdapter(null);
    }

    private void onTopRatedQuerySuccess(List<TvSeriesPersist> result) {
        if (result.size() > NOT_EMPTY_QUERY && result.get(FIRST_LIST_ELEMENT).getId() != null) {
            topRatedTvSeries.addAll(result);
            tvSeriesView.setupTopRatedAdapter(Category.TOP_RATED);
        } else {
            tvSeriesView.setupTopRatedAdapter(null);
        }
    }

    @Override
    public void cancelRequests() {
        dispose();
    }

    @Override
    public void setTopRatedAdapterFlag() {
        topRatedAdapterFlag = true;
        checkAdapterFlags();
    }

    @Override
    public void setMostPopularAdapterFlag() {
        mostPopularAdapterFlag = true;
        checkAdapterFlags();
    }

    @Override
    public void setUpcomingAdapterFlag() {
        upcomingAdapterFlag = true;
        checkAdapterFlags();
    }

    @Override
    public void onPosterClicked(Integer id) {
        if (!isConnected()) {
            selectedTvSeries = id;
            checkIfTvSeriesExistInPersist(id);
        } else {
            tvSeriesView.loadSelectedTvSeries(id, R.string.tv_series_media_type);
        }
    }

    @Override
    public List<Watchable> getUpcoming() {
        return airingTodayTvSeries;
    }

    @Override
    public List<Watchable> getTopRated() {
        return topRatedTvSeries;
    }

    @Override
    public List<Watchable> getMostPopular() {
        return mostPopularTvSeries;
    }

    private void checkIfTvSeriesExistInPersist(Integer id) {
        subscribeSingle(
                tvSeriesPersistService.getTvSeriesById(id),
                this::onTvSeriesExistSuccess,
                this::onTvSeriesExistError
        );
    }

    private void onTvSeriesExistError(Throwable throwable) {
        tvSeriesView.showError(R.string.error_network_connection_unavailable);
    }

    private void onTvSeriesExistSuccess(TvSeriesPersist tvSeries) {
        if (tvSeries.getId() == null) {
            tvSeriesView.loadSelectedTvSeries(tvSeries.getId(), R.string.tv_series_media_type);
        } else {
            tvSeriesView.loadSelectedTvSeries(selectedTvSeries, R.string.tv_series_media_type);
        }
    }

    private void checkAdapterFlags() {
        if (topRatedAdapterFlag && mostPopularAdapterFlag && upcomingAdapterFlag) {
            tvSeriesView.setProgressBarVisibility(false);
            resetFlags();
        } else if (!isConnected() && topRatedAdapterFlag && mostPopularAdapterFlag) {
            tvSeriesView.setProgressBarVisibility(false);
            resetFlags();
        }
    }

    private void resetFlags() {
        mostPopularAdapterFlag = false;
        topRatedAdapterFlag = false;
        upcomingAdapterFlag = false;
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}