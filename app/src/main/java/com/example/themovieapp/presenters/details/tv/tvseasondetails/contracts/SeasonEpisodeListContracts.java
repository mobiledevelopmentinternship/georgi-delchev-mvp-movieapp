package com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts;

import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.presenters.base.IBasePresenter;

import java.util.List;

public interface SeasonEpisodeListContracts {

    interface View {

        void setPresenter(SeasonEpisodeListContracts.Presenter presenter);

        void loadEpisodeList(List<EpisodePersist> episodes);

        void showEpisodeDetails(Integer seasonId, Integer seasonNumber, Integer episodeNumber, Integer tvSeriesId, Integer allSeasonsCount, String tvSeriesTitle);

    }

    interface Presenter extends IBasePresenter {

        void onSelected(Integer position);

        List<EpisodePersist> getEpisodes();
    }
}
