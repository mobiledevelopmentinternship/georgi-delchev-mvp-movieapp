package com.example.themovieapp.presenters.discover.contracts;

import com.example.themovieapp.networking.retrofit.response.search.SearchResponse;
import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.ui.fragments.base.BaseView;

import java.util.List;

// TODO: 4/2/2019 Replace strings with models when ready
public interface DiscoverContracts {

    interface View extends BaseView<Presenter> {

        void setResultListVisibility(boolean isVisible);

        void loadMovieDetails(Integer movieId);

        void loadTvSeriesDetails(Integer tvSeriesId);

        void showError(Integer messageId);

        void addResults(List<SearchResponse> result);

        void setMaxPagesForQuery(Integer maxPages);

        void incrementCurrentPageNumber();

        void resetPagesForNewQuery();

        void setLoading(boolean isLoading);

        void setResultsAdapter(List<SearchResponse> result);
    }

    interface Presenter extends IBasePresenter {

        void onSearchResultClick(SearchResponse response);

        void discover(String searchPhrase);

        void showResults();

        void loadMoreResults();

        void setLoadingCompleted();

        void onStopButtonClick(boolean isStopped);

        void onQueryCleared();
    }
}
