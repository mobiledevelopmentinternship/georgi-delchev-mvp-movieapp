package com.example.themovieapp.presenters.details.moviedetails.contracts;

import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;

public interface MovieDetailsContracts extends BaseDetailsContracts  {

    interface View extends BaseDetailsContracts.View {

        void loadRatingDialog(Integer id, String mediaType, String title);

        void loadAddToCollectionDialog(Watchable watchable);
    }

    interface Presenter extends BaseDetailsContracts.Presenter {

        void loadRating();

        void addToCollection();
    }
}
