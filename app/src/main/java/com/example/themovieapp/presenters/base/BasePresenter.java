package com.example.themovieapp.presenters.base;

import com.example.themovieapp.App;
import com.example.themovieapp.dagger.components.AppComponent;
import com.example.themovieapp.utils.NetworkUtils;
import com.example.themovieapp.utils.RxUtils;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.operators.completable.CompletableFromAction;

import static com.example.themovieapp.utils.RxUtils.applyCompletableScheduler;

public abstract class BasePresenter {

    @Inject
    NetworkUtils networkUtils;

    private final AppComponent appComponent;
    private CompositeDisposable compositeDisposable;

    protected BasePresenter() {
        this.appComponent = App.getInstance().getAppComponent();
        inject();
    }

    protected final void dispose() {
        getCompositeDisposable().dispose();
    }

    private CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }

        return compositeDisposable;
    }

    protected final <T> void subscribeSingle(Single<T> observable,
                                             Consumer<T> onSuccess,
                                             Consumer<Throwable> onError) {
        Disposable disposable = observable
                .compose(RxUtils.applySingleSchedulers())
                .subscribe(onSuccess, onError);

        getCompositeDisposable().add(disposable);
    }

    protected void subscribeCompletable(Completable observable, Action onComplete) {
        Disposable disposable = observable
                .compose(applyCompletableScheduler())
                .subscribe(onComplete);

        getCompositeDisposable().add(disposable);
    }

    protected void subscribeCompletable(Completable observable,
                                        Action onComplete,
                                        Consumer<Throwable> onError) {
        Disposable disposable = observable
                .compose(applyCompletableScheduler())
                .subscribe(onComplete, onError);

        getCompositeDisposable().add(disposable);
    }

    protected void subscribeCompletable(Completable observable) {
        Disposable disposable = observable
                .compose(applyCompletableScheduler())
                .subscribe();

        getCompositeDisposable().add(disposable);
    }

    protected Completable chainCompletables(Completable first, Completable second) {
        return first.andThen(second);
    }

    protected Completable createCompletable(Action action) {
        return new CompletableFromAction(action);
    }

    protected abstract void inject();

    protected final AppComponent getAppComponent() {
        return appComponent;
    }

    protected boolean isConnected() {
        return networkUtils.getNetworkStatus();
    }
}