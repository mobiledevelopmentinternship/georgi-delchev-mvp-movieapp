package com.example.themovieapp.presenters.details.tv.tvseriesdetails;

import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.tv.TvSeriesResponse;
import com.example.themovieapp.networking.services.common.DetailsService;
import com.example.themovieapp.networking.util.ResponseConverter;
import com.example.themovieapp.persistance.models.CrewMemberPersist;
import com.example.themovieapp.persistance.models.ScreenCastMemberPersist;
import com.example.themovieapp.persistance.models.SeasonPersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.persistance.services.SeasonPersistService;
import com.example.themovieapp.persistance.services.TvSeriesPersistService;
import com.example.themovieapp.presenters.details.base.BaseDetailsPresenter;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.presenters.details.tv.tvseriesdetails.contracts.TvSeriesDetailsContracts;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.example.themovieapp.utils.Constants.BIG_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.convertDateToString;
import static com.example.themovieapp.utils.Utils.convertFromWatchableToTvSeries;

public class TvSeriesDetailsPresenter extends BaseDetailsPresenter implements TvSeriesDetailsContracts.Presenter {

    private final TvSeriesDetailsContracts.View view;
    @Inject
    TvSeriesPersistService tvSeriesPersistService;
    @Inject
    DetailsService detailsNetworkService;
    @Inject
    SeasonPersistService seasonPersistService;

    private final Integer id;
    private final String mediaType;
    private List<Member> allMembers;

    public TvSeriesDetailsPresenter(BaseDetailsContracts.View view, Integer id, String mediaType) {
        super(view, id, mediaType);
        this.view = (TvSeriesDetailsContracts.View) view;
        view.setPresenter(this);
        this.id = id;
        this.mediaType = mediaType;
        allMembers = new ArrayList<>();
    }

    @Override
    protected void getDetailsFromDb() {
        subscribeSingle(
                tvSeriesPersistService.getTvSeriesById(id),
                this::onQuerySuccess,
                this::onQueryError
        );
    }

    @Override
    protected void persistWatchable(Watchable watchable) {
        if (watchable instanceof TvSeriesPersist) {
            tvSeriesPersistService.insert(convertFromWatchableToTvSeries(watchable));
        }
    }

    private void onQueryError(Throwable throwable) {
        getDetailsFromApi();
    }

    private void onQuerySuccess(TvSeriesPersist result) {
        watchable = result;
        title = result.getTitle();
        view.setAdditionalInformationTitle(R.string.details_similar_tv_series_title);
        view.setMediaType(R.string.tv_series_media_type);
        view.setOverview(result.getOverview());
        view.setName(title);
        view.setReleaseDate(convertDateToString(result.getAiringDate()));
        view.setPosterImage(buildFullImageUrl(result.getPosterUrl(), BIG_IMAGE_WIDTH));
        getTvSeriesCrewMemberFromDb();
        getSimilarMedia();
    }

    private void getTvSeriesScreenCastMembersFromDb() {
        subscribeSingle(
                tvSeriesPersistService.getTvSeriesScreenCastMembers(convertFromWatchableToTvSeries(watchable).getTitle()),
                this::onTvSeriesScreenCastMembersSuccess,
                this::onTvSeriesScreenCastMembersError
        );
    }

    private void onTvSeriesScreenCastMembersError(Throwable throwable) {
        getScreenCastFromApi(mediaType);
    }

    private void onTvSeriesScreenCastMembersSuccess(List<ScreenCastMemberPersist> screenCastMembers) {
        if (!screenCastMembers.isEmpty()) {
            this.screenCast.addAll(screenCastMembers);
            screenCastCounter--;
            checkCastCounter();
        } else {
            getScreenCastFromApi(mediaType);
        }
    }

    private void getTvSeriesCrewMemberFromDb() {
        subscribeSingle(
                tvSeriesPersistService.getTvSeriesCrewMembers(convertFromWatchableToTvSeries(watchable).getTitle()),
                this::onTvSeriesCrewMembersQuerySuccess,
                this::onTvSeriesCrewMembersQueryError
        );
    }

    private void onTvSeriesCrewMembersQueryError(Throwable throwable) {
        getScreenCastFromApi(mediaType);
    }

    private void onTvSeriesCrewMembersQuerySuccess(List<CrewMemberPersist> crewMembers) {
        if (crewMembers.isEmpty()) {
            getScreenCastFromApi(mediaType);
        } else {
            this.crewMembers.addAll(crewMembers);
            screenCastCounter--;
            checkCastCounter();
            getTvSeriesScreenCastMembersFromDb();
        }
    }

    private void checkCastCounter() {
        if (screenCastCounter == 0) {
            view.setupScreenCastAdapter();
        }
    }

    @Override
    public void loadRating() {
        if (!isConnected()) {
            view.showError(R.string.error_network_connection_unavailable);
        } else {
            view.showRatingDialog(id, mediaType, convertFromWatchableToTvSeries(watchable).getTitle());
        }
    }

    @Override
    public void addToCollection() {
        view.showAddToCollectionDialog(watchable);
    }

    @Override
    public void loadEpisodes() {
        if (!isConnected()) {
            subscribeSingle(
                    seasonPersistService.getTvSeriesSeasons(id),
                    this::onSeasonsQuerySuccess,
                    this::onSeasonsQueryError
            );
        } else {
            getFullTvSeriesDetails((TvSeriesPersist) watchable);
        }
    }

    private void onSeasonsQueryError(Throwable throwable) {
        view.showError(R.string.error_network_connection_unavailable);
    }

    private void onSeasonsQuerySuccess(List<SeasonPersist> result) {
        if (result.isEmpty()) {
            view.showError(R.string.error_network_connection_unavailable);
        } else {
            TvSeriesPersist tvSeries = (TvSeriesPersist) watchable;
            view.showEpisodeLists(tvSeries.getId(), tvSeries.getSeasonCount(), tvSeries.getTitle());
        }
    }

    private void getFullTvSeriesDetails(TvSeriesPersist fromDb) {
        subscribeSingle(
                detailsNetworkService.getTvSeriesDetailsResponse(fromDb.getId()),
                this::onFullTvSeriesDetailsSuccess,
                this::onNetworkRequestError
        );
    }

    private void onFullTvSeriesDetailsSuccess(TvSeriesResponse response) {
        try {
            tvSeriesPersistService.insert(ResponseConverter.convertTvResponseToPersist(response));
        } catch (Throwable ignored) {
            view.showError(R.string.error_details_cant_load_episodes);
        }

        view.showEpisodeLists(response.getId(), response.getNumberOfSeasons(), response.getName());
    }

    private void onNetworkRequestError(Throwable throwable) {
        view.showError(R.string.error_network_connection_unavailable);
    }

    @Override
    public List<Watchable> getWatchables() {
        return similarWatchables;
    }

    @Override
    public List<Member> getScreenCast() {
        allMembers.addAll(screenCast);
        allMembers.addAll(crewMembers);
        return allMembers;
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}