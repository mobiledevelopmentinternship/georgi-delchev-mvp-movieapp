package com.example.themovieapp.view;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import com.example.themovieapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProgressLayout extends ConstraintLayout {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private Unbinder unbinder;

    public ProgressLayout(Context context) {
        super(context);

        init(context);
    }

    public ProgressLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public ProgressLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    public void setLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void init(Context context) {
        View view = inflate(context, R.layout.layout_progress, this);
        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
    }
}
