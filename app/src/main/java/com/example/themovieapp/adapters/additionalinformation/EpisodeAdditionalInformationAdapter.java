package com.example.themovieapp.adapters.additionalinformation;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.contracts.TvSeriesEpisodeDetailsContracts;
import com.example.themovieapp.utils.PicassoImageLoader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.API_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.createEpisodeName;

public class EpisodeAdditionalInformationAdapter
        extends RecyclerView.Adapter<EpisodeAdditionalInformationAdapter.EpisodeAdditionalInformationViewHolder> {

    private final List<EpisodePersist> otherEpisodes;
    private final TvSeriesEpisodeDetailsContracts.Presenter presenter;

    public EpisodeAdditionalInformationAdapter(TvSeriesEpisodeDetailsContracts.Presenter presenter) {
        this.presenter = presenter;
        this.otherEpisodes = presenter.getEpisodesInSeason();
    }

    @NonNull
    @Override
    public EpisodeAdditionalInformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new EpisodeAdditionalInformationViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_card_view_details, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodeAdditionalInformationViewHolder additionalInformationViewHolder, int position) {
        EpisodePersist episode = otherEpisodes.get(position);
        additionalInformationViewHolder.setImgPoster(episode.getPosterUrl());
        additionalInformationViewHolder.setTxtEpisodeNumber(createEpisodeName(episode.getEpisodeNumber()));
        additionalInformationViewHolder.setTxtName(episode.getName());
    }

    @Override
    public int getItemCount() {
        return otherEpisodes.size();
    }

    class EpisodeAdditionalInformationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_additional_information_poster)
        ImageView imgPoster;
        @BindView(R.id.txt_additional_information_name)
        TextView txtName;
        @BindView(R.id.txt_additional_information_episode)
        TextView txtEpisodeNumber;

        EpisodeAdditionalInformationViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setImgPoster(String url) {
            PicassoImageLoader.setImage(imgPoster, buildFullImageUrl(url, API_IMAGE_WIDTH));
        }

        void setTxtName(String name) {
            txtName.setText(name);
        }

        void setTxtEpisodeNumber(String episodeNumber) {
            txtEpisodeNumber.setText(episodeNumber);
        }

        @OnClick(R.id.item_details)
        void onEpisodeClick() {
            int position = getAdapterPosition();
            EpisodePersist episode = otherEpisodes.get(position);
            presenter.onEpisodeClicked(episode.getEpisodeNumber());
        }
    }
}