package com.example.themovieapp.adapters.watchlist.models;

public class WatchlistEntryModel {

    private Long watchableId;
    private String posterUrl;
    private String briefOverview;
    private String mediaType;
    private String name;
    private String releaseDate;

    public Long getWatchableId() {
        return watchableId;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getBriefOverview() {
        return briefOverview;
    }

    public String getName() {
        return name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public static class Builder {

        private Long watchableId;
        private String posterUrl;
        private String briefOverview;
        private String mediaType;
        private String name;
        private String releaseDate;

        public Builder withWatchableId(Long watchableId) {
            this.watchableId = watchableId;
            return this;
        }

        public Builder withPosterUrl(String posterUrl) {
            this.posterUrl = posterUrl;
            return this;
        }

        public Builder withBriefOverview(String briefOverview) {
            this.briefOverview = briefOverview;
            return this;
        }

        public Builder withMediaType(String mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withReleaseDate(String releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public WatchlistEntryModel build() {
            WatchlistEntryModel model = new WatchlistEntryModel();
            model.briefOverview = this.briefOverview;
            model.mediaType = this.mediaType;
            model.name = this.name;
            model.posterUrl = this.posterUrl;
            model.watchableId = this.watchableId;

            return model;
        }
    }
}
