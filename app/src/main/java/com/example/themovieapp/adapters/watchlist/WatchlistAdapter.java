package com.example.themovieapp.adapters.watchlist;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.WatchlistPersist;
import com.example.themovieapp.ui.dialogs.addtowatchlist.presenter.contracts.AddToWatchlistContracts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WatchlistAdapter extends
        RecyclerView.Adapter<WatchlistAdapter.WatchlistViewHolder> {

    private final List<WatchlistPersist> watchlists;
    private final AddToWatchlistContracts.Presenter presenter;

    public WatchlistAdapter(List<WatchlistPersist> watchlists,
                            AddToWatchlistContracts.Presenter presenter) {
        this.watchlists = watchlists;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public WatchlistViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_watchlist_movie, viewGroup, false);

        return new WatchlistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchlistViewHolder watchlistViewHolder, int position) {
        watchlistViewHolder.setWatchlistName(watchlists.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return watchlists.size();
    }

    public void addWatchlist(WatchlistPersist watchlistPersist) {
        watchlists.add(watchlistPersist);
        notifyDataSetChanged();
    }

    class WatchlistViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_watchlist_title)
        TextView txtWatchlistName;

        WatchlistViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setWatchlistName(String name) {
            txtWatchlistName.setText(name);
        }

        @OnClick(R.id.txt_watchlist_title)
        void onWatchlistClick() {
            txtWatchlistName.setBackgroundColor(Color.YELLOW);
            presenter.onWatchlistClicked(getAdapterPosition());
        }
    }
}