package com.example.themovieapp.adapters.search;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.networking.retrofit.response.search.SearchResponse;
import com.example.themovieapp.presenters.discover.contracts.DiscoverContracts;
import com.example.themovieapp.utils.PicassoImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.HUGE_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Constants.MOVIE_MEDIA_TYPE;
import static com.example.themovieapp.utils.Constants.TV_SERIES_MEDIA_TYPE;
import static com.example.themovieapp.utils.Constants.TV_SERIES_TAB_TITLE;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.buildTitle;

public class DiscoverResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SearchResponse> discoveredWatchables;
    private DiscoverContracts.Presenter presenter;
    private boolean hasReachedTheEnd = false;

    public DiscoverResultAdapter(List<SearchResponse> result, DiscoverContracts.Presenter presenter) {
        discoveredWatchables = new ArrayList<>();
        this.discoveredWatchables = result;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new DiscoverResultViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_discovery_result_card, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder discoverResultViewHolder, int position) {
        SearchResponse searchResponse = discoveredWatchables.get(position);
        setupItemView(discoverResultViewHolder, searchResponse, position);
        if (position == getCurrentMaxPosition()) {
            setHasReachedTheEnd(true);
        }
    }

    private int getCurrentMaxPosition() {
        return getItemCount() - 1;
    }

    @Override
    public int getItemCount() {
        return discoveredWatchables.size();
    }

    public void updateResults(List<SearchResponse> newWatchables) {
        discoveredWatchables.addAll(newWatchables);
        setHasReachedTheEnd(false);
        presenter.setLoadingCompleted();
    }

    private void setupItemView(@NonNull RecyclerView.ViewHolder viewHolder, SearchResponse searchResponse, int position) {
        if (viewHolder instanceof DiscoverResultViewHolder) {
            DiscoverResultViewHolder discoverResultViewHolder = (DiscoverResultViewHolder) viewHolder;
            discoverResultViewHolder.setPoster(searchResponse.getPosterUrl());
            if (TextUtils.equals(searchResponse.getMediaType(), TV_SERIES_MEDIA_TYPE)) {
                discoverResultViewHolder.setTxtTitle(searchResponse.getName(), TV_SERIES_TAB_TITLE);
            } else {
                discoverResultViewHolder.setTxtTitle(searchResponse.getTitle(), MOVIE_MEDIA_TYPE);
            }

            viewHolder.itemView.setOnClickListener(view ->
                    presenter.onSearchResultClick(discoveredWatchables.get(position)));
            discoverResultViewHolder.setTxtOverview(searchResponse.getOverview());
        }
    }

    public boolean hasReachedTheEndOfCurrentList() {
        return hasReachedTheEnd;
    }

    private void setHasReachedTheEnd(boolean hasReachedTheEnd) {
        this.hasReachedTheEnd = hasReachedTheEnd;
    }

    static class DiscoverResultViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_result_poster)
        ImageView imgPoster;
        @BindView(R.id.txt_discover_result_title)
        TextView txtName;
        @BindView(R.id.txt_discover_result_overview)
        TextView txtOverview;

        DiscoverResultViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setPoster(String posterUrl) {
            PicassoImageLoader.setImage(imgPoster, buildFullImageUrl(posterUrl, HUGE_IMAGE_WIDTH));
        }

        void setTxtTitle(String name, String mediaType) {
            this.txtName.setText(buildTitle(name, mediaType));
        }

        void setTxtOverview(String overview) {
            this.txtOverview.setText(overview);
        }
    }
}
