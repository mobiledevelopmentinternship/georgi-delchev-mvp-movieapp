package com.example.themovieapp.adapters.similarmedia;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.utils.PicassoImageLoader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static com.example.themovieapp.utils.Constants.BIG_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;

public class MediaRecyclerAdapter extends RecyclerView.Adapter<MediaRecyclerAdapter.SimilarMediaViewHolder> {

    private final BaseDetailsContracts.Presenter presenter;
    private final List<Watchable> watchables;

    public MediaRecyclerAdapter(BaseDetailsContracts.Presenter presenter) {
        this.presenter = presenter;
        this.watchables = presenter.getWatchables();
    }

    @NonNull
    @Override
    public SimilarMediaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new SimilarMediaViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_card_view_details, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SimilarMediaViewHolder similarMediaViewHolder, int position) {
        similarMediaViewHolder.setVisibility();
        similarMediaViewHolder.setName(watchables.get(position).getTitle());
        similarMediaViewHolder.setPoster(watchables.get(position).getPosterUrl());
    }

    @Override
    public int getItemCount() {
        return watchables.size();
    }

    class SimilarMediaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_additional_information_poster)
        ImageView imgSimilarPoster;
        @BindView(R.id.txt_additional_information_name)
        TextView txtSimilarName;
        @BindView(R.id.txt_additional_information_episode)
        TextView txtNotUsed;

        SimilarMediaViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setPoster(String posterPath) {
            PicassoImageLoader.setImage(imgSimilarPoster, buildFullImageUrl(posterPath, BIG_IMAGE_WIDTH));
        }

        void setName(String name) {
            txtSimilarName.setText(name);
        }

        void setVisibility() {
            txtNotUsed.setVisibility(GONE);
        }

        @OnClick(R.id.item_details)
        void onSimilarMediaClick() {
            int position = getAdapterPosition();
            if (presenter.getWatchables().get(position) instanceof MoviePersist) {
                MoviePersist moviePersist = (MoviePersist) watchables.get(position);
                presenter.onSimilarMediaClicked(moviePersist.getId(), R.string.movie_media_type);
            } else {
                TvSeriesPersist tvSeriesPersist = (TvSeriesPersist) watchables.get(position);
                presenter.onSimilarMediaClicked(tvSeriesPersist.getId(), R.string.tv_series_media_type);
            }
        }
    }
}