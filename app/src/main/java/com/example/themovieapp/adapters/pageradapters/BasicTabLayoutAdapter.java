package com.example.themovieapp.adapters.pageradapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts.TvSeasonDetailsContracts;

import java.util.ArrayList;
import java.util.List;

import static com.example.themovieapp.utils.Utils.transformSeasonTitles;

public class BasicTabLayoutAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> fragmentList;
    private List<String> fragmentTitles;

    public BasicTabLayoutAdapter(FragmentManager fragmentManager,
                                 TvSeasonDetailsContracts.Presenter presenter) {
        super(fragmentManager);
        fragmentList = new ArrayList<>(presenter.getSeasons().values());
        fragmentTitles = new ArrayList<>(transformSeasonTitles(presenter.getSeasons().keySet()));
    }

    public BasicTabLayoutAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        fragmentList = new ArrayList<>();
        fragmentTitles = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitles.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitles.get(position);
    }
}
