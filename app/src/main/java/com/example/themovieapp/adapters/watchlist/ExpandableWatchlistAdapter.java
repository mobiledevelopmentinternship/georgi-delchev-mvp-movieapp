package com.example.themovieapp.adapters.watchlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.App;
import com.example.themovieapp.R;
import com.example.themovieapp.adapters.watchlist.models.ExpandableWatchlistModel;
import com.example.themovieapp.adapters.watchlist.models.WatchlistEntryModel;
import com.example.themovieapp.presenters.watchlist.contracts.WatchlistsContracts;
import com.example.themovieapp.utils.PicassoImageLoader;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.API_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;
import static com.example.themovieapp.utils.Utils.buildTitle;

public class ExpandableWatchlistAdapter extends AbstractExpandableItemAdapter<ExpandableWatchlistAdapter.ExpandableWatchlistGroupViewHolder,
        ExpandableWatchlistAdapter.ExpandableWatchlistChildViewHolder> {

    private static WatchlistsContracts.Presenter presenter;
    private static List<ExpandableWatchlistModel> watchlists;

    public ExpandableWatchlistAdapter(WatchlistsContracts.Presenter presenter) {
        setHasStableIds(true);
        ExpandableWatchlistAdapter.presenter = presenter;
        watchlists = presenter.getWatchlists();
    }

    @Override
    public int getGroupCount() {
        return watchlists.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return watchlists.get(groupPosition).getEntries().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return watchlists.get(groupPosition).getEntries().get(childPosition).getWatchableId();
    }

    @Override
    public ExpandableWatchlistGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        return new ExpandableWatchlistGroupViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_watchlist, parent, false)
        );
    }

    @Override
    public ExpandableWatchlistChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        return new ExpandableWatchlistChildViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_watchlist_entry, parent, false)
        );
    }

    @Override
    public void onBindGroupViewHolder(ExpandableWatchlistGroupViewHolder holder, int groupPosition, int viewType) {
        holder.setTxtWatchlistName(
                watchlists.get(groupPosition).getWatchlistTitle()
        );
        holder.setGroupPosition(groupPosition);
    }

    @Override
    public void onBindChildViewHolder(ExpandableWatchlistChildViewHolder holder,
                                      int groupPosition,
                                      int childPosition,
                                      int viewType) {
        WatchlistEntryModel child = watchlists.get(groupPosition).getEntries().get(childPosition);
        holder.setChildPosition(childPosition);
        holder.setParentPosition(groupPosition);
        holder.setImgPoster(child.getPosterUrl());
        holder.setTxtWatchableBriefOverview(child.getBriefOverview());
        holder.setTxtWatchableName(child.getName(), child.getMediaType());
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(ExpandableWatchlistGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

    private static void invertArrowIcon(ExpandableWatchlistGroupViewHolder holder,
                                 boolean isExpanded) {
        if (isExpanded) {
            holder.txtWatchlistName.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, null,
                    App.getInstance().getResources().getDrawable(R.drawable.ic_arrow_drop_up),
                    null
            );
        } else {
            holder.txtWatchlistName.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, null,
                    App.getInstance().getResources().getDrawable(R.drawable.ic_arrow_drop_down),
                    null
            );
        }
    }

    static class ExpandableWatchlistGroupViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.txt_watchlist_name)
        TextView txtWatchlistName;

        private Integer groupPosition;

        ExpandableWatchlistGroupViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setTxtWatchlistName(String watchlistName) {
            txtWatchlistName.setText(watchlistName);
        }

        @OnClick(R.id.watchlist)
        void onWatchlistClick() {
            if (watchlists.get(groupPosition).getEntries().isEmpty()) {
                presenter.onEmptyWatchlistSelected(watchlists
                        .get(groupPosition).getWatchlistTitle());
                watchlists.get(groupPosition).setExpanded(false);
            } else {
                if (watchlists.get(groupPosition).isExpanded()) {
                    watchlists.get(groupPosition).setExpanded(false);
                } else {
                    watchlists.get(groupPosition).setExpanded(true);
                }
                invertArrowIcon(this, watchlists.get(groupPosition).isExpanded());
            }
        }

        void setGroupPosition(Integer groupPosition) {
            this.groupPosition = groupPosition;
        }
    }

    static class ExpandableWatchlistChildViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.img_result_poster)
        ImageView imgPoster;
        @BindView(R.id.txt_discover_result_title)
        TextView txtWatchableName;
        @BindView(R.id.txt_discover_result_overview)
        TextView txtWatchableBriefOverview;

        private Integer parentPosition;
        private Integer childPosition;

        ExpandableWatchlistChildViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setImgPoster(String imageUrl) {
            PicassoImageLoader.setImage(imgPoster, buildFullImageUrl(imageUrl, API_IMAGE_WIDTH));
        }

        void setTxtWatchableName(String name, String mediaType) {
            txtWatchableName.setText(buildTitle(name, mediaType));
        }

        void setTxtWatchableBriefOverview(String overview) {
            txtWatchableBriefOverview.setText(overview);
        }

        @OnClick(R.id.watchlist_entry)
        void onWatchlistEntryClick() {
            WatchlistEntryModel entryModel = watchlists
                    .get(parentPosition)
                    .getEntries()
                    .get(childPosition);
            presenter.onEntrySelected(Integer.valueOf(String.valueOf(entryModel.getWatchableId())),
                    entryModel.getMediaType());
        }

        void setChildPosition(Integer childPosition) {
            this.childPosition = childPosition;
        }

        void setParentPosition(Integer parentPosition) {
            this.parentPosition = parentPosition;
        }
    }
}
