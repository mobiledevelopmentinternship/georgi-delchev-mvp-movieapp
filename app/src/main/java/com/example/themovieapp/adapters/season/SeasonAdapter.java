package com.example.themovieapp.adapters.season;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.EpisodePersist;
import com.example.themovieapp.presenters.details.tv.tvseasondetails.contracts.SeasonEpisodeListContracts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Utils.convertDateToString;

public class SeasonAdapter extends RecyclerView.Adapter<SeasonAdapter.SeasonAdapterViewHolder> {

    private final SeasonEpisodeListContracts.Presenter presenter;
    private final List<EpisodePersist> episodes;

    public SeasonAdapter(SeasonEpisodeListContracts.Presenter presenter) {
        this.presenter = presenter;
        this.episodes = presenter.getEpisodes();
    }

    @NonNull
    @Override
    public SeasonAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int position) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_season_episode, viewGroup, false);

        return new SeasonAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SeasonAdapterViewHolder seasonAdapterViewHolder, int position) {
        seasonAdapterViewHolder.setEpisodeName(episodes.get(position).getName());
        seasonAdapterViewHolder.setEpisodeNumber(String.valueOf(episodes.get(position).getEpisodeNumber()));
        seasonAdapterViewHolder.setEpisodeReleaseDate(convertDateToString(episodes.get(position).getAirDate()));
    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }

    class SeasonAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_tv_episode_number)
        TextView txtEpisodeNumber;
        @BindView(R.id.txt_tv_episode_name)
        TextView txtEpisodeName;
        @BindView(R.id.txt_tv_season_episode_release_date)
        TextView txtEpisodeReleaseDate;

        SeasonAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setEpisodeNumber(String episodeNumber) {
            txtEpisodeNumber.setText(episodeNumber);
        }

        void setEpisodeName(String episodeName) {
            txtEpisodeName.setText(episodeName);
        }

        void setEpisodeReleaseDate(String releaseDate) {
            txtEpisodeReleaseDate.setText(releaseDate);
        }

        @OnClick(R.id.item_season_episode)
        void onEpisodeClick() {
            presenter.onSelected(getAdapterPosition());
        }
    }
}