package com.example.themovieapp.adapters.pageradapters;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.themovieapp.R;
import com.example.themovieapp.presenters.base.IBasePresenter;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.contracts.TvSeriesEpisodeDetailsContracts;
import com.example.themovieapp.utils.PicassoImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.themovieapp.utils.Constants.HUGE_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;

public class ImagesPagerAdapter extends PagerAdapter {

    @BindView(R.id.img_view_pager_item)
    ImageView imgViewPagerItem;

    private List<String> allImagesUrls;

    public ImagesPagerAdapter(TvSeriesEpisodeDetailsContracts.Presenter presenter) {
        allImagesUrls = new ArrayList<>();
        allImagesUrls = presenter.getEpisodeImages();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(container.getContext());
        ViewGroup layout = (ViewGroup) layoutInflater.inflate(R.layout.item_image, container, false);
        ButterKnife.bind(this, layout);
        PicassoImageLoader.setImage(imgViewPagerItem, buildFullImageUrl(allImagesUrls.get(position), HUGE_IMAGE_WIDTH));
        container.addView(layout);

        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return allImagesUrls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
