package com.example.themovieapp.adapters.poster;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.MoviePersist;
import com.example.themovieapp.persistance.models.TvSeriesPersist;
import com.example.themovieapp.persistance.models.markers.Watchable;
import com.example.themovieapp.presenters.home.base.BaseHomeContracts;
import com.example.themovieapp.utils.Category;
import com.example.themovieapp.utils.PicassoImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.themovieapp.utils.Constants.BIG_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;

public class PosterAdapter extends RecyclerView.Adapter<PosterAdapter.PosterViewHolder> {

    private List<Watchable> watchables;
    private final BaseHomeContracts.Presenter presenter;

    public PosterAdapter(BaseHomeContracts.Presenter presenter, Category category) {
        this.presenter = presenter;
        watchables = new ArrayList<>();
        if (category != null) {
            getImages(category);
        }
    }

    private void getImages(Category category) {
        switch (category) {
            case UPCOMING:
                if (!presenter.getUpcoming().isEmpty()) {
                    watchables = presenter.getUpcoming();
                }
                break;
            case TOP_RATED:
                if (!presenter.getTopRated().isEmpty()) {
                    watchables = presenter.getTopRated();
                }
                break;
            case MOST_POPULAR:
                if (!presenter.getMostPopular().isEmpty()) {
                    watchables = presenter.getMostPopular();
                }
                break;
        }
    }

    @NonNull
    @Override
    public PosterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new PosterViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_movie_poster, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PosterViewHolder posterViewHolder, int position) {
        posterViewHolder.bindImage(watchables.get(position).getPosterUrl());
    }

    @Override
    public int getItemCount() {
        return watchables.size();
    }

    class PosterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_movie_poster)
        ImageView imgPoster;

        PosterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindImage(String imageUrl) {
            PicassoImageLoader.setImage(imgPoster, buildFullImageUrl(imageUrl, BIG_IMAGE_WIDTH));
        }

        @OnClick(R.id.img_movie_poster)
        void onPosterClick() {
            int position = getAdapterPosition();
            if (watchables.get(position) instanceof MoviePersist) {
                presenter.onPosterClicked(((MoviePersist) watchables.get(position)).getId());
            } else {
                TvSeriesPersist tvSeriesPersist = (TvSeriesPersist) watchables.get(position);
                presenter.onPosterClicked(tvSeriesPersist.getId());
            }
        }
    }
}
