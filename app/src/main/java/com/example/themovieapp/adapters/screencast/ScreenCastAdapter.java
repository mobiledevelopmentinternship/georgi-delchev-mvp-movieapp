package com.example.themovieapp.adapters.screencast;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themovieapp.R;
import com.example.themovieapp.persistance.models.markers.Member;
import com.example.themovieapp.presenters.details.base.contracts.BaseDetailsContracts;
import com.example.themovieapp.presenters.details.tv.tvseriesepisodedetails.contracts.TvSeriesEpisodeDetailsContracts;
import com.example.themovieapp.utils.PicassoImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.themovieapp.utils.Constants.BIG_IMAGE_WIDTH;
import static com.example.themovieapp.utils.Utils.buildFullImageUrl;

public class ScreenCastAdapter extends RecyclerView.Adapter<ScreenCastAdapter.ScreenCastViewHolder> {

    private List<Member> members;

    public ScreenCastAdapter(TvSeriesEpisodeDetailsContracts.Presenter presenter) {
        members = new ArrayList<>(presenter.getScreenCast());
    }

    public ScreenCastAdapter(BaseDetailsContracts.Presenter presenter) {
        members = new ArrayList<>();
        members = presenter.getScreenCast();
    }

    @NonNull
    @Override
    public ScreenCastViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ScreenCastViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_card_view_details, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ScreenCastViewHolder screenCastViewHolder, int position) {
            screenCastViewHolder.setName(members.get(position).getMemberName());
            screenCastViewHolder.setRole(members.get(position).getRole());
            screenCastViewHolder.bindImage(members.get(position).getImageUrl());
    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    static class ScreenCastViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_additional_information_poster)
        ImageView imgMember;
        @BindView(R.id.txt_additional_information_name)
        TextView txtScreenMemberName;
        @BindView(R.id.txt_additional_information_episode)
        TextView txtScreenMemberRole;

        ScreenCastViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindImage(String posterPath) {
            PicassoImageLoader.setImage(imgMember, buildFullImageUrl(posterPath, BIG_IMAGE_WIDTH));
        }

        void setName(String memberName) {
            txtScreenMemberName.setText(memberName);
        }

        void setRole(String role) {
            txtScreenMemberRole.setText(role);
        }

    }
}