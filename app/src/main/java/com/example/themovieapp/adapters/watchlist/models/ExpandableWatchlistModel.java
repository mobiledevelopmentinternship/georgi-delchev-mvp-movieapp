package com.example.themovieapp.adapters.watchlist.models;

import java.util.List;
import java.util.Objects;

public class ExpandableWatchlistModel implements Comparable<ExpandableWatchlistModel> {

    private String watchlistTitle;
    private List<WatchlistEntryModel> entries;
    private boolean isExpanded;
    private Long watchlistPersistId;

    public ExpandableWatchlistModel(String watchlistTitle,
                                    List<WatchlistEntryModel> watchables,
                                    Long watchlistPersistId) {
        this.watchlistTitle = watchlistTitle;
        this.entries = watchables;
        this.watchlistPersistId = watchlistPersistId;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public String getWatchlistTitle() {
        return watchlistTitle;
    }

    public void setWatchlistTitle(String watchlistTitle) {
        this.watchlistTitle = watchlistTitle;
    }

    public List<WatchlistEntryModel> getEntries() {
        return entries;
    }

    public void setEntries(List<WatchlistEntryModel> entries) {
        this.entries = entries;
    }

    public Long getWatchlistPersistId() {
        return watchlistPersistId;
    }

    public void setWatchlistPersistId(Long watchlistPersistId) {
        this.watchlistPersistId = watchlistPersistId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        ExpandableWatchlistModel that = (ExpandableWatchlistModel) object;
        return Objects.equals(watchlistTitle, that.watchlistTitle) &&
                Objects.equals(watchlistPersistId, that.watchlistPersistId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(watchlistTitle, watchlistPersistId);
    }

    @Override
    public int compareTo(ExpandableWatchlistModel other) {
        return this.watchlistPersistId.compareTo(other.watchlistPersistId);
    }
}